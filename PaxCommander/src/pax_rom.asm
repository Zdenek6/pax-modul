;===============================================================================
; PAX commander
; - zapouzdření do ROM modulu
;===============================================================================
                  ; verze pro PMD85-2/2A
                  ; zavaděč cz 0ec00h vloží PAX modul, pokud identifikuje verzi 3
                  call  08c00h
                  ; adresa v ROM modulu
                  dw    12
                  ; velikost 
                  dw    _pax_end - _pax_start
                  ; místo, kam se zavede
                  dw    0
                  ; spuštění
                  jmp   0

_pax_start                  
                  binclude "..\bin\main.bin"
_pax_end                  