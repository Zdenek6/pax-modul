;****************************************************************************
; pax_emu1 - emulátor pmd ver 1 pro nahrávání nekompatibilních her na pmd85-2a
;
; Druhy instrukcí:
; - 1B, vyžadující všechny registry, kromě sp
;            (mov, add, ora, ana, sub, sbb, xor, adc,cmp) - *
;            (stax,ldax,inx,dcx,inr,dcr,dad,xchg)*
; - 1B, pracuje jen s a a psw: rlc, ral, daa, stc, cma, cmc,rrc,rar, dcr a, inr a
; - 2B, všechny registry (mvi,??)*
; - 2B, aritmetické (adi, aci,...,out) - pouze a/psw
; - 3B, všechny registry kromě sp (lxi,shld,sta,lhld,lda)
; - 3B skokové jmp, jxx
; - 3B volání podprogramu, call, cxx
; - 1B návrat z podprogramu ret, rxx
; - 1B volání podprogramu rst
; - 2B - vstup z portů: in
; - pracující s sp: lxi sp,; dad sp; sphl; push/pop
; - speciální sphl
;****************************************************************************
EMU_POSITION      equ   9000h           ; umístění emulátoru
mgfbyte           equ   vrReadByte      ; čtení bajtu z sd modulu
;-------------------------------------------------------------------------------
; Spuštění emulátoru 
; - kopie emulátoru do oblasti 9000h
; - kopie monitoru z verze 1 na místo 8000-8fff
; - nastaví se časovač 8253 a sériová linka 8251
; HL - adresa začátku emulovaného programu
; CY = 1 ... verze bez autoloaderu - načte samsotatně blok
;-------------------------------------------------------------------------------
emustart          ; kopie emulátoru
                  lxi   h, EMU_POSITION
                  lxi   d, Emulator_start
                  lxi   b, Emulator_end - Emulator_start
                  call   copy 
                  ; kopie monitoru verze 1
                  lxi   h, 8000h
                  lxi   d, Monitor1_start
                  lxi   b, Monitor1_end - Monitor1_start
                  call   copy 
                  jmp   emurun                
;-------------------------------------------------------------------------------
; Samotný emulátor - spouštěný od jiné adresy
;-------------------------------------------------------------------------------
Emulator_start    phase EMU_POSITION
; skoky podmíněné i nepodmíněné
insjxx            sta   .opjmp
                  mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  pop   psw
                  push  psw
.opjmp            jmp   insjxx2
                  ; skok se nekoná
                  jmp   emuloop
; volání podprogramu (podmíněné i nepodmíněné)
inscxx            sta   opcall
                  mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  pop   psw
                  push  psw
opcall            call  inscxx2
                  ; volání podprogramu se nekoná
                  jmp   emuloop
; návratové instrukce ret, rxx
insrxx            sta   .opret
                  pop   psw
                  push  psw
                  call  .opret
                  ; podmínka splněna - návrat z podpogramu
                  call  popdw
                  xchg
                  jmp   emuloop
.opret            nop
                  ; podmínka nesplněna -
                  pop   b               ; úprava zásobníku
                  jmp   emuloop
inspopa           lxi   d,psw_reg
                  jmp   inspop
inspoph           lxi   d,hl_reg
                  jmp   inspop
inspopd           lxi   d,de_reg
                  jmp   inspop
inspopb           lxi   d,bc_reg
inspop            push  h
                  lhld  sp_reg
                  mov   a,m
                  inx   h
                  stax  d
                  inx   d
                  mov   a,m
                  inx   h
                  stax  d
                  shld  sp_reg
                  pop   h
                  jmp   emuloop
; pushování
inspusha          push  h
                  lhld  psw_reg
                  jmp   pushcmpsw
inspushb          push  h
                  lhld  bc_reg
                  jmp   pushcm
inspushd          push  h
                  lhld  de_reg
                  jmp   pushcm
inspushh          push  h
                  lhld  hl_reg
                  jmp   pushcm
; dvou bajtové alu operace s přímým operandem
insai             sta   .opa2
                  mov   a,m
                  inx   h
                  sta   .opa2+1
                  pop   psw
.opa2             adi   0
                  push  psw
                  jmp   emuloop
; rst - převod na call
insrst            ani   38h
                  mov   e,a
                  mvi   d,0
                  call  inscxx2
; xthl - pozor na zápis od oblasti rom!!!
insxthl           jmp   insxthl_
; pchl
inspchl           lhld  hl_reg
                  jmp   emuloop
insdi
insei
insxchg           jmp   insallreg
inssphl           xchg
                  lhld  hl_reg
                  shld  sp_reg
                  xchg
                  jmp   emuloop
insin             jmp   insin2
insout            jmp   insout2
; makro zapisující pouze lsb
dbl               macro a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p
                  db    a & 255, b & 255, c & 255, d & 255, e & 255, f & 255, g & 255, h & 255
                  db    i & 255, j & 255, k & 255, l & 255, m & 255, n & 255, o & 255, p & 255
                  endm
_tablec0          ds    ((_tablec0 &0ff00h + 0c0h)-_tablec0)
; rozskoková tabulka
; - na konci stránky xxc0 se nacházejí instrukce c0 až ff
tablec0 
                  dbl   insrxx, inspopb, insjxx, insjxx , inscxx, inspushb, insai, insrst , insrxx, insrxx , insjxx, insjxx , inscxx, inscxx, insai, insrst
                  dbl   insrxx, inspopd, insjxx, insout , inscxx, inspushd, insai, insrst , insrxx, insrxx , insjxx, insin , inscxx, inscxx, insai, insrst
                  dbl   insrxx, inspoph, insjxx, insxthl, inscxx, inspushh, insai, insrst , insrxx, inspchl, insjxx, insxchg, inscxx, inscxx, insai, insrst
                  dbl   insrxx, inspopa, insjxx, insdi , inscxx, inspusha, insai, insrst , insrxx, inssphl, insjxx, insei , inscxx, inscxx, insai, insrst

; rozskoková tabulka pro instrukce 0x00 - 0x3f
; - musí být zarovnána na celou stránku
; - obsahuje pouze nižší adresy, vyšší adresa je stejná jakou má tabulka
table00           dbl   insnop, inslxib , insstaxb, insinx , insinr, insdcr, insmvi, insrlc , insnop, insdad , insldax, insdcx , insinr , insdcr , insmvi, insrrc
                  dbl   insnop, inslxid , insstaxd, insinx , insinr, insdcr, insmvi, insral , insnop, insdad , insldax, insdcx , insinr , insdcr , insmvi, insrar
                  dbl   insnop, inslxih , insshld, insinx , insinr, insdcr, insmvi, insdaa , insnop, insdad , inslhld, insdcx , insinr , insdcr , insmvi, inscma
                  dbl   insnop, inslxisp, inssta , insinxsp, insinrm, insdcrm, insmvim, insstc , insnop, insdadsp, inslda , insdcxsp, insinra, insdcra, insmvi, inscmc
; hlavní emulační smyčka
insnop 
emuloop 
                  in    0f5h
                  ani   40h
                  jz    emuend
nb                mov   a,m
                  inx   h
                  cpi   40h
                  jc    insdec1
                  cpi   0c0h
                  jnc   insdec2
                  cpi   70h
                  jc    ins1b
                  cpi   78h
                  jc    insmov_m
ins1b 
; jednobajtové instrukce vyžadující všechny registry (vyjma sp)
insldax
insinx
insdcx
insinr
insdcr
insdad
insallreg         sta   .opins1
                  pop   psw
                  pop   b
                  pop   d
                  xthl
.opins1           nop
                  xthl
                  push  d
                  push  b
                  push  psw
                  jmp   emuloop
; mvi m - s ochranou proti přepisu rom oblasti
insmvim           mov   b,m
                  inx   h
                  xchg
                  lhld  hl_reg
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  xchg
                  jz    emuloop
                  mov   a,b
                  stax  d
                  jmp   emuloop
; zpracování instrukcí:
; 3b instrukce lxi,shld,lhld,lda
inslxih           jmp   inslxih_
inslxid           jmp   inslxid_
inslxib           jmp   inslxib_
inslxisp          jmp   inslxisp_
inslda            jmp   inslda_
insshld           jmp   insshld_
inslhld           jmp   inslhld_
; zápis - test, zda se nezapisuje do oblasti rom
inssta            mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  mov   a,d
                  ani   0c0h
                  cpi   80h
                  jz    emuloop
                  lda   psw_reg+1
                  stax  d
                  jmp   emuloop
; stax d
insstaxd          mov   b,a
                  lda   de_reg+1
                  jmp   emuwrite1b
; stax b
insstaxb          mov   b,a
                  lda   bc_reg+1
                  jmp   emuwrite1b
; jednobajtové instrukce pracující jen s a a psw (rychlejší vykonání)
insrlc 
insrrc
insral
insrar
insdaa
inscma
inscmc
insstc
insinra
insdcra
                  sta   .opinsa
                  pop   psw
.opinsa           nop
                  push  psw
                  jmp   emuloop
; 2B instrukce mvi 
insmvi            sta   opstains
                  mov   a,m
                  inx   h
                  sta   opstains+1
                  pop   psw
                  pop   b
                  pop   d
                  xthl
opstains          mvi   a,0
                  xthl
                  push  d
                  push  b
                  push  psw
                  jmp   emuloop
insinxsp          xchg
                  lhld  sp_reg
                  inx   h
                  shld  sp_reg
                  xchg
                  jmp   emuloop
insdcxsp          xchg
                  lhld  sp_reg
                  dcx   h
                  shld  sp_reg
                  xchg
                  jmp   emuloop
; instrukce mov m,a... mov m,h, stax
insinrm
insdcrm           jmp   insinrdcrm

insdadsp          mov   b,h
                  mov   c,l
                  pop   psw
                  lhld  sp_reg
                  xchg
                  lhld  hl_reg
                  dad   d
                  shld  hl_reg
                  push  psw
                  mov   h,b
                  mov   l,c
                  jmp   emuloop
                  if    insdadsp > 91ffh
                  warning "emulace - skok mimo rozsah"
                  endif

pushcmpsw         mov   a,l
                  ani   0f7h
                  ori   2
                  mov   l,a
pushcm            xchg
                  jmp   emupush
insinrdcrm        sta   opinrdcrm
                  xchg
                  lhld  hl_reg
                  mov   a,m
                  lxi   h,dcrinrm
                  mov   m,a
                  pop   psw
opinrdcrm         nop
                  push  psw
                  mov   b,m
                  lhld  hl_reg
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .sk
                  mov   m,b
.sk               xchg
                  jmp   emuloop
dcrinrm           db    0

insmov_m          mov   b,a
                  lda   hl_reg+1
emuwrite1b        ani   0c0h
                  cpi   80h
                  mov   a,b
                  jnz   insallreg
                  jmp   emuloop
insin2            mov   a,m
                  inx   h
                  cpi   1fh
                  jz    .i1f
                  cpi   1eh
                  jz    .i1e
                  sta   .op+1

.op               in    0
.e                sta   psw_reg+1
                  jmp   emuloop
.i1f              mvi   a,2
                  jmp   .e
.i1e              call  mgfbyte
                  sta   psw_reg+1
                  jmp   emuloop
insout2           mov   a,m
                  inx   h
                  sta   .op+1
                  cpi   0f7h
                  lda   psw_reg+1
                  jnz   .op
                  ana   a
                  jm    emuloop         ; změna módu není povolena
.op               out   0
                  jmp   emuloop
insjxx2           xchg
                  jmp   emuloop

; pomocná funkce pro vytáhnutí dat ze zásobníku
popdw             lhld  sp_reg
                  mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  shld  sp_reg
                  ret
inscxx2           xchg
                  xthl
emupush           lhld  sp_reg
                  dcx   h
                  ; ochrana proti zápisu do oblasti rom
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .skip1
                  mov   m,d
.skip1            dcx   h
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .skip2
                  mov   m,e
.skip2            shld  sp_reg
                  pop   h
                  jmp   emuloop
; instrukce s operačním kódem 0xc0 až 0xff
insdec2           mvi   d, tablec0>>8
                  mov   e,a
                  xchg
                  mov   l,m
                  push  h
                  xchg
                  ret
; instrukce s operačním kódem 0x00 až 0x3f
; - načte dolní část skokové adresy
insdec1           mvi   d, table00>>8
                  mov   e,a
                  xchg
                  mov   l,m
                  push  h
                  xchg
                  ret
; lxi verze
inslxih_          mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  xchg
                  shld  hl_reg
                  xchg
                  jmp   emuloop
inslxid_          mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  xchg
                  shld  de_reg
                  xchg
                  jmp   emuloop
inslxib_          mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  xchg
                  shld  bc_reg
                  xchg
                  jmp   emuloop
inslxisp_         mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  xchg
                  shld  sp_reg
                  xchg
                  jmp   emuloop
inslda_           mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  ldax  d
                  sta   psw_reg+1
                  jmp   emuloop
inslhld_          mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  ldax  d
                  inx   d
                  sta   hl_reg
                  ldax  d
                  sta   hl_reg+1
                  jmp   emuloop
insshld_          mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  push  h
                  lhld  hl_reg
wrhl              xchg
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .skip1
                  mov   m,e
.skip1            inx   h
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .skip2
                  mov   m,d
.skip2            pop   h
                  jmp   emuloop

insxthl_          push  h
                  lhld  hl_reg
                  push  h
                  ; vytáhne se ze zásobníku údaj
                  lhld  sp_reg
                  mov   e,m
                  inx   h
                  mov   d,m
                  xchg
                  shld  hl_reg
                  ; zápis hl s ochranou před zápis do rom oblasti
                  pop   h
                  xchg
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .skip1
                  mov   m,d
.skip1            dcx   h
                  mov   a,h
                  ani   0c0h
                  cpi   080h
                  jz    .skip2
                  mov   m,e
.skip2            pop   h
                  jmp   emuloop
; načte všechny registry a spustí program
; - dochází po stisku klávesy shift + stop
emuend                                  ; nejprve čekáme na uvolnění kláves, aby se neovlivnil další program
.rel              in    0f5h
                  ani   40h
                  jz    .rel
                  shld  .pcset+1
                  lhld  hl_reg
                  shld  .hlset+1
                  lhld  de_reg
                  shld  .deset+1
                  lhld  bc_reg
                  shld  .bcset+1
                  lhld  sp_reg
                  shld  .spset+1
                  pop   psw
.hlset            lxi   h, 0
.deset            lxi   d, 0
.bcset            lxi   b, 0
.spset            lxi   sp, 0
.pcset            jmp   0
;-------------------------------------------------------------------------------
; Rozběh emulace
;-------------------------------------------------------------------------------
emurun            lxi   sp, emustack
                  ; 8251
                  xra   a
                  out   1fh
                  mvi   a, 40h
                  out   1fh
                  mvi   a, 0edh
                  out   1fh
                  mvi   a, 23h
                  out   1fh                  
                  ;8253: mód 3, symetr. dělička, 1200bd
                  mvi   a,76h           
                  out   5fh
                  mvi   a,0abh
                  out   5dh
                  mvi   a,06h
                  out   5dh
                  ; pokud hra nemá autoloader, musí se načíst tělo programu
                  call  isAutorun
                  jc    .auto
                  rst   5
                  call  vrLoadMgBlock
                  jc    8000h
                  ; zahájení emulace - pokračuje jako po načtení prvního bloku
.auto             lxi   h, emu_entry
                  jmp   emuloop
; Vstupní bod do emulace (tj. odsud se spouští emulovaný kód)
emu_entry         xra   a
                  mvi   c,3fh
                  lxi   sp, 7ffdh
                  lhld  7ffbh
                  pchl                  
emu_var
; zásobník emulátoru
emustack          equ   emu_var + 16
psw_reg           equ   emustack + 0
bc_reg            equ   emustack + 2
de_reg            equ   emustack + 4
hl_reg            equ   emustack + 6
sp_reg            equ   emustack + 8
                  dephase
Emulator_end       
Monitor1_start    binclude  "monitor1.bin"
Monitor1_end