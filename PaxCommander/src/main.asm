;===============================================================================
; PAX commander
; - průzkumník souborů a zavaděč programů
; - nastavení PAX modulu
;
; Commander funguje na verzích PMD85-2/2A/3.
;===============================================================================
;-------------------------------------------------------------------------------
; Konfigurace překladu
;-------------------------------------------------------------------------------
; Max. počet zanoření do adresářů
; Jelikož exFAT neudržuje ve složkách odkaz na nadřazenou složku, je nutné
; toto řešit softwarově a aktuální cestu si udržovat v PMD. Cesta se udržuje
; jako seznam pořadí složek ve výčtu adresáře. Následující položka určuje
; max. délku takového seznamu.
MAX_PATHDEPTH     equ   10               
;-------------------------------------------------------------------------------
; Spuštění od adresy 0000 
;-------------------------------------------------------------------------------
                  org   0
                  di
                  lxi   sp, 8000h
                  jmp   start
;-------------------------------------------------------------------------------
; RST 1 - kopie krátkého bloku, pozor, jeden bajt je před adresou 8!
; I: HL - dest
;    DE - source
;     C - počet 
;-------------------------------------------------------------------------------
                  org   1*8-1
cpy8              rz    
                  ldax  d
                  mov   m,a
                  inx   h
                  inx   d
                  dcr   c
                  jmp   cpy8
;-------------------------------------------------------------------------------
; RST 2  
;-------------------------------------------------------------------------------
                  org   2*8
                  ret
;-------------------------------------------------------------------------------
; RST 3 - příkaz pro PAX modul
; Použití:        RST 3
;                 db  command
;
; Nebo makrem:    pax command 
;-------------------------------------------------------------------------------
                  org   3*8
                  xthl
                  mov   a,m
                  inx   h
                  xthl
                  jmp   vrPaxCmd
;-------------------------------------------------------------------------------
; RST 4 - provede HL - DE, bez uložení výsledku
; příznaky C a Z se nastaví stejně jako u CMP instrukce
;-------------------------------------------------------------------------------
                  org   4*8
cmp16             mov   a,h
                  sub   d
                  rnz
                  mov   a,l
                  sub   e
                  ret                  
;-------------------------------------------------------------------------------
; RST 5 - načte adresu a délky z hlavičky
;-------------------------------------------------------------------------------
                  org   5*8
                  lhld  0c1b6h
                  xchg
                  lhld  0c1b4h
                  ret      
;-------------------------------------------------------------------------------
; RST 6 - slouží k odchycení přechodu na režim kompatibility
;-------------------------------------------------------------------------------
                  org   6*8
                  jmp   pmd3To2A.trap                  
;-------------------------------------------------------------------------------
; RST7 - obsluha přerušení/služby na pozadí
;-------------------------------------------------------------------------------
                  org   7*8
                  jmp   paxService
;-------------------------------------------------------------------------------
; Hlavní smyčka aplikace
;-------------------------------------------------------------------------------
start             call  setup
                  call  drawDesktop
                  call  winSDHC
                  ;call  winOptions
                  jmp   sysExit

;-------------------------------------------------------------------------------
; Překreslí desktop bez otevřeného okna
;-------------------------------------------------------------------------------
drawDesktop       mvi   a, CA_BLUE + CA_PATTERN
                  call  setColor
                  call  clearScreen
                  xra   a
                  jmp   setColor                  
;-------------------------------------------------------------------------------
; Prvotní nastavení hw, detekce typu PMD, příprava paměti
;-------------------------------------------------------------------------------
setup             ; rozlišení verze 2/3
                  lxi   h, 0e030h
                  mov   a,m
                  inr   m
                  cmp   m
                  jnz   .not3
                  ; verze PMD85-3
                  ; vyvolání režimu kompatibility
                  call  pmd3To2A
                  lxi   h, .font3
                  mvi   a, 3
                  jmp   .common
.not3             ; kopie monitoru - má smysl jen u verze 2A, u ostatních nevadí
                  lxi   h, 8000h
                  mvi   a, 90h
.cpy              mov   b,m
                  mov   m,b
                  inx   h
                  cmp   h
                  jnz   .cpy                                   
                  ; rozlišení modelu 1/2
                  lda   8b6ch
                  cpi   0c5h            ; monitor 2/2A zde má instrukci push b
                  jz    .model2
                  ; model 1
                  xra   a
                  lxi   h, .font1
                  jmp   .common
.model2           ; model2/2A - rozlišení později
                  lxi   h, .font2
                  mvi   a, 2
.common           ; společná část pro všechny modely
                  ; zápis verze modelu 0- PMD1, 2 - PMD2/2A, 3 - PMD3
                  sta   varModel
                  ; kopie fontů z HL
                  mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  push  h
                  lxi   h, varFont
                  lxi   b, 200h
                  call  copy
                  xthl
                  mov   e,m
                  inx   h
                  mov   d,m
                  pop   h
                  mvi   b, 1
                  call  copy
                  ; nakopíruje rutiny do oblasti vedle videoram
                  call  Copy2VRAM
                  ; vymaže paměti
                  lxi   h, _zeroSection
                  lxi   b, _zeroSectionEnd - _zeroSection
                  call  clearMemory
                  ; předvyplnění mezer do souborové položky
                  lxi   h, varFileItem
                  mvi   c, varFileItem.end - varFileItem
                  mvi   d, ' '
                  call  clearMemory.loop
                  ; aktivuje AllRam
                  mvi   a, 82h          ; zapne AllRam režim, vypne ROM
                  out   0f7h            ; PCH = výstup
                  ; rozlišení modelu 2/2A
                  lda   varModel
                  sui   2
                  jnz   .not2A
                  lxi   h, 8000h
                  mov   b, m
                  mov   m, a            ; A = 0
                  cmp   m
                  mov   m, b
                  jz    .not2A
                  inr   a
                  sta   varModel        ; varModel = 1 (PMD2)
.not2A            ; nastaví výchozí barevný režim (colorace)
                  mvi   a,2
                  sta   varColorMode
                  mvi   a,0c1h
                  out   0f8h
                  mvi   a,001h
                  out   06ch
                  ; vypne SAA
                  mvi   a, 1ch
                  out   0efh
                  mvi   a, 2
                  out   0eeh
                  ret
.font1            dw    8600h, 8700h
.font2            dw    8600h, 88c0h
.font3            dw    0e600h, 0e8c0h
;-------------------------------------------------------------------------------
; paxService / RST 7 by mělo volat každé okno, aktualizují se služby
; - odchytávání systémových kláves
; - odchytávání uart komunikace
;-------------------------------------------------------------------------------
paxService        ; test - příjem z uartu
                  call  uartService
                  ; klávesy
                  call  xkey_inkey
                  ana   a
                  rz
                  push  psw
                  ; vyskočení do monitoru?
                  cpi   KEY_SHIFT_RCL
                  jz    SysExit
                  pop   psw
                  ret
                  
;-------------------------------------------------------------------------------
; Vložení dalších zdrojových kódů
;-------------------------------------------------------------------------------
                  include "pax_lib.asm"
                  include "pax_inkey.asm"
                  include "pax_gfx.asm"
                  include "pax_list.asm"
                  include "pax_patch.asm"
                  include "pax_sdhc.asm"
                  include "winSDHC.asm"
                  include "winROM.asm"
                  include "winUART.asm"
                  include "winPTPExplorer.asm"
                  include "winOptions.asm"
                  include "pax_vram.asm"
                  include "pax_emu1.asm"
ram_protected                  
;-------------------------------------------------------------------------------
; Využití paměti
;-------------------------------------------------------------------------------
varSDHCStatus     ds    1               ; stav SD karty
varFont           ds    96*8            ; font pro kódy 32 - 127
varColorMode      ds    1               ; barevný mód
varCurrentColor   ds    1               ; aktuální index nastavené barvy
varColor          ds    2               ; atributy pro lichý a sudý mikrořádek
varCursor         ds    2               ; kurzor na obrazovce
varModel          ds    1               ; typ PMD
varRunOptions     ds    1               ; volby spuštění

_zeroSection
varNumber         ds    6               ; 4 znaky číslo, 1 null term., +1 jednotka "k" pro soubory                  
varEntry
.attr             ds    1               ; atributy
.size             ds    4               ; velikost souboru
.name             ds    16              ; jméno
varLabel          ds    16              ; volume label
; stav přijímání na uartu
varUartState      ds    1               ; 0 - nic, 1 - přijato #
; proměnné pro seznam 
_listbox
varListCalback    ds    3               ; adresa metody pro získání textu a barvy položky
varListPos        ds    2               ; umístění seznamu ve videoram
varListIndex      ds    1               ; aktuální položka
varListCount      ds    1               ; počet položek v seznamu
varListFirst      ds    1               ; index první viditelné položky
varListVisible    ds    1               ; počet viditelných položek
varListColor      ds    1               ; barva položky
varListSelected   ds    1               ; barvy vybrané položky
varListWidth      ds    1               ; počet znaků na položku
_listboxEnd
varFileItem       ds    1               ; mezera
.name             ds    15+5            ; jméno + 5 mezer
.info             ds    6+1             ; 6 znaků velikost + null
.end              
; aktuální cesta
varPath
.depth            ds    1               ; počet platných údajů v cestě, 0 => root
.list             ds    MAX_PATHDEPTH   ; prostor pro vyjádření cesty k aktuální složce
; používá PTP okno
varFilePos        ds    4
varCurListPtr     ds    2
varCurRecord      ds    2
varListPointers   ds    255*2           ; prostor pro ukazatele na položky v seznamu
varListItems      ds    255*20          ; prostor pro položky souborů (20B na položku)
_zeroSectionEnd

Ram_end           warning "Ram end: \{Ram_end}H"                  
