;===============================================================================
; pax_list
; - funkce pro seznamy
; - funkce volají callback funkci, která má vracet:
;   - pokud je CY = 0
;        HL - ukazatel na text položky s indexem A
;         C - barva položky
;   - CY = 1, pokud už je dosaženo konce seznamu
;===============================================================================
;-------------------------------------------------------------------------------
; Inicializuje seznam ze struktury
; I: HL - ukazatel na strukturu:
;                 2B Address            adresa callback metody
;                 2B Cursor             umístění seznamu ve videoram
;                 1B Index              index vybrané položky
;                 1B Count              počet položek
;                 1B First              index první viditelné položky
;                 1B Visible            počet viditelných položek
;                 1B Color              barva položky - použije se pro řádky, kde už seznam nezasahuje
;                 1B Selected           barvy vybrané položky
;                 1B Width              počet znaků na položku
;-------------------------------------------------------------------------------
lb_Init           xchg
                  lxi   h, varListCalback
                  mvi   m, 0c3h         ; JMP
                  inx   h
                  mvi   c, _listboxEnd - _listbox - 1
                  rst   1
;-------------------------------------------------------------------------------
; Překreslí všechny položky
; I: HL - ukazatel na deskriptor seznamu
;-------------------------------------------------------------------------------
lb_Refresh        mvi   b, 0
                  lda   varListVisible
                  mov   c, a
.loop             push  b
                  call  lb_DrawItem
                  pop   b
                  inr   b
                  dcr   c
                  jnz   .loop
                  ret
;-------------------------------------------------------------------------------
; Uloží akt. nastavení listboxu do zásobníku
;-------------------------------------------------------------------------------
lb_Push           pop   b               ; BC = návratová adresa
                  lxi   h, - (_listboxEnd - _listbox - 1)
                  dad   sp
                  sphl
                  push  b
                  lxi   d, varListCalback+1
                  mvi   c, _listboxEnd - _listbox - 1
                  rst   1
                  ret
;-------------------------------------------------------------------------------
; Načte akt. nastavení listboxu ze zásobníku
;-------------------------------------------------------------------------------
lb_Pop            lxi   h, 2
                  dad   sp
                  lxi   d, varListCalback+1
                  xchg
                  mvi   c, _listboxEnd - _listbox - 1
                  rst   1
                  pop   b
                  xchg
                  sphl
                  push  b
                  ret
;-------------------------------------------------------------------------------
; Vykreslí položku
; I: B - pořadí (ve viditelném seznamu)
;-------------------------------------------------------------------------------
lb_DrawItem       ; výpočet pozice položky
                  lhld  varListPos
                  lxi   d, 10*64
                  mov   c, b
.add              dcr   c
                  jm    .adde
                  dad   d
                  jmp   .add
.adde             shld  varCursor
                  ; volání callback metody
                  lxi   h, varListCount
                  lda   varListFirst
                  add   b
                  cmp   m               ; překročili jsme počet položek?
                  jnc   .noitem
                  mov   e,a
                  lda   varListIndex
                  xra   e
                  mov   c,a
                  mov   a,e
                  push  b
                  call  varListCalback
                  pop   b
                  jc    .noitem
                  ; rozhodnutí, zda se jedná o aktivní položku nebo ne
                  inr   c
                  dcr   c
                  jnz   .render
                  lda   varListSelected
.render           call  setColor
                  ; výpis textu/ doplnění mezer za text
                  lda   varListWidth
                  mov   c,a
.t                mov   a,m
                  inx   h
                  ana   a
                  jnz   .p
                  dcx   h
                  mvi   a, 32
.p                call  print
                  dcr   c
                  jnz   .t
                  ret
.noitem           ; tady už položka není, jen se vybarví barvou listboxu
                  lda   varListColor
                  call  setColor
                  lda   varListWidth
                  mov   b,a
                  mvi   c, 12
                  lhld  varCursor
                  jmp   fillRect
;-------------------------------------------------------------------------------
; Nastaví první položku
;-------------------------------------------------------------------------------
lb_Home           xra   a
                  sta   varListIndex
                  sta   varListFirst
                  jmp   lb_Refresh
;-------------------------------------------------------------------------------
; Nastaví poslední položku
;-------------------------------------------------------------------------------
lb_End            lxi   h, varListVisible
                  mov   c,m             ; C = visible
                  dcx   h
                  dcx   h
                  lda   varListCount
                  mov   b,m             ; B = count
                  dcx   h
                  mov   m,b
                  dcr   m               ; index = count - 1
                  xra   a
.l                add   c
                  cmp   b
                  jc    .l
                  sub   c
                  inx   h
                  inx   h
                  mov   m,a             ; first
                  jmp   lb_Refresh

;-------------------------------------------------------------------------------
; Nastaví další položku v seznamu
;-------------------------------------------------------------------------------
lb_Next           lxi   h, varListIndex
                  mov   b, m
                  inr   b
                  inx   h
                  mov   a,m
                  cmp   b
                  rz
                  dcx   h
                  mov   m, b
                  ; nutno překreslit položky
                  ; nejprve původní index, pak aktu8ln9
                  mov   a, b
                  lxi   h, varListFirst
                  sub   m
                  dcr   a
                  mov   b,a
                  push  b
                  call  lb_DrawItem
                  pop   b
                  inr   b
                  lda   varListVisible
                  cmp   b
                  jnz   lb_DrawItem
                  ; došlo k podtečení - skok na další stránku
                  lxi   h, varListFirst
                  add   m
                  mov   m, a
                  jmp   lb_Refresh
;-------------------------------------------------------------------------------
; Nastaví předchozí položku v seznamu
;-------------------------------------------------------------------------------
lb_Prev           lxi   h, varListIndex
                  mov   a, m
                  ana   a
                  rz
                  dcr   m
                  ; nutno překreslit položky
                  ; nejprve tu označenou
                  lda   varListIndex
                  inr   a
                  lxi   h, varListFirst
                  sub   m
                  jz    .refresh
                  mov   b,a
                  push  b
                  call  lb_DrawItem
                  pop   b
                  dcr   b
                  jmp   lb_DrawItem
                  ; došlo k podtečení - skok na další stránku
.refresh          lda   varListVisible
                  mov   b,a
                  lda   varListFirst
                  sub   b
                  jnc   .ok
                  xra   a
.ok               sta   varListFirst
                  jmp   lb_Refresh
;-------------------------------------------------------------------------------
; Zkusí se přesunout na nejbližší další položku, která začíná na dané písmeno
; I: A - znak, kterým má položka začínat
;-------------------------------------------------------------------------------
lb_jump           call  upCase
                  cpi   'A'
                  rc
                  cpi   'Z' + 1
                  rnc
                  sta   .cpi+1
                  lxi   h, varListIndex
                  mov   b, m            ; B = index akt. položky
                  inx   h
                  mov   c, m            ; C = počet položek
.loop             inr   b
                  mov   a,b             ; A = další položka
                  cmp   c               
                  rz                    ; dosáhli jsme konce? pak tedy konec...
                  push  b
                  call  varListCalback  ; získá ukazatel na text v HL
                  call  skipWhite       ; přeskočí mezery
                  pop   b
                  rc                    ; tato položka neexistuje
                  mov   a,m
                  call  upCase
.cpi              cpi   0               ; začíná na hledající znak?
                  jnz   .loop
                  ; nalezeno - nastaví se jako akt. položka
                  lxi   h, varListIndex
                  mov   m, b
                  inx   h
                  inx   h
                  inx   h               ; HL = varListVisible
                  xra   a
.lf               add   m               ; A += visible
                  cmp   b
                  jz    .lf
                  jc    .lf
                  sub   m
                  dcx   h
                  mov   m,a             ; first                  
                  jmp   lb_Refresh
;-------------------------------------------------------------------------------
; Naplní seznam výpisem ze souborového adresáře
; - nastaví také callback, nastaví položku na první
; I: HL - umístění na obrazovce
;-------------------------------------------------------------------------------
_initstack        jmp   cbDirList       ; callback
.pos              dw    6*10*64+10+0c000h ; umístění seznamu ve videoram
                  db    0               ; index vybrané položky
                  db    0               ; počet položek
                  db    0               ; index první viditelné položky
                  db    14              ; počet viditelných položek
                  db    CA_WHITE        ; barva položky
                  db    CA_YELLOW+CA_FILL ; barvy vybrané položky
                  db    28              ; šířka položky
.end
lb_DirInit        shld  _initstack.pos
                  lxi   d, _initstack
                  lxi   h, varListCalback
                  mvi   c, _initstack.end - _initstack
                  rst   1               ; copy 8
                  ; ... pokračuje refreshem
;-------------------------------------------------------------------------------
; Znova sestaví seznam souborů a složek, nastaví pozici na první položku
;-------------------------------------------------------------------------------
lb_DirRefresh     xra   a
                  sta   varListIndex
                  sta   varListFirst
.saveindex        call  sdLoadDir
                  mov   a,b
                  sta   varListCount
                  call  sdSortList
                  jmp   lb_Refresh
;-------------------------------------------------------------------------------
; Vrátí ukazatel na aktuální položku u seznamu souborů
;-------------------------------------------------------------------------------
lb_GetFileItem    lda   varListIndex
;-------------------------------------------------------------------------------
; Vrátí ukazatel na položku u seznamu souborů
; I: A - index
;-------------------------------------------------------------------------------
lb_Index2Ptr      mov   l,a
                  mvi   h,0
                  dad   h
                  lxi   d, varListPointers
                  dad   d
                  mov   a,m
                  inx   h
                  mov   h,m
                  mov   l,a             ; HL = ukazatel na položku
                  ret
;-------------------------------------------------------------------------------
; Callback pro file list
;-------------------------------------------------------------------------------
cbDirList         lxi   h, varListCount
                  cmp   m
                  cmc
                  rc
                  ; určení adresy
                  call  lb_Index2Ptr
                  ; typ?
                  mov   a,m
                  ani   ATTR_DIR        ; directory?
                  push  psw
                  jz    .file
                  ; directory - dokopíruje text <DIR>
                  push  h
                  lxi   d, txtDir
                  jmp   .contcopy
                  ; soubor - dopíše velikost souboru
.file             push  h
                  inx   h
                  mov   c,m
                  inx   h
                  mov   b,m
                  inx   h
                  mov   e,m
                  inx   h
                  mov   d,m
                  call  size2string
                  lxi   d, varNumber
.contcopy         lxi   h, varFileItem.info+1
                  mvi   c, 5
                  rst   1               ; copy8
                  pop   h
                  ; kopie jména
.namecopy         lxi   d,5
                  dad   d
                  xchg
                  lxi   h, varFileItem.name
                  mvi   c, 15
                  rst   1
                  pop   psw
                  mvi   a, CA_WHITE
                  lxi   h, varFileItem
                  rz
                  mvi   a, CA_MAGENTA
                  ret
txtDir            db    "<DIR>"