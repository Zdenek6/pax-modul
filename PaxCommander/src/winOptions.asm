;===============================================================================
; winOptions
; - okno s volbou způsobu načtení
; - vybere typ spuštění:
;   žádný patch     - nijak neupravuje loader
;   patch 8b6c/8de2 - nahradí volání těchto adres za volání do videoram 
;   patch out F7    - nahradí nop - zamezí vypnutí allram režimu
;   emulace PMD85-1 - spustí emulaci PMD85-1
;===============================================================================
; 
;-------------------------------------------------------------------------------
winOptions        lxi   h, optionsDesc
                  call  winLayout
                  lxi   h, _optlist
                  call  lb_Init
.loop             rst   7
                  lxi   h, .loop
                  push  h
                  rz
                  cpi   KEY_NEXT
                  jz    lb_Next
                  cpi   KEY_PREV
                  jz    lb_Prev
                  cpi   KEY_HOME
                  jz    lb_Home
                  cpi   KEY_END
                  jz    lb_End
                  cpi   KEY_EOL                  
                  jnz   .j
                  lda   varListIndex
                  jmp   .setopt
.j                sui   KEY_K0
                  rnz
                  cma
.setopt           sta   varRunOptions   ; FF - nespouštět
                  pop   psw
                  ret  
;-------------------------------------------------------------------------------
; Callback pro seznam s PTP soubory
;-------------------------------------------------------------------------------
cbOptList         lxi   h, varListCount
                  cmp   m
                  cmc
                  rc
                  ; *16 + opttext
                  mov   l,a
                  mvi   h,0
                  dad   h               ; *2
                  dad   h               ; *4
                  dad   h               ; *8
                  dad   h               ; *16
                  lxi   d, opttxt
                  dad   d
                  mvi   a, CA_WHITE
                  ret
opttxt            db    "zadny patch    ",0                  
                  db    "patch 8b6c/8de2",0
                  db    "patch out F7   ",0
                  db    "patch vseho    ",0
                  db    "emulace PMD85-1",0                  
;-------------------------------------------------------------------------------
; Popis okna
;-------------------------------------------------------------------------------
optionsDesc       PAXWIN  19, 9, optTitle

                  ; listbox
                  db    CA_CYAN + CA_FILL + CA_BORDER
                  db    _x+1, _y+1
                  db    _w-2, 5*10
                  dw    0
                  
                  ; K0
                  db    CA_CYAN + CA_FILL + CA_BORDER
                  db    _x+2, _y+7
                  db    5, 10
                  dw    romK0

                  ; EOL
                  db    CA_RED + CA_FILL + CA_BORDER
                  db    _x+12, _y+7
                  db    5, 10
                  dw    romEOL


                  db    255
optTitle          db    "Volba spusteni",0
;-------------------------------------------------------------------------------
; Inicializace seznamu
;-------------------------------------------------------------------------------
_optlist          dw    cbOptList       ; callback
.pos              dw    (_y+1)*64*10 +_x +0c000h ; umístění seznamu ve videoram
.index            db    0               ; index vybrané položky
.count            db    5              ; počet položek
                  db    0               ; index první viditelné položky
                  db    5              ; počet viditelných položek
                  db    CA_WHITE        ; barva položky
                  db    CA_YELLOW+CA_FILL ; barvy vybrané položky
                  db    _w-2            ; šířka položky
.end