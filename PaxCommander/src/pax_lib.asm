;===============================================================================
; pax_lib
; - knihovna pomocných utilit a maker
;===============================================================================
;-------------------------------------------------------------------------------
; Přepne PMD85-3 do režimu kompatibility
;-------------------------------------------------------------------------------
pmd3To2A          ; zkopíruje monitor na adresu 8000 - 9000
                  lxi   h, 8000h
                  lxi   d, 0e000h
                  lxi   b, 1000h
                  call  copy
                  ; modifikuje první bajt na instrukci rst6, aby nedošlo ke vstupu do monitoru
                  mvi   a, 0f7h         ; rst 6
                  sta   8000h
                  ; spustí z monitoru zbytek modifikační rutiny
                  jmp   0fb73h
.trap             pop   psw             ; vytáhne návratovou adresu uloženou instrukcí rst6
                  mvi   a, 31h
                  sta   8000h           ; oprava prvního bajtu - lxi sp,
                  ; teď je nutné přenastavit registry systému, nastavit PIO a smazat obrazovku
                  ; (smazání obrazovky nastaví i kurzor, jinak nefunguje výpis při návratu do monitoru)
                  mvi   a, 0c9h
                  sta   802dh
                  call  800ah
                  mvi   a, 0cdh
                  sta   802dh
                  ret
;-------------------------------------------------------------------------------
; Tisk řetězce
; I: HL - ukazatel na nulou zakončený řetězec
;-------------------------------------------------------------------------------
printString       mov   a,m
                  inx   h
                  ana   a
                  rz
                  call  print
                  jmp   printString
;-------------------------------------------------------------------------------
; Vymaže paměť na zadané adrese
; I: HL - adresa
;    BC - délka
;-------------------------------------------------------------------------------
clearMemory       mvi   d,0
.loop             mov   m,d
                  inx   h
                  dcx   b
                  mov   a,b
                  ora   c
                  jnz   .loop
                  ret
;-------------------------------------------------------------------------------
; Kopie bloku paměti - neřeší překrytí
; I: HL cíl
;    DE zdroj
;    BC délka
;-------------------------------------------------------------------------------
copy              ldax  d
                  mov   m,a
                  inx   d
                  inx   h
                  dcx   b
                  mov   a,c
                  ora   b
                  jnz   copy
                  ret
;-------------------------------------------------------------------------------
; Výstup do monitoru
;-------------------------------------------------------------------------------
sysExit           mvi   a, CA_GREEN
                  call  setColor
                  call  clearScreen
                  ; pro případ otevřeného ptp souboru se nastaví na začátek
                  lxi   h, 0
                  mov   d,h
                  mov   e,l
                  call  sdSetFilePos
                  jmp   803eh
;-------------------------------------------------------------------------
; upCase - převede písmeno na velké písmeno
; I/O: A
;-------------------------------------------------------------------------
upCase            cpi   'a'
                  rc
                  cpi   'z'+1
                  rnc
                  ani   0ffh-020h
                  ret
;-------------------------------------------------------------------------------
; Přeskočí mezery v textu, na který ukazuje HL. Max. do znaku 0
;-------------------------------------------------------------------------------
skipWhite         mov   a,m
                  ana   a
                  rz
                  cpi   ' '
                  rnz
                  inx   h
                  jmp   skipWhite                  
;-------------------------------------------------------------------------------
; Převede 4B číslo o velikosti souboru na textové vyjádření v kB
; 15000 =>  15k
; 12    =>   1k
; max.    9999k
; I: DEBC = velikost souboru
;-------------------------------------------------------------------------------
size2string       ;  každý načnutý kb musí být vidět
                  ;  tj. pokud dolních 10 bitů v čísle je nenulových přičítáme 1000
                  mov   a,b
                  ani   3
                  ora   c
                  push  psw             ; uložíme příznak Z
                  ; dělení 1024 ... posun o 10 bitů ~ posun o 2 bity + přesun mezi registry
                  mvi   l, 2
.right            ana   a               ; CY = 0
                  mov   a,d
                  rar
                  mov   d,a
                  mov   a,e
                  rar
                  mov   e,a
                  mov   a,b
                  rar
                  mov   b,a
                  dcr   l
                  jnz   .right
                  mov   h,e
                  mov   l,b
                  pop   psw
                  jz    .z
                  inx   h               ; každý načnutý kB se přičte
.z                call  int2string
                  lxi   h, varNumber+4
                  mvi   m, 'k'
                  inx   h
                  mvi   m, 0
                  ret
;-------------------------------------------------------------------------------
; Převod čísla 0..9999 do textové podoby
; I: HL - číslo
; O: varNumber - nulou zakončený text
;-------------------------------------------------------------------------------
int2string        dad   h
                  dad   h               ; dva MSB bity mají váhu nad 10 000
                  lxi   d, 0
                  lxi   b, 0e00h        ; protože zpracováváme jen 14 bitů
.loop             dad   h               ; nejvyšší bit do CY
                  mov   a,e
                  adc   a
                  daa
                  mov   e,a
                  mov   a,d
                  adc   a
                  daa
                  mov   d,a             ; DE = 2*DE + CY včetně dek. korekce
                  dcr   b
                  jnz   .loop
                  ; v DE je teď BCD číslo, převod na ASCII
                  lxi   h, varNumber
                  call  .uplow
                  mov   d,e
                  call  .uplow
                  ; pokud to byla nula, je ji nutné zapsat
                  mov   m,b             ; zakončí řetězec nulou
                  dcr   c
                  rp
                  dcx   h
                  mvi   m, '0'
                  ret
.uplow:           mov   a,d
                  call  .upper
                  mov   a,d
                  jmp   .lower
.upper:           rrc
                  rrc
                  rrc
                  rrc
.lower:           ani   0fh
                  jnz   .nonz
                  inr   c
                  dcr   c
                  jnz   .nonz           ; nuly na začátku nahrazujeme mezerama
                  mvi   m, ' '
                  inx   h
                  ret
.nonz:            adi   30h
                  inr   c
                  mov   m,a
                  inx   h
                  ret
;-------------------------------------------------------------------------------
; Rychlé dělení 16 nebo 32
; I: HL - číslo
; O: HL - výsledek dělení
;-------------------------------------------------------------------------------                  
div32             xra   a
                  jmp   div16.x
div16             xra   a
                  dad   h
                  adc   a
.x                dad   h
                  adc   a
                  dad   h
                  adc   a
                  dad   h
                  adc   a
                  mov   l,h
                  mov   h,a
                  ret
;-------------------------------------------------------------------------------
; Převod čísla v DE do hexa vyjádření na adresu HL
;-------------------------------------------------------------------------------
de2Hex            mov   a,d
                  call  a2hex
                  mov   a,e
a2hex             mov   b,a
                  rrc
                  rrc
                  rrc
                  rrc
                  call  a2h
                  mov   a,b
a2h               ani   0fh
                  cpi   10
                  sbi   2fh
                  daa
                  mov   m,a
                  inx   h
                  ret
;-------------------------------------------------------------------------------
; číslo v A zapíše do [HL] jako  dvě hexadecimální číslice
;-------------------------------------------------------------------------------
hex2ascii         push  b
                  call  813bh
                  pop   b
                  inx   h
                  ret
;-------------------------------------------------------------------------------
; Převede hlavičku pmd souboru na text - např.: 00/? Auto
; I: C1B2 - načtená hlavička
;    HL   - kam se má výpis připravit (13 bajtů)
;-------------------------------------------------------------------------------
headPrint         lda   0c1b2h
                  push  h
                  call  8e73h           ; převod na desítkové číslo
                  pop   h
                  call  hex2ascii
                  mvi   m, '/'
                  inx   h
                  lda   0c1b3h
                  mov   m,a
                  inx   h
                  mvi   m, ' '
                  inx   h
                  mvi   c,8
                  lxi   d,0c1b8h
                  rst   1
                  ret
;-------------------------------------------------------------------------------
; Test, zda načtená hlavička představuje program s autorunem.
; Principem je, že adresa 7ffbh je v rozsahu nahrávání
; I: C1B2 - načtená hlavička
; O: CY   - pokud má autorun, je CY = 1
;-------------------------------------------------------------------------------
isAutorun         rst   5
                  mov   b,d
                  mov   c,e
                  lxi   d,7ffbh         ; adresa, na které je návratová adresa při nahrávání bloku
                  rst   4               ; HL - DE
                  rnc                   ; adr >= 7fffb - není autorun
                  dad   b               ; HL = adr + len = end
                  xchg
                  rst   4               ; 7ffb - end
                  ret
;-------------------------------------------------------------------------------
; Nalezne v názvu souboru příponu
; I: HL - ukazatel na 15B název souboru
; O: DE - ukazatel na příponu, pokud chybí, je DE = 0
;-------------------------------------------------------------------------------
findExtension     lxi   d,0             ; místo za poslední nalezenou tečkou
                  mvi   c, 15
.findext          mov   a,m
                  inx   h
                  cpi   '.'
                  jnz   .nodot
                  mov   d,h
                  mov   e,l
.nodot            dcr   c
                  jnz   .findext
                  ret
;-------------------------------------------------------------------------------
; Vrátí v HL adresu obsluhy, vybranou podle tabulky přípon
; I: DE - ukazatel na příponu
;    HL - ukazatel na tabulku:
;                  3 B - eee, 2 B - vec, zakončení 0
; O: CY=1 - přípona není v tabulce
;    CY=0, HL = načtený vektor
;-------------------------------------------------------------------------------
getExtVector      db    01h             ; lxi b,... přeskočí další dvě instrukce
.restart          inx   h
                  inx   h
                  mov   a,m
                  ana   a
                  stc                   ; CY = 1
                  rz                    ; nepodporovaná přípona
                  push  d
                  lxi   b, 3
.lp               ldax  d
                  call  upCase
                  inx   d
                  sub   m
                  inx   h
                  ora   b
                  mov   b,a
                  dcr   c
                  jnz   .lp
                  pop   d
                  ana   a
                  jnz   .restart        ; skok - další přípona
                  mov   a,m
                  inx   h
                  mov   h,m
                  mov   l,a
                  ret                   ; CY = 0
;-------------------------------------------------------------------------------
; Skočí na adresu podle přípony souboru, pokud taková přípona není, pokračuje dál
; I: DE - ukazatel na tabulku s příponama a adresama
;    HL - ukazatel na jméno souboru
;-------------------------------------------------------------------------------
switchExt         push  d
                  call  findExtension
                  pop   h
                  mov   a,d
                  ora   e
                  rz
                  call  getExtVector
                  rc
                  pchl
;-------------------------------------------------------------------------------
; Z nastaveného souboru se pokusí načíst hlavičku
; - striktně vyžaduje posloupnost několik FF, 16 x 00 a 16 x 55H na začátku
; - volá se vrReadBytes, který je nastaven buď na přímé čtení nebo čtení z ptp
;-------------------------------------------------------------------------------
loadPMDHead       lxi   b,0FFFFh         ; max. 255 x FF
                  call  .skipBytes
                  ana   a               ; posloupnost musí být ukončena 00
                  rnz
                  lxi   b, 15             ; b = 00h, c = 15 (jedna nula už byla)
                  call  .skipBytes
                  rnz
                  lxi   b,5510h           ; b = 55h, c = 16
                  call  .skipBytes
                  rnz
                  ; načte hlavičku
                  lxi   h, 0c1b2h
                  lxi   d, 13
                  jmp  vrLoadMgBlock   
.skipBytes        call  vrReadByte
                  rc
                  cmp   b
                  rnz
                  dcr   c
                  jnz   .skipBytes
                  ret
;-------------------------------------------------------------------------------
; Nastaví PMD režim
; - patchne monitor + aktivuje v načítacích rutinách odpočet začátku PTP souboru
;-------------------------------------------------------------------------------
switchToPMD       lxi   h, 0
                  lxi   d, MonitorPatches
                  shld  vrPTPCounter
                  shld  MgGetByte.record+1     
                  xchg
                  jmp   DoPatch
;-------------------------------------------------------------------------------
; Nastaví PTP režim
; - patchne monitor + aktivuje v načítacích rutinách odpočet začátku PTP souboru
;-------------------------------------------------------------------------------
switchToPTP       ; deaktivace čtení z PTP
                  lxi   h, 1
                  shld  vrPTPCounter
                  shld  MgGetByte.record+1     
                  ret
;-------------------------------------------------------------------------------
; Spustí program z pmd/ptp souboru
; - nahrávací rutinky musejí být modifikované podle režimu PMD/PTP
; - u autostartu provede modifikaci nahrávací rutiny
;-------------------------------------------------------------------------------
runProgram        ; výchozí spuštění - náhrada 8b6c a 8de2 za vlastní rutiny
                  mvi   a, 1
                  sta   varRunOptions                  
                  ; EOL + SHIFT => spustí se jako emulace verze 1
                  in    0f5h
                  ani   20h
                  cz    winOptions
                  lda   varRunOptions
                  inr   a
                  rz
                  ; načte hlavičku, ověří prompt
                  call  loadPMDHead
                  lda   0c1b3H
                  cpi   '?'
                  rnz                   ; toto není spustitelný soubor
                  ; smaže se obrazovka
                  mvi   a, CA_WHITE
                  call  setColor
                  call  clearScreen
                  ; smaže editační řádek - pro výpis hlavičky do dialogového řádku
                  call  8113h
                  ; nastavení, aby to rutinky v monitoru sežraly, číslo =0, prompt = '?'
                  lxi   h, 3f00h
                  shld  0c1b0h
                  lxi   sp, 7fffh
                  lhld  0c1b4h
                  push  h
                  push  h               ; návratová adresa je začátek nahraného bloku
                  ; výpis pmd hlavičky do dialogového řádku
                  lxi   h, txtInfo
                  push  h
                  call  headPrint
                  pop   d
                  lhld  0c030h          ; místo textu pro dialogový řádek
                  mvi   c, 13
                  rst   1
                  call  8855h
                  ; pokud má hra autorun, načte se a patchne se
                  lxi   sp, 7fffh
                  lhld  0c1b4h
                  push  h
                  push  h               ; návratová adresa je začátek nahraného bloku
                  call  isAutorun
                  jc    .autorun
                  lda   varRunOptions
                  cpi   4
                  jz    emustart   
                  jmp   vrLoadMgfAndRun
                  ; načte loader
.autorun          lxi   sp, 0ffc0h     ; zde autoloadery nezasahují
                  rst   5
                  push  d
                  lxi   d, ram_protected
                  rst   4
                  pop   d
                  jc    vrLoadMgfAndRun
                  
                  call  vrLoadMgBlock
                  rnz
                  ; spuštění v emulaci?
                  lda   varRunOptions
                  cpi   4
                  jz    emustart   
                  ; hack autorun: HL  (co se má nahradit) => DE (čím se má nahradit)
                  ani   1
                  jz    .nohack8b6c
                  
                  lxi   h, 8b6ch
                  lxi   d, vrReadByte
                  call  hackLoader

                  lxi   h, 0eb6ch
                  lxi   d, vrReadByte
                  call  hackLoader

                  lxi   h, 8dc2h
                  lxi   d, vrLoadMgBlock
                  call  hackLoader
                  
                  lxi   h, 0edc2h
                  lxi   d, vrLoadMgBlock
                  call  hackLoader
                  
.nohack8b6c       lda   varRunOptions
                  ani   2                  
                  lxi   h, 0f7d3h
                  lxi   d, 0000
                  cnz   hackLoader

                  mvi   c, 3fh
                  xra   a
                  lxi   sp, 7ffbh
                  ret
;-------------------------------------------------------------------------------
; Provede hack - náhrada dvou bajtů jinými
; I: HL - co se nahrazuje
;    DE - čím se nahrazuje
;    c1b2 - načtená hlavička
;-------------------------------------------------------------------------------                  
hackLoader        mov   a,l
                  sta   .f1 + 1
                  mov   a,h
                  sta   .f2 + 1
                  mov   a,e
                  sta   .r1 + 1
                  mov   a,d
                  sta   .r2 + 1
                  rst   5
.loop             mov   a,m
                  inx   h
.f1               cpi   6ch
                  jnz   .nexthack
                  mov   a,m
.f2               cpi   8bh
                  jnz   .nexthack
                  dcx   h
.r1               mvi   m, vrReadByte & 255
                  inx   h
.r2               mvi   m, vrReadByte >> 8
.nexthack         dcx   d
                  mov   a,d
                  ora   e
                  jnz   .loop
                  ret                  