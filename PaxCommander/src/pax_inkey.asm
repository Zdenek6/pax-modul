;===============================================================================
; pax_inkey
; - vstup z klávesnice
;===============================================================================
;-------------------------------------------------------------------------------
; kódy kláves
;-------------------------------------------------------------------------------
KEY_SHIFT_CLR     equ   1
KEY_END           equ   3
KEY_SHIFT_C_D     equ   6
KEY_SHIFT_WRK     equ   7
KEY_LEFT          equ   8
KEY_SHIFT_NEXT    equ   9
KEY_SHIFT_PREV    equ   10
KEY_WRK           equ   11
KEY_HOME          equ   12
KEY_EOL           equ   13
KEY_SHIFT_RCL     equ   14
KEY_SHIFT_INS     equ   15
KEY_SHIFT_END     equ   16
KEY_SHIFT_RIGHT   equ   17
KEY_SHIFT_DEL     equ   18
KEY_SHIFT_LEFT    equ   19
KEY_RCL           equ   23
KEY_RIGHT         equ   24
KEY_NEXT          equ   25
KEY_PREV          equ   26
KEY_CLR           equ   27
KEY_INS           equ   28
KEY_DEL           equ   29
KEY_C_D           equ   30
KEY_SHIFT_HOME    equ   31
KEY_K0            equ   88h
KEY_K1            equ   89h
KEY_K2            equ   8Ah
KEY_K3            equ   8Bh
KEY_K4            equ   8Ch
KEY_K5            equ   8Dh
KEY_K6            equ   8Eh
KEY_K7            equ   8Fh
KEY_K8            equ   90h
KEY_K9            equ   91h
KEY_K10           equ   92h
KEY_K11           equ   93h
KEY_SHIFT_K0      equ   94h
KEY_SHIFT_K1      equ   95h
KEY_SHIFT_K2      equ   96h
KEY_SHIFT_K3      equ   97h
KEY_SHIFT_K4      equ   98h
KEY_SHIFT_K5      equ   99h
KEY_SHIFT_K6      equ   9Ah
KEY_SHIFT_K7      equ   9Bh
KEY_SHIFT_K8      equ   9Ch
KEY_SHIFT_K9      equ   9Dh
KEY_SHIFT_K10     equ   9Eh
KEY_SHIFT_K11     equ   9Fh
;-------------------------------------------------------------------------------
; následující proměnné musejí být v paměti přesně v uvedeném pořadí!
;-------------------------------------------------------------------------------
xkey_last         db    0               ; poslední kód klávesy získaný funkcí xkey_scan
xkey_first        db    100             ; doba po prvním stisku
xkey_next         db    20              ; doba při autorepeatu
xkey_timer        db    0               ; počítadlo doby před autorepeaterem
;-------------------------------------------------------------------------------
; Čtení z klávesnice s autorepeatem
;-------------------------------------------------------------------------------
xkey_inkey        push  h
                  push  d
                  push  b
                  lxi   h, .ret
                  push  h
                  call  xkey_scan
                  mov   a,d
                  lxi   h, xkey_last
                  cmp   m
                  mov   m,a
                  inx   h
                  jnz   .newkey
                  ; stejná klávesa
                  inx   h
                  mov   e,m             ; E = xkey_next
                  inx   h
                  dcr   m
                  jz    .setup
                  xra   a               ; zatím neuběhl čas pro opětovné generování
                  ret
.newkey           ; jiná klávesa
                  mov   e,m             ; E = xkey_first
                  inx   h
                  inx   h
.setup            mov   m,e
                  ret
.ret              pop   b
                  pop   d
                  pop   h
                  ret
;-------------------------------------------------------------------------------
; Sken klávesnice
; OUT: D - 0.... nic není stlačeno
;          <>0.. kód klávesy, zahrnuje i modifikaci shiftem
;-------------------------------------------------------------------------------
xkey_scan         lxi   d, 0            ; D = kód klávesy, E = počítadlo sloupců
                  ; nejprve test na shift
                  lxi   h, xkeymap
                  in    0f5h
                  ani   20h
                  jnz   .loop
                  lxi   h, xkeymap_shift
.loop             mov   a, e
                  inr   e
                  out   0f4h
                  cpi   16
                  rz
                  in    0f5h
                  mvi   b,5
.lp               rrc
                  jc    .skip
                  mov   d,m
.skip             inx   h
                  dcr   b
                  jnz   .lp
                  jmp   .loop
;-------------------------------------------------------------------------------
; Kódy kláves seřazené podle sloupce od horní ke spodní klávese
;-------------------------------------------------------------------------------
xkeymap           db    88h, '1', 'Q', 'A', ' '
                  db    89h, '2', 'W', 'S', 'Y'
                  db    8Ah, '3', 'E', 'D', 'X'
                  db    8Bh, '4', 'R', 'F', 'C'
                  db    8Ch, '5', 'T', 'G', 'V'
                  db    8Dh, '6', 'Z', 'H', 'B'
                  db    8Eh, '7', 'U', 'J', 'N'
                  db    8Fh, '8', 'I', 'K', 'M'
                  db    90h, '9', 'O', 'L', ','
                  db    91h, '0', 'P', 3bh, '.'
                  db    92h, '_', '@', ':', '/'
                  db    93h, '}', '\\', ']', ' '
                  db    0bh, 1ch, 08h, 1ah, ' '
                  db    1eh, 1dh, 0ch, 03h, 0dh
                  db    17h, 1bh, 18h, 19h, 0dh
xkeymap_shift     db    94h, '!', 'q', 'a', ' '
                  db    95h, '"', 'w', 's', 'y'
                  db    96h, '#', 'e', 'd', 'x'
                  db    97h, '$', 'r', 'f', 'c'
                  db    98h, '%', 't', 'g', 'v'
                  db    99h, '&', 'z', 'h', 'b'
                  db    9Ah, '\H', 'u', 'j', 'n'
                  db    9Bh, '(', 'i', 'k', 'm'
                  db    9Ch, ')', 'o', 'l', '<'
                  db    9Dh, '-', 'p', '+', '>'
                  db    9Eh, '=', '`', '*', '?'
                  db    9Fh, '{', '^', '[', ' '
                  db    07h, 0fh, 13h, 0ah, ' '
                  db    06h, 12h, 1fh, 10h, 0dh
                  db    0Eh, 01h, 11h, 09h, 0dh