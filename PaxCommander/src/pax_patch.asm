;===============================================================================
; pax_patch
; - úpravy monitoru tak, aby spolupracoval s PAX modulem
;===============================================================================
; Vytipovaná místa, kam se dá vměstnat nový kód:
; 816E - 81D7 (106B) - smyčka terminálu
; 8404 - 8412 ( 15B) - text "++ Mgf stop!++",CR
; 848D - 8492 (  6B) - přijetí bajtu z mgf - jen push a skok do 8B6C, používá jen 8ebe
; 85F6 - 85FF ( 10B) - načtení bajtu ze sériové linky
; 886B - 887F ( 21B) - zápis 16 shodných bajtů na mgf + načtení ze sér. linky
; 8890 - 88A2 ( 19B) - čtení bitu z mgf a čekání na změnu
; 8B1C - 8B40 ( 37B) - vyslání bloku 128 bajtů na sériovou linku
; 8B6C - 8B9B ( 48B) - načtení z mgf, začátek a push/pop musejí zůstat!
; 8BD8 - 8BED ( 22B) - mazání paměti, pokud je 2A na C1F1
; 8C74 - 8C7C (  9B) - test stisku klávesy stop - používá pouze DUMP, předěláno na vrácení testu v CY
; 8D6C - 8B7D ( 18B) - zápis bloku dat na mgf
; 8DE2 - 8E40 ( 97B) - načtení celého souboru z mgf, od hlavičky až po blok dat.
;                    - porovná shodu s C1B0, vypisuje názvy do řádku
; 8E7F - 8EBD ( 63B) - MGSV (analýza přík. řádku)
; 8EBE - 8F28 (107B) - načtení hlavičky souboru z mgf
; 8F60 - 8F93 ( 52B) - zápis souboru (hlavičky + data) na mgf
; 8FF1 - 8FFF ( 15B) - načtení bloku dat ze sériové linky
;
; Celkem:      636B
;===============================================================================
ENTRY_NAME        equ   0c330h          ; pozice, kam se uloží jméno načteného souboru
;-------------------------------------------------------------------------------
; Makro pro snadnou definici patche
;
; Příklad užití:
;                 PATCH 8000h
;   label         dcr a
;                 jnz label
;                 PATCH_END
;
; Generuje
;                 db    3               ; délka patche jsou 3 bajty
;                 dw    8000h           ; bude umístěn na 8000h
;                 dcr   a
;                 jnz   8000h           ; labely se překládájí pro uvedené umístění
;
; Kontrola, zda patch nevybočil z vymezeného prostoru
;                 PATCH 8000H
;                 ...
;                 PATCH_CHECK 8030h     ; patch nesmí přesáhnout adresu 8030 výše
;-------------------------------------------------------------------------------
PATCH             macro adr
                  db    + - $ - 3
                  dw    adr
                  phase adr
                  endm
PATCH_END         macro
                  dephase
+                 label $
                  endm
PATCH_CHECK       macro top
                  if    $ > top
                  error "\{$} patch exceed limit"
                  endif
                  dephase
+                 label $
                  endm
;-------------------------------------------------------------------------------
; Modifikuje paměť seznamem patchů
; I: HL - ukazatel na seznam patchů
;-------------------------------------------------------------------------------
DoPatch           mov   a, m
                  ana   a
                  rz
                  mov   c, a            ; C = délka
                  inx   h
                  mov   e, m
                  inx   h
                  mov   d, m            ; DE = umístění
                  inx   h
                  xchg
                  rst   1               ; copy8
                  xchg
                  jmp   DoPatch
;===============================================================================
; Seznam patchů
;===============================================================================
MonitorPatches
;-------------------------------------------------------------------------------
; Aby se v all ram režimu neničil font písmene 'A' po stisku K0-K12, pouze pro PMD85-2A
;-------------------------------------------------------------------------------
                  PATCH 8832h
                  db    3ah
                  PATCH_END
;-------------------------------------------------------------------------------
; úprava hlášky - max. 15B!
;-------------------------------------------------------------------------------
                  PATCH 8427h
                  db    "PAX sys "
                  PATCH_CHECK 8433h
;-------------------------------------------------------------------------------
; STOP - původní kód vracel nastavený Z, novější vrací i nastavený C
;-------------------------------------------------------------------------------
                  PATCH 8C74H
sysStop           in    0f5h
                  ani   40h             ; nastaví Z = 0, pokud je STOP stisknuto
                  cma
                  rlc
                  rlc                   ; nastaví C = 1, pokud je STOP stisknuto
                  ret
                  PATCH_CHECK 8C7Ch
;-------------------------------------------------------------------------------
; Rutina pro načítání dat z PAX modulu
; - stejná rutina pro verzi emulace PMD i PTP
;-------------------------------------------------------------------------------
                  PATCH 8B6Ch
MgGetByte         push  b               ; tuto instrukci testují některé hry
                  push  h
.record           lxi   h,0             ; odpočet bloku v ptp souboru, pokud je 0000, nejedná se o ptp
                  mov   a,l
                  ora   h                  
                  jnz   ReloadPTPLength                  
.cont             call  paxCmdNextByte
                  pop   h
                  pop   b
                  ret
; přečte další bajt ze streamu, bez timeoutu, s testem STOP 
paxCmdNextByte    mvi   a, CMD_NEXT_BYTE                 
                  out   0f8h
.loop             call  sysStop
                  rc
                  in    0fbh
                  inr   a
                  jz    .loop           ; FF - busy
                  dcr   a
                  stc
                  rnz                  ; <> 0, CY = 1 => chyba
                  in    0f8h
                  ana   a
                  ret                   ; CY = 0, A = přečtený bajt
                  PATCH_CHECK 8B9Ch
;-------------------------------------------------------------------------------
; Pro PTP soubory je nutné hlídat začátek bloku, která obsahuje 2B délku
; - leží na místě mazání paměti, pokud je na C1F1 hodnota 2A
;-------------------------------------------------------------------------------
                  PATCH 8bd8h
                  ret                   ; případné mazání paměti po resetu se ukončí
ReloadPTPLength   dcx   h
                  mov   a,h
                  ora   l
                  jnz    .save
                  call  paxCmdNextByte
                  mov   l,a
                  call  paxCmdNextByte
                  mov   h,a
.save             shld  MgGetByte.record+1
                  jmp   MGGetByte.cont                                    
                  PATCH_CHECK 8BEDh
;-------------------------------------------------------------------------------
; Rutina pro detekci pilotního tónu a načtení hlavičky
; - verze pro procházení PMD souborů v aktuálním adresáři
;-------------------------------------------------------------------------------
                  PATCH 8EBEH
HeadIn            call  MgGetByte       ; načte jeden bajt
                  jnc   .load
                  ; chyba konce souboru?
                  cpi   errEOF
                  jz    .next
                  stc
                  ret   ; ne, jiná chyba (např. stisknutý stop)
.load             inr   a               ; test na FF
                  ; pokusí se načíst hlavičku
                  cz    LoadHead
                  rz    ; vše ok
                  ; skok na další soubor                  
.next             lhld  MgGetByte.record+1
                  mov   a,h
                  ora   l
                  jnz   PTPnext        ; PTP režim
                  mvi   a, CMD_DIR_NEXT
                  db    1               ; lxi b,
.dirstart         mvi   a, CMD_DIR_START
                  call  paxCmd
                  rc
                  call  MgGetByte
                  cpi   255
                  jz    .dirstart       ; konec výpisu, restart- emulace pásky je nekonečná smyčka
                  ani   10h
                  jnz   .next           ; adresáře přeskakujeme   
                  ; přeskočí 19 bajtů
                  mvi   c, 19
.skip             call  MgGetByte
                  dcr   c
                  jnz   .skip
                  jmp   HeadIn  

                  PATCH_CHECK 8effh

; následující sdílí PMD i PTP režim, je proto odtlačen až na konec původního místa rutiny 8ebe
                  PATCH 8effh
                  ; pokusí se načíst hlavičku
                  ; striktně vyžaduje posloupnost několik x FF, 16 x 00 a 16 x 55H na začátku
LoadHead          lxi   b,0FFFFh         ; max. 255 x FF
                  call  SkipBytes
                  ana   a               ; posloupnost FF musí být ukončena 00
                  rnz
                  lxi   b, 15             ; b = 00h, c = 15 (jedna už byla)
                  call  SkipBytes
                  rnz
                  lxi   b,5510h           ; b = 55h, c = 16
                  call  SkipBytes
                  rnz
                  ; načte hlavičku
                  lxi   h, 0c1b2h
                  lxi   d, 13
                  jmp   8dc2h             
SkipBytes         call  MgGetByte
                  rc
                  cmp   b
                  rnz
                  dcr   c
                  jnz   SkipBytes
p_end             ret

                  warning "Patch end: \{p_end}H"           
                  PATCH_CHECK 8F29h
;-------------------------------------------------------------------------------
; Posune se dál v PTP souboru
;-------------------------------------------------------------------------------
                  PATCH 816Eh
PTPnext           call  MgGetByte
                  jc    .rewind
                  dcx   h
                  mov   a,h
                  ora   l
                  jnz   PTPnext
                  jmp   HeadIn
.rewind           cpi   errEOF
                  stc
                  rnz       
                  lxi   h, 1   
                  shld  MgGetByte.record+1
                  call  resetFile                  
                  jmp   HeadIn
;-------------------------------------------------------------------------------
; přetočení PTP souboru na začátek souboru
;-------------------------------------------------------------------------------
resetFile         lxi   h,0
                  mov   d,h
                  mov   e,l
;-------------------------------------------------------------------------------
; Nastaví pozici v souboru
; I: DEHL = 4B pozice v souboru (HL = low)
;-------------------------------------------------------------------------------
setFilePos        mvi   a, CMD_SEEK
                  call  paxCmd
                  cpi   127
                  rnz
                  mvi   c,4
.loop             mov   a,d
                  mov   d,e
                  mov   e,h
                  mov   h,l
                  call  paxCmd
                  cpi   127
                  rnz
                  dcr   c
                  jnz   .loop
                  xra   a
                  out   0fbh
                  ret                    
                  PATCH_CHECK 81B4h
;-------------------------------------------------------------------------------
;
;-------------------------------------------------------------------------------
                  PATCH  8FF1h
                  ret
; provede příkaz s vrácením hodnoty
paxCmd            out   0f8h
.wait             in    0fbh
                  add   a                  
                  jc    .wait
                  rar
                  rz
                  stc
                  ret                    
                  PATCH_CHECK 9000h
                  db    0               ; zakončení seznamu                                   