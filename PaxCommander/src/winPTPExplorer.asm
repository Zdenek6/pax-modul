;===============================================================================
; winPTPExplorer
; - průzkumník PTP souboru
;===============================================================================
;-------------------------------------------------------------------------------
; Sestaví seznam hlavičkových souborů v ptp souboru
; I: HL - ukazatel na jméno ptp souboru
;-------------------------------------------------------------------------------
ptpBuild          call  sdFind
                  rc                    ; tento soubor nebyl nalezen
                  ; vynuluje pozici v souboru
                  lxi   h,0
                  shld  varFilePos
                  shld  varFilePos+2
                  shld  varCurRecord
                  shld  _ptplist.index  ; index = 0, počet = 0
                  lxi   h, varListItems
                  shld  varCurListPtr
                  ; načte velikost záznamu
.ptploop          call  vrReadByte
                  cmc
                  rnc                   ; už není, co číst, vrací se, ale s CY = 0
                  mov   c,a
                  call  vrReadByte
                  mov   b,a
                  ana   a
                  jnz   .nextrecord
                  mov   a,c
                  cpi   3fh
                  jnz   .nextrecord
                  ; velikost je 63, to by mohla být hlavička
                  push  b
                  call  loadPMDHead
                  pop   b
                  jnz   .nextrecord
                  ; přidá tuto hlavičku do seznamu
                  push  b
                  ; zapíše hlavičku - 4B pozice v souboru, číslo, prompt, jméno 8B
                  ; převede PMD hlavičku na text
                  lhld  varCurListPtr
                  lxi   d, varFilePos
                  mvi   c, 4
                  rst   1               ; kopie adresy v ptp souboru
                  call  headPrint       ; převede hlavičku na text
                  mvi   m, 0            ; zakončí text 0
                  inx   h
                  shld  varCurListPtr
                  lxi   h, _ptplist.count
                  inr   m
                  pop   b
.nextrecord       ; nastaví pozici do souboru na další
                  lhld  varCurRecord
                  inx   h
                  shld  varCurRecord
                  inx   b
                  inx   b
                  lhld  varFilePos
                  dad   b
                  shld  varFilePos
                  jnc   .skipaddhigh
                  lhld  varFilePos+2
                  inx   h
                  shld  varFilePos+2
.skipaddhigh      ; odešle nastavení pozice
                  lhld  varFilePos+2
                  xchg
                  lhld  varFilePos
                  call  sdSetFilePos
                  jmp   .ptploop
;-------------------------------------------------------------------------------
; Spuštění okna
;-------------------------------------------------------------------------------
winPTP            ; kopie názvu PTP souboru do titulku
                  xchg
                  lxi   h, ptpTitle.name
                  mvi   c, 15
                  rst   1
                  ; vyhledání hlavičkových souborů v ptp souboru¨
                  lxi   h, ptpTitle.name
                  call  ptpBuild
                  rc
                  ; přednastavení rutiny pro čtení zPTP souboru
                  call  switchToPTP
                  ; hlavní smyčka tohoto okna
.start            lxi   h, ptpDesc
                  call  winLayout
                  lxi   h, _ptplist
                  call  lb_Init
.loop             rst   7
                  lxi   h, .loop
                  push  h
                  rz
                  cpi   KEY_NEXT
                  jz    lb_Next
                  cpi   KEY_PREV
                  jz    lb_Prev
                  cpi   KEY_HOME
                  jz    lb_Home
                  cpi   KEY_END
                  jz    lb_End
                  cpi   KEY_EOL
                  jz    ptpOpenFile
                  cpi   KEY_LEFT
                  rnz
                  pop   psw
                  ret
;-------------------------------------------------------------------------------
; Otevře soubor v PTP souboru
; - nastaví ptp soubor jako pásku (provede patch monitoru)
; - nastaví pozici na vybraný soubor
; - zahájí načítání z monitoru
;-------------------------------------------------------------------------------
ptpOpenFile       ; nastavení pozice vybraného záznamu
                  lda   varListIndex
                  call  cbPTPList
                  dcx   h
                  mov   d,m
                  dcx   h
                  mov   e,m
                  dcx   h
                  mov   a,m
                  dcx   h
                  mov   l,m
                  mov   h,a
                  call  sdSetFilePos
                  call  lb_push
                  call  runProgram
                  call  lb_pop
                  lxi   h, ptpDesc
                  call  winLayout                  
                  jmp   lb_Refresh

;-------------------------------------------------------------------------------
; Callback pro seznam s PTP soubory
;-------------------------------------------------------------------------------
cbPTPList         lxi   h, varListCount
                  cmp   m
                  cmc
                  rc
                  ; *18 + varListItems
                  mov   l,a
                  mvi   h,0
                  mov   e,l
                  mov   d,h
                  dad   h               ; *2
                  dad   h               ; *4
                  dad   h               ; *8
                  dad   d               ; *9
                  dad   h               ; *18
                  lxi   d, varListItems+4
                  dad   d
                  mvi   a, CA_WHITE
                  ret
;-------------------------------------------------------------------------------
; Popis okna PTP průzkumníku
;-------------------------------------------------------------------------------
ptpDesc           PAXWIN 21, 14, ptpTitle

                  ; listbox
                  db    CA_CYAN + CA_FILL + CA_BORDER
                  db    _x+1, _y+1
                  db    _w-2, 12*10
                  dw    0

                  db    255
ptpTitle          db    "PTP: "
.name             db    "               ", 0
ptpFileText       ds    13

;-------------------------------------------------------------------------------
; Inicializace seznamu
;-------------------------------------------------------------------------------
_ptplist          dw    cbPTPList       ; callback
.pos              dw    (_y+1)*64*10 +_x +0c000h ; umístění seznamu ve videoram
.index            db    0               ; index vybrané položky
.count            db    12              ; počet položek
                  db    0               ; index první viditelné položky
                  db    12              ; počet viditelných položek
                  db    CA_WHITE        ; barva položky
                  db    CA_YELLOW+CA_FILL ; barvy vybrané položky
                  db    _w-2            ; šířka položky
.end