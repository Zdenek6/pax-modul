;===============================================================================
; winUART
; - okno s vyobrazením přenosu dat po UARTu
;===============================================================================
varUartAdr        equ   uartTransfer.adr+1
varUartLen        equ   uartTransfer.len+1
varUartDelta      equ   uartTransfer.delta+1;-----------------------------------
; Spuštění okna, už musí být načtena adresa a délka očekávaných dat
;-------------------------------------------------------------------------------
winUart           lhld  varUartAdr
                  xchg
                  lxi   h, uartTitle.from
                  call  de2Hex
                  lhld  varUartLen
                  dad   d
                  xchg
                  lxi   h, uartTitle.to
                  call  de2Hex
                  ; výpočet okamžiku přírustku progressbaru: length/32
                  lhld  varUartLen
                  call  div32
                  shld  varUartDelta
                  lxi   h, uartDesc
                  call  winLayout
                  lhld  varCursor
                  lxi   d, -1
                  dad   d
                  shld  updateProgress2+1                  
                  ; příprava zásobníku
                  lhld  varUartAdr
                  xchg
                  lxi   h, 8000h
                  sphl                  
                  lxi   b, 803eh
                  push  d               ; aby se spuštěná aplikace vrátila do systému
                  push  h               ; kam skočí nahrávací rutina, pokud dojde k chybě
                  jmp   uartTransfer
;-------------------------------------------------------------------------------                  
; Zpracování vstupního příkazu          
;-------------------------------------------------------------------------------
uartService       call  uartReadByte
                  rc
                  cpi   '#'
                  rnz
                  call  uartGetByte
                  rc
                  cpi   'I'
                  jz    uartIdentify
                  cpi   'L'
                  rnz
                  call  uartGetWord
                  rc
                  shld  varUartAdr
                  call  uartGetWord
                  rc
                  shld  varUartLen
                  jmp   winUart
; příkaz identifikace - odešle se #VPAX1
uartIdentify      lxi   h, .idtext
                  mvi   c, 6+1
                  mvi   a, CMD_UART_SEND
.loop             call  vrPaxCmd
                  cpi   127
                  rnz
                  mov   a,m
                  inx   h
                  dcr   c
                  jnz   .loop
                  xra   a
                  out   0fbh
                  ret                  
.idtext           db    "#VPAX1"
; načte 2 bajty
uartgetword       call  uartGetByte
                  rc
                  mov   l,a
                  call  uartGetByte
                  mov   h,a
                  ret                     
;-------------------------------------------------------------------------------
; 
;-------------------------------------------------------------------------------
uartDesc          PAXWIN  34, 3, uartTitle                        
                  

                  ; progressbar
                  db    CA_GREEN + CA_FILL + CA_PATTERN + CA_BORDER
                  db    _x+1, _y+1
                  db    32, 12
                  dw    0

                  db    255
uartTitle         db    "Prenos dat "
.from             db    "0000 - "
.to               db    "1234",0