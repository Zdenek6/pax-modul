;===============================================================================
; winROM
; - okno pro upload souboru do ROM modulu
;===============================================================================
winROM            call  sdFind          ; nalezne soubor/načte entry
                  rc                    ; nenalezeno - návrat   
                  ; ukazatel na jméno souboru
                  lxi   h, varEntry.name                  
                  shld  romNamePtr
                  lhld  varEntry.size
                  shld  .length+1
                  lxi   h, romDesc
                  call  winLayout
.loop             rst   7
                  cpi   KEY_K0
                  rz
                  cpi   KEY_EOL
                  jnz   .loop
                  ; spuštění zápisu do ROM modulu
                  ; - nejprve se vynuluje počáteční adresa
                  xra   a
                  out   0f9h
                  xra   a
                  out   0fah
.length           lxi   d, 0   
                  ; výpočet kroku progressu = length+15 / 16
                  mov   h,d
                  mov   a,e
                  ani   0f0h
                  mov   l,a
                  dcx   h
                  call  div16
                  shld  .delta+1
                  lhld  varCursor
                  lxi   b, -1
                  dad   b
                  shld  updateProgress2+1  
.delta            lxi   h, 0
.wrdata           call  vrNextByte
                  cc    .e
                  mov   c, a
                  mvi   a, 20h
                  call  vrPaxCmd
                  cc   .e               ; jiná chyba, nejspíš přetečení
                  mov   a,c
                  call  vrPaxCmd
                  cc  .e                ; jiná chyba, nejspíš přetečení
                  dcx   d
                  mov   a,d
                  ora   e
                  rz
                  ; kontrola progressu
                  dcx   h
                  mov   a,h
                  ora   l
                  jnz   .wrdata
                  ; update progressu
                  call  updateProgress             
                  jmp   .delta
.e                sui   127
                  rz
                  ; nějaká chyba při zápisu
                  mvi   a,'X'
                  call  print
.wk               call  xkey_inkey
                  ana   a
                  jz    .wk
                  pop   psw
                  ret
;-------------------------------------------------------------------------------
; Popis okna ROM
;-------------------------------------------------------------------------------
romDesc           PAXWIN 25, 9, romTitle

                  ; text 
                  db    CA_WHITE + CA_FILL
                  db    _x+0, _y+1
                  db    _w-2, 10
                  dw    romMenu

                  ; text s názvem ROM souboru
                  db    CA_WHITE + CA_FILL
                  db    _x + 7, _y+1
                  db    17, 10
romNamePtr        dw    0

                  ; EOL
                  db    CA_RED + CA_FILL + CA_BORDER
                  db    _x+1, _y+3
                  db    5, 10
                  dw    romEOL
                  ; 
                  db    CA_WHITE + CA_FILL
                  db    _x+7, _y+3
                  db    15, 10
                  dw    romJustRom

                  ; K0
                  db    CA_CYAN + CA_FILL + CA_BORDER
                  db    _x+1, _y+5
                  db    5, 10
                  dw    romK0
                  ; 
                  db    CA_WHITE + CA_FILL
                  db    _x+7, _y+5
                  db    15, 10
                  dw    romCancel
                
                  ; Progress
                  db    CA_CYAN + CA_BORDER
                  db    _x+4, _y+7
                  db    16, 12
                  dw    0

                  db    255
romTitle          db    "ROM Manager ",0
romMenu           db    "Soubor",0
romEOL            db    "EOL",0
romJustRom        db    "nahradit rom", 0                  
romK0             db    "K0", 0
romCancel         db    "zrusit", 0
