;===============================================================================
; pax_gfx
; - grafické utilitky
;===============================================================================
;-------------------------------------------------------------------------------
; Makro pro lehčí specifikaci okna
;   PAXWIN  width, height, text
;-------------------------------------------------------------------------------
PAXWIN            macro width, height, textptr
_x                set   (50-width) / 2
_y                set   (25 - height) / 2
_w                set   width
_h                set   height
                  ; stín - vpravo
                  db    CA_WHITE
                  db    _x + width, _y
                  db    1, (height+1)*10
                  dw    0

                  ; stín dole
                  db    CA_WHITE
                  db    _x + 1, _y + height
                  db    width, 10
                  dw    0
                  
                  ; panel okna
                  db    CA_WHITE + CA_FILL + CA_BORDER
                  db    _x, _y-1
                  db    width, (height + 1)*10
                  dw    0                                   
                  
                  ; titulek okna
                  db    CA_CYAN + CA_FILL + CA_BORDER
                  db    _x, _y-1
                  db    width, 10
                  dw    textptr

                  if    0
                  ; test: _x, _y, _w, _h*10 musí vyplnit celý uživatelský prostor okna - bez titulku
                  db    CA_RED + CA_FILL + CA_PATTERN
                  db    _x, _y
                  db    _w, _h*10
                  dw    0
                  endif
                  
                  endm
;-------------------------------------------------------------------------------
; Barvy  pro COLORACE
;         COLORACE:  0-bílá
;                    1-červená
;                    2-zelená
;                    4-modrá
;                   +8-včetně vyplnění pozadí
;                  +16-mřížka
;                 +128-ohraničení okolo   
;-------------------------------------------------------------------------------
CA_RED            equ   1
CA_GREEN          equ   2
CA_YELLOW         equ   3
CA_BLUE           equ   4
CA_MAGENTA        equ   5
CA_CYAN           equ   6
CA_WHITE          equ   7
CA_FILL           equ   8
CA_PATTERN        equ   16
CA_BORDER         equ   128
;-------------------------------------------------------------------------------
; setColor - nastaví atributy podle zvolené barvy a režimu
; Vstup:  A - 0..7 kód barvy
;         COLORACE:  0-transparentní
;                    1-červená
;                    2-zelená
;                    4-modrá
;                   +8-včetně vyplnění pozadí
;                  +16-mřížka
;                 +128-ohraničení okolo
;         ostatní barvy kombinace součtem: 6 = 2+4=zelená+modrá=azurová
; Výstup: barvy uložené na adrese varColor
;------------------------------------------------------------------------------
setColor          push  h
                  push  d
                  push  psw
                  sta   varCurrentColor
                  ani   7
                  mov   c,a
                  mvi   b,0
                  lda   varColorMode
                  ana   a
                  mov   d,a
                  mov   e,a
                  jz    .m0       ; pro monochromatický režim jsou atributy vždy nulové
                  lxi   h, .grayscale 
                  dcr   a
                  jz    .skip
                  lxi   h, .colors
.skip             dad   b
                  dad   b
                  mov   d,m
                  inx   h
                  mov   e,m
.m0               pop   psw
                  ani   7fh
                  cpi   8
                  jc    .l
                  lxi   h,3f3fh
                  cpi   16
                  jc    .ll
                  lxi   h,2a15h
.ll               mov   a,d
                  xra   h
                  mov   d,a
                  mov   a,e
                  xra   l
                  mov   e,a
.l                xchg
                  db    3eh             ; MVI A - přeskočí další instrukci pop psw
.setcolor         pop   psw
                  shld  varColor
                  pop   d
                  pop   h
                  ret                   
; tabulka pro stupně šedi/4 barevný režim
.grayscale:       dw    0000h
                  dw    4040h
                  dw    8080h
                  dw    4040h
                  dw    08080h
                  dw    8080h
                  dw    0c0c0h
                  dw    0000h
; atributy jednotlivých barev pro colorace
.colors           dw    0000h
                  dw    4040h
                  dw    0000h
                  dw    0040h
                  dw    8080h
                  dw    4080h
                  dw    0080h
                  dw    00c0h   
;-------------------------------------------------------------------------------
; clearScreen - rychlé vyplnění obrazovky zvolenou barvou (varColor)
;-------------------------------------------------------------------------------
clearScreen       di
                  lxi   h, 0
                  dad   sp
                  shld  .sp+1
                  lhld  varColor
                  xchg
                  lxi   sp,0c030h
.loop             lxi   h, 64+48
                  mov   b,d
                  mov   c,d
                  mvi   a,8
.pl               rept  24/8
                  push  b
                  endm
                  dcr   a
                  jnz   .pl
                  mov   a,d
                  mov   d,e
                  mov   e,a
                  dad   sp
                  sphl
                  jnc   .loop
.sp               lxi   sp,0                  
                  ei
                  ret                  
;-------------------------------------------------------------
; fillRect - vyplní rámeček předvolenou barvou
; I:    HL - pointer
;       BC - X, Y
;-------------------------------------------------------------
fillRect          call  _rect
                  lda   varCurrentColor
                  ani   CA_BORDER
                  rz
;-------------------------------------------------------------
; drawRect - kreslí rámeček (metoda AND)
; I:    HL - pointer
;       BC - X, Y
;-------------------------------------------------------------
drawRect          push  h
                  push  b
                  lxi   d, - 64
                  dad   d
                  inr   c
                  inr   c
                  call  .dr         
.pp               pop   b
                  pop   h
                  ret
.dr               push  h                                   
                  mov   e,b
                  call  .hline
                  mvi   a,255-1;20h
.vl1              call  .vline
                  pop   h       
                  dcx   h           
                  mvi   a,255-20h
                  dcr   c
.vl2              call  .vline
                  mov   a,m
                  ani   255-20h
                  mov   m,a
                  inx   h
                  mov   e,b
.hline            mvi   d, 0c0h
.hl               mov   a,m
                  ana   d
                  mov   m,a
                  inx   h
                  dcr   e
                  jnz   .hl
                  ret
.vline            lxi   d,64
                  push  b
                  mov   b,a
.vl               mov   a,m
                  ana   b
                  mov   m,a
                  dad   d
                  dcr   c
                  jnz   .vl
                  pop   b
                  ret        

;-------------------------------------------------------------
; _rect - vyplní obdélník nastavenou barvou
; I:    HL - pointer
;       BC - X, Y
;-------------------------------------------------------------
_rect             push  h
                  push  b
                  call  .r
                  pop   b
                  pop   h
                  ret
.r                dcr   b
                  jz    .w1
                  lda   varColor
                  sta   .color1+1
                  lda   varColor+1
                  sta   .color2+1
                  inr   b
                  mvi   a,65
                  sub   b
                  ani   3eh
                  mov   e,a
                  mov   a,b
                  rar
                  mov   d,a
                  sbb   a
                  ani   70h             ; mov m,b
                  sta   .mode1
                  sta   .mode2
                  mov   a,d
.loopb:
.color1:          mvi   b,0
.loop:            mov   m,b
                  inx   h
                  mov   m,b
                  inx   h
                  dcr   d
                  jnz   .loop
.mode1:           mov   m,b
                  dad   d
                  mov   d,a
                  dcr   c
                  rz
.color2:          mvi   b,0
.loop2:           mov   m,b
                  inx   h
                  mov   m,b
                  inx   h
                  dcr   d
                  jnz   .loop2
.mode2:           mov   m,b
                  dad   d
                  mov   d,a
                  dcr   c
                  jnz   .loopb
                  ret
.w1:              mov   a,c
                  xchg
                  lhld  varColor
                  xchg
                  lxi   b,64
.w1loop:          mov   m,e
                  dad   b
                  mov   m,d
                  dad   b
                  dcr   a
                  rz
                  dcr   a
                  jnz   .w1loop
                  ret                            
;-------------------------------------------------------------------------------
; Vykreslí rozvržení okna
; I: HL - ukazatel na layout
; - layout = seznam prvků
;   1B color (0ffh = konec seznamu)
;   2B x,y pozice
;   2B w,h rozměr
;   2B ukazatel na text
;-------------------------------------------------------------------------------
winLayout         mov   a,m
                  cpi   255
                  rz         
                  call  setColor
                  inx   h
                  mov   b,m
                  inx   h
                  mov   c,m
                  call  setCursor
                  inx   h
                  mov   b,m
                  inx   h
                  mov   c,m
                  push  h
                  lhld  varCursor
                  lxi   d, -1
                  dad   d
                  call  fillRect
                  pop   h
                  inx   h
                  mov   e,m
                  inx   h
                  mov   d,m
                  inx   h
                  mov   a,d
                  ora   e
                  jz    winLayout
                  ; tisk textu
.print            ldax  d
                  ana   a
                  jz    winLayout
                  inx   d
                  call  print
                  jmp   .print                     
;-------------------------------------------------------------------------------
; Nastaví pozici na zadaný kurzor
; I: B = X (0 - 48)
;    C = Y (0 - 25)
;-------------------------------------------------------------------------------
setCursor         push  h
                  ; C = C*10
                  mov   a,c
                  add   a
                  add   a
                  add   c
                  add   a
                  ; HL = 64*C => rychlejší dělit 4
                  rrc
                  rrc
                  mov   l,a
                  ori   0c0h
                  mov   h,a
                  mov   a,l
                  ani   0c0h
                  add   b
                  mov   l,a
                  shld  varCursor
                  pop   h
                  ret
;-------------------------------------------------------------------------------
; Vlastní vykreslení znaku - rychlejší a s podporou colorace
; - znak je v rozlišení 8x10 pixelů
; - rozlišení obrazovky je proto 48x25 znaků
; I: A - ASCII kód
;-------------------------------------------------------------------------------
print             di
                  push  b
                  push  d
                  push  h
                  sui   32
                  jc    .e                  
                  lxi   h, 0
                  dad   sp
                  shld  .sp+1
                  lxi   sp, 64
                  lhld  varColor
                  mov   b,h
                  mov   c,l             ; BC = color
                  mov   l,a
                  mvi   h,0
                  dad   h
                  dad   h
                  dad   h
                  lxi   d, varFont
                  dad   d
                  xchg                  ; DE = zdroj dat
                  lhld  varCursor       ; HL = ukazatel do VIDEORAM
                  ; 1. mikrořádek - jen atribut
                  mov   m,c             
                  dad   sp
                  rept  4
                  ; 2./4./6./8. mikrořádek
                  ldax  d
                  inx   d
                  xra   b               
                  mov   m,a
                  dad   sp
                  ; 3./5./7./9. mikrořádek
                  ldax  d
                  inx   d
                  xra   c
                  mov   m,a
                  dad   sp
                  endm
                  ; 10. mikrořádek
                  mov   m,b                  
.sp               lxi   sp, 0     
                  ; posun kurzoru
                  lhld  varCursor
                  inr   l
                  shld  varCursor
.e                pop   h
                  pop   d
                  pop   b
                  ei
                  ret            
;-------------------------------------------------------------------------------
; Cykluje barevné režimy
;-------------------------------------------------------------------------------
nextColorMode     lxi   h, varColorMode
                  inr   m
                  mov   a,m
                  dcr   a
                  mvi   c, 3
                  jz    .skip
                  mvi   c, 1
                  dcr   a
                  jz    .skip
                  mvi   m,0
                  mvi   c, 7
.skip             mov   a,c
                  ori   0c0h                  
                  out   0f8h
                  ret                  