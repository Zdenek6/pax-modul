;===============================================================================
; pax_sdhc
; - funkce pro práci se souborovým systémem
;===============================================================================
errEOF            equ   5
errTimeout        equ   6
;-------------------------------------------------------------------------------
; Makro pro vyvolání příkazu PAX modulu
;-------------------------------------------------------------------------------
pax               macro cmd
                  rst   3
                  db    cmd
                  endm
;-------------------------------------------------------------------------------
; Příkazy pro PAX modul
;-------------------------------------------------------------------------------
; inicializace SD karty - vhodne vzdy na zacatku nebo vymene karty
CMD_INIT          equ   1
; nastavi root jako aktualni adresar
CMD_ROOT          equ   2
; zahaji vypis polozek z aktualniho adresare, polozka je tvorena 20 bajty, po 20 bajtech
; se automaticky vycte obsah souboru
CMD_DIR_START     equ   3
; presune se na dalsi polozku v adresari
CMD_DIR_NEXT      equ   4
; nacte dalsi bajt z polozky/souboru
CMD_NEXT_BYTE     equ   5
; resetuje ukazatel na zacatek streamu - 20 B popis + obsah souboru
CMD_FILE_RESET    equ   6
; nastaví pozici v aktuálním souboru
CMD_SEEK          equ   7
; zahaji cteni z UARTU
CMD_UART_READ     equ   10
; zahaji vysilani pres UART
CMD_UART_SEND     equ   11

; maska pro atribut složky
ATTR_DIR          equ   10h
; atribut volume entry
ATTR_VOLUME       equ   90h
;-------------------------------------------------------------------------------
; Inicializace SD karty
; O: A - status
;-------------------------------------------------------------------------------
sdInit            ; vypne případný lock, tj. přerušení probíhající operaci
                  xra   a
                  out   0fbh
                  sta   varLabel        ; vymazání volume label
                  ; vyžádá si inicializaci SD karty
                  mvi   c, 5            ; 5 pokusů
.lpinit           pax   CMD_INIT
                  ; v A je status
                  sta   varSDHCStatus
                  jnc   .initok
                  dcr   c
                  jnz   .lpinit
                  ana   a
                  rnz
                  ; timeout
                  mvi   a, errTimeout
                  sta  varSDHCStatus
                  ana   a               ; CY = 0
                  ret               
.initok           ; najde label                  
                  call  sdStartDir
.lp               call  sdGetEntry
                  jc    .e              ; možná karta nemá label
                  cpi   ATTR_VOLUME
                  jnz   .lp
                  ; kopie labelu jinam
                  lxi   h, varLabel
                  lxi   d, varEntry.name
                  mvi   c, 15
                  rst   1
.e                xra   a               ; jako že varSDHCStatus je 0
                  ret
;-------------------------------------------------------------------------------
; Zahájí výčet souborů
;-------------------------------------------------------------------------------
sdStartDir        lda   varSDHCStatus
                  ana   a
                  rnz
                  pax   CMD_DIR_START
                  ret
;-------------------------------------------------------------------------------
; Načte 20 bajtů položky a nechá připravit další položku
;-------------------------------------------------------------------------------
sdGetEntry        lxi   h, varEntry
.hl               call  sdReadEntry
                  ; připraví další entry
sdNextEntry       push  psw
                  pax   CMD_DIR_NEXT
                  pop   psw
                  ret
;-------------------------------------------------------------------------------
; Načte 20 bajtů položky
; I: HL - ukazatel na 20B buffer, kam se položka vyčte
; O: CY = 1 ... už není žádná další položka
;-------------------------------------------------------------------------------
sdReadEntry       push  h
                  pax   CMD_FILE_RESET
                  mvi   c, 20
.loop             pax   CMD_NEXT_BYTE
                  in    0f8h
                  mov   m,a
                  inx   h
                  dcr   c
                  jnz   .loop
                  xthl
                  mov   a,m
                  pop   h
                  inr   a
                  sui   1               ; nastaví CY = 1, pokud už žádný další záznam není
                  ret
;-------------------------------------------------------------------------------
; Najde položku, která se shoduje s předaným názvem
; I: HL - název souboru
; O: CY = 1 ... nenalezeno
;    B = index pořadí položky ve výčtu
;-------------------------------------------------------------------------------
sdFind            shld  .name+1
                  call  sdStartDir
                  rc
                  mvi   b,0             ; B = počítadlo pořadí ve výčtu
.c                lxi   h, varEntry
                  call  sdReadEntry
                  rc
                  ; porovná jména, TODO: wildcards, case insensitive
                  mvi   c, 15
                  lxi   d, varEntry.name
.name             lxi   h, 0
.loop             ldax  d
                  cmp   m
                  jnz   .next
                  inx   d
                  inx   h
                  dcr   c
                  jnz   .loop
                  ana   a               ; CY = 0
                  ret
.next             call  sdNextEntry
                  inr   b
                  jmp   .c
;-------------------------------------------------------------------------------
; Načte do paměti až 255 položek aktuálního adresáře
; O: B  - počet načtených položek
;-------------------------------------------------------------------------------
sdLoadDir         lxi   d, varListPointers ; seznam ukazatelů na položky
                  lxi   h, varListItems ; ukazatel do paměti, kam se položky načtou
                  mvi   b,0             ; počítadlo položek
                  lda   varSDHCStatus
                  ana   a
                  rnz                   ; SDHC není zinicializovaná
                  pax   CMD_DIR_START
.lp               ; zapíše se ukazatel
                  xchg
                  mov   m,e
                  inx   h
                  mov   m,d
                  inx   h
                  xchg
                  call  sdGetEntry.hl
                  rc
                  cpi   ATTR_VOLUME      ; je to VOLUME LABEL?
                  jnz   .skip
                  ; ano - ignorujeme
                  mov   a,b
                  lxi   b, -20
                  dad   b
                  mov   b,a
                  dcx   d
                  dcx   d
                  jmp   .lp
.skip             inr   b
                  mov   a,b
                  inr   a
                  jnz   .lp
                  ret
;-------------------------------------------------------------------------------
; Kontrola, že varEntry představuje adresář
; O: CY = 1 ... není to adresář
;-------------------------------------------------------------------------------
sdCheckPath       lda   varEntry.attr
                  ani   ATTR_DIR
                  rnz
                  stc
                  ret
;-------------------------------------------------------------------------------
; Otevře složku, na jejíž název odkazuje HL. Složka musí být v akt. adresáři
; HL: ukazatel na jméno, max. 15 znaků, může být kratší, když končí nulou
;     nebo lomítkem
;-------------------------------------------------------------------------------
sdOpenDir         call  sdFind
                  rc                    ; tato složka neexistuje
                  ; kontrola, že to, co se našlo, je opravdu složka
                  call  sdCheckPath
                  rc                    ; není to složka
                  ; připíše se index do cesty, ale jen pokud nepřekročíme max. povolenou hloubku zanoření
                  lxi   h, varPath.depth
                  mov   a,m
                  cpi   MAX_PATHDEPTH+2
                  jnc   vrReadByte      ; tento adresář otevřeme, ale nezapisujeme ho do cesty
                  inr   m               ; počet podadresářu vzrostl
                  mov   e,m
                  mvi   d,0
                  dad   d
                  mov   m,b             ; připíše se na konec seznamu
                  ; otevření složky - stačí přečíst jeden bajt
                  jmp   vrReadByte
;-------------------------------------------------------------------------------
; Vrátí se do nadřazeného adresáře
;-------------------------------------------------------------------------------
sdParentDir       lxi   h, varPath.depth
                  mov   a,m
                  ana   a
                  rz
                  dcr   m
                  jmp   sdSetPath
;-------------------------------------------------------------------------------
; Skočí do rootu souborového systému
;-------------------------------------------------------------------------------
sdRoot            xra   a
                  sta   varPath.depth                  
;-------------------------------------------------------------------------------
; Nastaví cestu tak, jak jsou uvedené indexy v varPath
;-------------------------------------------------------------------------------
sdSetPath         pax   CMD_ROOT
                  lxi   h, varPath.depth
                  mov   a,m
                  ana   a
                  rz
                  mov   b,a
.loop2            inx   h
                  mov   c,m
                  call  sdSetItem
.cont             ; otevře - tj. načte 21 bajtů, entry + první bajt
                  push  h
                  lxi   h, varEntry
                  call  sdReadEntry
                  pop   h
                  ; kontrola, že to je adresář
                  call  sdCheckPath
                  rc
                  ; vstoupí dovnitř - přečtení 21. bajtu
                  call  vrReadByte
                  ; další podadresář
                  dcr   b
                  jnz   .loop2
                  ret
;-------------------------------------------------------------------------------
; Odkrokuje položku v akt. adresáři (nastaví ji pro případné čtení)
; I: C - index položky 0 - 255
;-------------------------------------------------------------------------------
sdSetItem         call  sdStartDir
                  inr   c
.loop             dcr   c
                  rz 
                  pax   CMD_DIR_NEXT
                  jmp   .loop
;-------------------------------------------------------------------------------
; Nastaví pozici v souboru
; I: DEHL = 4B pozice v souboru (HL = low)
;-------------------------------------------------------------------------------
sdSetFilePos      pax   CMD_SEEK
                  cpi   127
                  rnz
                  mvi   c,4
.loop             mov   a,d
                  mov   d,e
                  mov   e,h
                  mov   h,l
                  call  vrPaxCmd
                  cpi   127
                  rnz
                  dcr   c
                  jnz   .loop
                  xra   a
                  out   0fbh
                  ret                  
;-------------------------------------------------------------------------------
; Seřadí položky
; I: varListCount + varListPointers + varListItems
;-------------------------------------------------------------------------------
sdSortList        lda   varListCount
                  mov   b,a             ; B = right = Count
                  xra   a               ; A = left = 0
;-------------------------------------------------------------------------------
; Quicksort na file list
; A = left
; B = right
; Navrženo na základě:
; void quicksort(int array[], int left, int right){
;    if(left < right){
;        int boundary = left;
;        for(int i = left + 1; i < right; i++){
;            if(array[i] > array[left]){
;                swap(array, i, ++boundary);
;            }
;        }
;        swap(array, left, boundary);
;        quicksort(array, left, boundary);
;        quicksort(array, boundary + 1, right);
;    }
; }
;-------------------------------------------------------------------------------
quicksort         cmp   b               ; left - right < 0
                  rnc
                  mov   c,a             ; B = right, C = left
                  push  b
                  mov   l,a
                  sub   b               ; A = left - right = -počet opakování
                  mvi   h,0
                  dad   h
.data             lxi   d, varListPointers
                  dad   d               ; HL = array[left]
                  push  h
                  mov   e,m
                  inx   h
                  mov   d,m
                  inx   h               ; HL = left+1  / array[i] v původním algoritmu
                  xchg
                  shld  .pivot+1
                  xchg
                  pop   d               ; DE = boundary, HL = array[i]
                  push  d               ;*
                  ; porovná array[i] a array[left]
.loop             inr   a
                  jz    .end
                  push  psw
                  push  h
                  push  d
                  mov   e,m
                  inx   h
                  mov   d,m
.pivot            lxi   h, 0
                  call  qsCompare       ; porovná [hl] a [de]
                  pop   d
                  pop   h
                  jc    .skip
                  inx   d
                  inx   d               ; boundary++
                  call  qsSwap          ; prohodí boundary a array[i]
.skip             inx   h
                  inx   h               ; HL = array[++i]
                  pop   psw
                  jmp   .loop
.end              ; zařazení pivotu na místo boundary
                  pop   h               ;* HL = left, DE = boundary
                  call  qsSwap
                  ; přepočítá ukazatel boundary na index
                  ; bindex = (boundary - left)/2 + lf
                  mov   a,e
                  sub   l
                  mov   l,a
                  mov   a,d
                  sbb   h
                  rar
                  mov   a,l
                  rar
                  pop   b
                  add   c               ; A = boundary index
                  push  b
                  mov   b,a
                  mov   a,c
                  call  quicksort       ; quicksort(left, boundary)
                  mov   a,b
                  inr   a
                  pop   b
                  jmp   quicksort       ; quicksort(boundary + 1, right)
; přehodí dvoubajtové hodnoty na ukazatelích HL a DE
qsSwap            mov   b,m
                  ldax  d
                  mov   m,a
                  xchg
                  mov   m,b
                  inx   h
                  inx   d
                  mov   b,m
                  ldax  d
                  mov   m,a
                  xchg
                  mov   m,b
                  dcx   h
                  dcx   d
                  ret
; porovná dvě souborové položky
; - nejprve adresáře, pak soubory
; - pak řazení dle abecedy, case insensitive
qsCompare         mov   a,m
                  ani   10h
                  mov   b,a
                  ldax  d
                  ani   10h
                  cmp   b
                  rnz                   ; složky vs soubory
                  ; teď už jen podle názvu
                  lxi   b, 5
                  dad   b
                  xchg
                  dad   b
.loop             mov   a,m
                  call  upCase
                  mov   b,a
                  ldax  d
                  call  upCase
                  cmp   b
                  rnz
                  inx   d
                  inx   h
                  jmp   .loop