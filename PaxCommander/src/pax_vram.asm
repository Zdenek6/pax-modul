;===============================================================================
; pax_vram
; - rutinky, které se naskládají do paměti vedle videoram
; - používá se při přenosu dat pomocí UARTU
; - používá PMD85-2, který nemá možnost přepisu Monitoru
; - používá i PAX Commander, aby se neduplikoval kód
;===============================================================================
VRAM_START        equ   0c430h          ; startovací adresa umístění
;-------------------------------------------------------------------------------   
; Přenese kód do videoram
;-------------------------------------------------------------------------------   
Copy2VRAM         lxi   d, CODE_START
                  lxi   h, VRAM_START
.loop             lxi   b, 16 * 256 + 48     
.cpy              ldax  d
                  mov   m,a
                  inx   h
                  inx   d
                  dcr   b
                  jnz   .cpy
                  dad   b
                  mov   a,e
                  sui   CODE_END & 255
                  mov   a,d
                  sbi   CODE_END >> 8
                  jc    .loop
                  ret
CODE_START
;*******************************************************************************
                  phase VRAM_START + 0*64
;*******************************************************************************
;-------------------------------------------------------------------------------
; Zápis příkazu a čekání na jeho dokončení i s timeoutem
; Vstup:
;    A = číslo příkazu
; Vrací:
;    CY = 0 ... příkaz se provedl 
;    CY = 1 ... došlo k chybě, A = chyba (0 => timeout)
;-------------------------------------------------------------------------------
vrPaxCmd          out   0f8h
                  push  b
                  lxi   b, 0
.loop             in    0fbh
                  add   a
                  dcx   b 
                  jc    vrPaxCmd2
                  jmp   vrPaxCmd3
;*******************************************************************************
                  phase VRAM_START + 1*64
;*******************************************************************************
vrPaxCmd2         mov   a,b
                  ora   c
                  jnz   vrPaxCmd.loop
                  pop   b                  
                  stc                   ; timeout, CY = 1, A = 0
                  ret  
vrPaxCmd3         pop   b
                  rar   
                  rz                    ; ok, CY = 0, A = 0
                  stc                   
                  ret                   ; chyba, CY = 1, A = chyba      
vrSP              ds    2
                  ds    1
;*******************************************************************************
                  phase VRAM_START + 2*64
;*******************************************************************************
;-------------------------------------------------------------------------------
; Přečte jeden bajt ze streamu 
;-------------------------------------------------------------------------------
vrReadByte        push  b
                  push  d
                  push  h                  
                  lxi   h, 0
                  dad   sp
                  shld  vrSP
                  lxi   sp, vrLocalStack
                  jmp   vrReadByte2
;                  call  vrPTPReload     ; otestuje nutnost začít další ptp block
 ;                 call  vrNextByte
  ;                pop   h
   ;               pop   b
    ;              ret
                  ;ds    1
;*******************************************************************************
                  phase VRAM_START + 3*64
;*******************************************************************************
;-------------------------------------------------------------------------------
; Pokud se dočetl záznam z PTP souboru, je nutné načíst délku dalšího
;-------------------------------------------------------------------------------
vrPTPReload       lhld  vrPTPCounter
                  mov   a,h
                  ora   l
                  rz                    ; nejedná se o PTP soubor
                  dcx   h
                  mov   a,h
                  ora   l
                  jz    vrPTPReload2    ; přečten celý blok, načte další
                  shld  vrPTPCounter
                  ret
;*******************************************************************************
                  phase VRAM_START + 4*64                 
;*******************************************************************************
vrPTPReload2      call  vrNextByte
                  mov   l,a
                  call  vrNextByte
                  mov   h,a
                  shld  vrPTPCounter
                  ret
vrPTPCounter      db    0,0,0,0
;*******************************************************************************
                  phase VRAM_START + 5*64
;*******************************************************************************
fillProgress      mov   m,c
                  dad   d
                  dcr   b
                  jnz   fillProgress
                  lxi   h, updateProgress2+1
                  inr   m
                  ret
                  ds    5
;-------------------------------------------------------------------------------                  
; Přenos do paměti po UARTu
;-------------------------------------------------------------------------------
                  phase VRAM_START + 6*64
uartTransfer      
.adr              lxi   h, 0            ; kam
.len              lxi   d, 0            ; kolik
.delta            lxi   b, 0            ; čítač inkrementace progressbaru
.loop             call  uartGetByte
                  rc                    ; chyba - skok do systému
                  jmp   uartTransfer2
                  phase VRAM_START + 7*64
uartTransfer2     mov   m,a
                  inx   h
                  dcx   d
                  mov   a,d
                  ora   e
                  jz    uartRun
                  dcx   b
                  mov   a,b
                  ora   c
                  jmp   uartTransfer3
                  ds    2
                  phase VRAM_START + 8*64
uartTransfer3     jnz   uartTransfer.loop
                  call  updateProgress
                  jmp   uartTransfer.delta
uartRun           pop   psw             ; vytáhne adresu 8000
                  mvi   a, 1ch
                  jmp   8500h           ; smaže obrazovku + návrat do načtené aplikace
                  ds    1     
;-------------------------------------------------------------------------------                  
; Načtení bloku z magnetofonu a jeho spuštění
; - jedná se o první blok za hlavičkou
;-------------------------------------------------------------------------------                  
                  phase VRAM_START + 9*64
vrLoadMgfAndRun   lhld  0c1b6h
                  xchg
                  lhld  0c1b4h
                  call  vrLoadMgBlock
                  rc
                  mvi   c, 3fh
                  jmp   LoadMgf2     
                  dephase
                  phase VRAM_START + 10*64
vrLoadMgBlock     mvi   b, 0
.loop             call  vrReadByte
                  rc
                  mov   m,a
                  inx   h
                  add   b
                  mov   b,a
                  mov   a,d
                  ora   e
                  dcx   d
                  jmp   LoadMgBlock2
                  dephase
                  phase VRAM_START + 11*64
LoadMgBlock2      jnz   vrLoadMgBlock.loop
                  call  vrReadByte
                  xra   b
                  rz
                  stc
                  ret
LoadMgf2          xra   a                  
                  lxi   sp, 7ffbh
                  ret
                  
                  ds    1
                  dephase
                  phase VRAM_START + 12*64    
AllRamMGByte      mvi   a, 82h          ; zapne AllRam režim, vypne ROM
                  out   0f7h            ; PCH = výstup
                  jmp   8b6ch
                  ds    9
                  dephase
;-------------------------------------------------------------------------------                  
; Čte z UARTu bajt, pokud nebylo nic přijato, vrací CY = 1
;-------------------------------------------------------------------------------
                  phase VRAM_START + 13*64
vrNextByte        mvi   a, CMD_NEXT_BYTE
                  jmp   vrPaxCmdRead                  
uartReadByte      mvi   a, CMD_UART_READ
; provede příkaz, který po provedení vrací hodnotu
vrPaxCmdRead      call  vrPaxCmd
                  rc                    ; chyba
                  in    0f8h
                  ana   a
                  ret
                  ds    1

;-------------------------------------------------------------------------------                  
; Čte z UARTu bajt, pokud nebylo nic přijato, čeká až do timeoutu
;-------------------------------------------------------------------------------
                  phase VRAM_START + 14*64
uartGetByte       call  uartReadByte
                  rnc
                  ana   a
                  stc
                  rz                    ; timeout - konec s chybou
                  jmp   uartGetByte     ; nic nepřišlo a timeout nevypršel - znova
updateProgress    push  h
                  push  d
                  push  b
                  jmp   updateProgress2
;-------------------------------------------------------------------------------                  
; Vyplní progressbar
;-------------------------------------------------------------------------------
                  phase VRAM_START + 15*64
updateProgress2   lxi   h, 0c000h       ; adresa políčka
                  lxi   d, 64
                  lxi   b, 12*256 + 01fh
                  call  fillProgress
                  pop   b
                  pop   d
                  pop   h
                  ret
                  phase VRAM_START + 16*64
;-------------------------------------------------------------------------------                  
; Dokončení čtení bajtu
;-------------------------------------------------------------------------------
vrReadByte2       call  vrPTPReload     ; otestuje nutnost začít další ptp block
                  call  vrNextByte
                  lhld  vrSP
                  sphl
                  pop   h
                  pop   d
                  pop   b
                  ret
                  ds    4
                  dephase
                  dephase
                  phase VRAM_START + 17*64
                  ds    16
vrLocalStack                  
                  dephase
CODE_END                  