;===============================================================================
; winSDHC
; - hlavní okno - procházení adresářovou strukturou
;===============================================================================
varTimer          ds    1
;-------------------------------------------------------------------------------
; Zobrazení okna a spuštění jeho vlákna
;-------------------------------------------------------------------------------
winSDHC           ; inicializace SD karty
                  call  sdInit
                  lxi   h, varLabel
                  jz    .repsd              
                  ; chyba karty - zobrazí se místo labelu
                  adi   '0'
                  sta   sdhcLabel.error+9
                  lxi   h, sdhcLabel.error
.repsd            shld  sdhcMessage
                  ; nastavení PMD režimu
                  call  switchToPMD    
.repaint          lxi   h, sdhcDesc
                  call  winLayout
                  mvi   a, 255
                  sta   varTimer
                  ; naplní seznam soubory z aktuálního adresáře
                  lxi   h, 6*10*64+10+0c000h
                  call  lb_DirInit
.loop             ; spuštění služeb na pozadí + návrat stisknuté klávesy
                  rst   7
                  lxi   h, .loop
                  push  h
                  push  psw
                  ; tisk informací o vybraném souboru
                  call  sdhcInfo
                  ; zpracování klávesy
                  pop   psw
                  rz
                  cpi   KEY_C_D
                  jz    sdhcCM
                  cpi   KEY_EOL
                  jz    sdhcOpen
                  ; všechny další klávesy mohou potenciálně změnit označenou položku, smaže se proto info
                  call  sdhcClearInfo

                  cpi   KEY_NEXT
                  jz    lb_Next
                  cpi   KEY_PREV
                  jz    lb_Prev
                  cpi   KEY_HOME
                  jz    lb_Home
                  cpi   KEY_END
                  jz    lb_End
                  cpi   KEY_LEFT
                  jz    sdhcUp
                  jmp   lb_jump
                  ret
sdhcCM            call  nextColorMode
                  pop   psw
                  jmp   winSDHC.repaint
                  ; provede návrat do nadřazené složky
sdhcUp            call  sdParentDir
                  jmp   lb_DirRefresh
sdhcOpen          ; provede enumeraci a nalezne zvolenou položku
                  call  sdhcGetEntry
                  jz    sdhcOpenFile
                  call  sdOpenDir
                  ; překreslení listboxu
                  jmp   lb_DirRefresh
;-------------------------------------------------------------------------------
; Načte popisek souboru podle vybrané položky v seznamu
; I:  aktuální index v listobixu
; O:  HL - ukazazuje na název položky, o 4 bajty dříve se nachází velikost
;     C = atributy souboru
;     Z = 1, pokud se jedná o soubor, jinak je to složka
;-------------------------------------------------------------------------------
sdhcGetEntry      call  lb_GetFileItem
                  mov   c,m
                  lxi   d, 5            ; HL = &Entry
                  dad   d               ; offset na jméno
                  mov   a,c
                  ani   ATTR_DIR
                  ret
;-------------------------------------------------------------------------------
; Otevře soubor
; - podle přípony zvolí akci
; I: HL - ukazatel na název souboru
;-------------------------------------------------------------------------------
sdhcOpenFile      lxi   d, .exttable
                  jmp   switchExt
; pmd soubor
.pmd              call  sdhcGetEntry    ; do HL nastaví ukazatel na jméno vybrané položky
                  call  sdFind          ; nalezne soubor/načte entry
                  rc                    ; nenalezeno - návrat 
                  call  lb_Push
                  call  runProgram
                  jmp   .refreshWin
; ptp - zobrazí se jiné okno, po jeho zavření se musí opět překreslit SDHC okno
.ptp              call  lb_Push
                  call  drawDesktop
                  call  sdhcGetEntry    ; do HL = název souboru
                  call  winPTP
                  call  switchToPMD
.refreshWin       call  drawDesktop
                  lxi   h, sdhcDesc
                  call  winLayout
                  call  lb_Pop
                  jmp  lb_DirRefresh.saveindex
; rmm - načtení ROM modulu   
.rmm              call  lb_Push
                  call  sdhcGetEntry    ; do HL = název souboru
                  call  winROM
                  jmp   .refreshWin
; tabulka přípon
.exttable         db    "PMD"
                  dw    .pmd
                  db    "PTP"
                  dw    .ptp
                  db    "RMM"
                  dw    .rmm
                  db    0
;-------------------------------------------------------------------------------
; Tisk informací o souboru, který má hlavičku pmd3To2A
;-------------------------------------------------------------------------------
sdhcInfo          lxi   h,varTimer
                  inr   m
                  dcr   m
                  rz
                  dcr   m
                  rnz
                  ; najde vybraný soubor
                  call  sdhcGetEntry
                  rnz                   ; info se u složek netiskne
                  call  sdFind
                  rc
                  call  sdhcInfoPrepare
                  call  loadPMDHead
                  rnz                   ; toto není pmd soubor
                  ; výpis pmd hlavičky
                  lxi   h, txtInfo
                  push  h
                  call  headPrint
                  ; info o zabírané paměti
                  rst   5
                  xchg
                  dad   d
                  push  h
                  lxi   h, txtinfo.adr
                  mov   a,d
                  call  hex2ascii
                  mov   a,e
                  call  hex2ascii
                  inx   h
                  pop   d
                  mov   a,d
                  call  hex2ascii
                  mov   a,e
                  call  hex2ascii
                  ; pokud má hra autorun, vypíše *
                  call  isAutorun
                  mvi   a, ' '
                  jnc   .noauto
                  mvi   a, '*'
.noauto           sta   txtInfo.autorun
                  pop   h
                  jmp   printString
;-------------------------------------------------------------------------------
; smaže info text
;-------------------------------------------------------------------------------
sdhcClearInfo     push  psw
                  lxi   h, varTimer
                  mvi   a, 255
                  cmp   m
                  jz    .e
                  mov   m,a
                  call  sdhcInfoPrepare
                  lhld  varCursor
                  lxi   b, 26*256 + 10
                  call  fillRect
.e                pop   psw
                  ret
;-------------------------------------------------------------------------------
; nastaví barvy a kurzor vykreslení informačního textu
;-------------------------------------------------------------------------------
sdhcInfoPrepare   lxi   b, 0A15h
                  call  setCursor
                  mvi   a, CA_WHITE + CA_FILL
                  jmp   setColor
;-------------------------------------------------------------------------------
; Deskriptor okna
;-------------------------------------------------------------------------------
sdhcDesc          PAXWIN 30, 19, sdhcLabel.title

                  ; text s aktuální madresářem
                  db    CA_WHITE + CA_FILL + CA_BORDER
                  db    _x+1, _y+1
                  db    _w-2, 10
sdhcMessage       dw    sdhcLabel.error

                  ; podbarvení listboxu - jen rámeček
                  db    CA_WHITE + CA_FILL + CA_BORDER
                  db    _x+1, _y+3
                  db    _w-2, 14*10
                  dw    0

                  db    255
sdhcLabel                  
.title            db    "PAX Commander 1.5",0
.error            db    "SDHC Err# ",0
txtInfo           db    "00/? 12345678 ("
.adr              db    "xxxx-xxxx)"
.autorun          db    " ",0