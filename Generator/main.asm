;
; Generator.asm
;
; - generuje clock pro SAA1099A
; - prodlu�uje p�eru�ovac� impuls z �asova�e 8253
;
; Created: 27.06.2021 7:36:28
; Author : Zdenek
;                       ATTiny25/45/85 
;                       .-----__-----.         
;           RESET >---- | *          | ----> VCC
;                       |            |           
;             |-------- |         PB2| ----< Timer 8253 - OUT0
;           XTAL        |            |         
;             |-------- |         PB1| ----> IRQ
;                       |            |             
;             GND >---- |         PB0| ----> 8 MHz
;                       `------------'					
;
; Mo�no pou��t i bez krystalu, taktov�no z RC + PLL na 16 MHz
;
; Fuses L: 0xC1
; Fuses H: 0xDF
;
; P�i pou�it� s krystalem 16 MHz
;
; Fuses L: 0xEE
; Fuses H: 0xDF
;

start:
		rjmp	main
; obsluha p�eru�en� INT0
		sbi	PORTB, PB1		; nastav� sign�l IRQ
		; delay 12us ~ 192 takt�
		ldi	r16, 64
wait:	dec	r16
		brne wait
		cbi	PORTB, PB1		; resetuje sign�l IRQ
		reti

; hlavn� nastaven� a nekone�n� smy�ka
main:	; nastaveni SP
		ldi	r16, 0x60 + 64	; kv�li p�eru�en�, vyhovuje pro v�echny varianty ATTiny25/45/85
		out	SPH, r16
		; konfigurace ��ta�e 0 => gener�tor 8 MHz (CPO clock / 2 s v�stupem na PB0)
		; Clock = fcpu
		; CTC mode
		; TOP = 0
		ldi	r16, 0b01000010
		out	TCCR0A, r16
		ldi	r16, 0b00000001
		out	TCCR0B, r16
		ldi	r16, 0
		out	OCR0A, r16
		ldi	r16, 0
		out	TCNT0, r16
		sbi DDRB, PB0	; pin mus� b�t v�stupn�
		; IRQ pin je v�stupn�
		sbi DDRB, PB1		
		; povol� p�eru�en� INT0 - n�b�n� hrana
		ldi	r16, 0x03
		out	MCUCR, r16
		ldi	r16, 0x40
		out	GIMSK, r16
		sei
loop:	rjmp	loop


