{
***************************************************************************
*    SDHC Driver                                                          *
*    Author: Zdenek Sestak                                                *
*    Web: maximalne.8u.cz                                                 *
***************************************************************************
Vlastnosti driveru:
- pracuje pouze s SDHC kartami naformatovane na exFat, mus� b�t MBR ne GPT
- pracuje pouze s prvnim oddilem
- ignoruje soubory, ktere maji jmeno delsi nez 15 znaku
- ignoruje soubory, ktere zabiraji vice, nez jeden cluster

Pozn�mky:
- pri inicializace je treba pouzivat nizsi rychlost
- vyssi rychlost komunikace nezrychluje pristup k sektorum - cekani na odpoved FE pri cteni dat
  je ted az po 500 pokusech
                                      
}
CON
        _clkmode        = xtal1 + pll16x
        _xinfreq        = 5_000_000

        ' komunikacni rychlost (Mb/s)
        SD_INIT_SPEED = 100_000     
        SD_SPEED      = 1_000_000
        'SD_SPEED      = 250_000     

        'id chyb
        OK     = 0                                      ' bez chyby
        errCardInitError = 1                            ' chyba behem inicializace SD karty
        errReadSectorError = 2                          ' chyba behem cteni sektoru
        errBadFormat = 3                                ' na karte neni exFAT
        errEOD = 4                                      ' dosazeno konce vypisu
        errEOF = 5                                      ' dosazeno konce souboru
        errRxEmpty = 10                                 ' UART neprijal zadna data                        
        errBusy = $FF                                   ' operace neni jeste dokoncena

OBJ
        s:      "Shared"        
VAR
        'buffer pro 1 sektor
        byte  SDBUFFER1[512]            

        ' cog beziciho driveru
        long  cog

        ' Directory entry - celkem 24B
        '  1 B - atributy (skryty, systemovy, adresar, volume label)
        '  4 B - velikost souboru
        ' 15 B - jm�no
        long  entry[5]
        

DAT
        ' prikazy pro SD kartu
        sdCMD0          byte  $40 + 0, $00, $00, $00, $00, $95  
        sdCMD8          byte  $40 + 8, $00, $00, $01, $AA, $87  
        sdCMD55         byte  $40 +55, $00, $00, $00, $00, $01  
        sdACMD41        byte  $40 +41, $40, $00, $00, $00, $01  
        sdCMD58         byte  $40 +58, $00, $00, $00, $00, $01
       

CON
        ' offsety do seznamu prikazu
        SD_CMD0 = 0
        SD_CMD8 = 6
        SD_CMD55 = 12
        SD_ACMD41 = 18
        SD_CMD58 = 24
        
PUB  start (Shared)
  ' pokud uz je spusteno, zastavi se
  stop    
  ' nastaveni ukazatelu pro cog
  sdbufferptr1 := @SDBUFFER1
  sdCmd := @sdCMD0
  textPtr := Shared + s#MESSAGE     ' pro ladici ucely dostane cog ukazatel do  vypisu na obrazovku

  sdEntryPtr:= @entry
  SD_COMMAND_Ptr := Shared + s#PAX_CMD
  SD_STATUS_Ptr := Shared + s#PAX_STATUS
  SD_DATA_Ptr := Shared + s#PAX_DATA
  LOCK_Ptr := Shared + s#PAX_LOCK
  cognew(@sdCogInit,0)
  

PUB stop  
  if cog
    cogstop(cog~ - 1)
  
CON
        ' offsety hodnot vuci zacatku MBR/BOOT sektoru        
        MBR_SYSTEMID = $1C2                             ' offset to SystemID byte in MBR Sector
        MBR_LBABEGIN = $1C6                             ' offset to LBA begin in MBR Sector
        BOOT_SECTORPERCLUSTER = $6D                     ' offset to sectors per cluster
        BOOT_CLUSTERHEAPOFFSET = $58                    ' offset to cluster heap offset (in sectors)
        BOOT_ROOTDIRCLUSTER = $60                       ' offset to root directory cluster
        BOOT_SECTORSPERFATS = $16                       ' offset to num of FAT (usualy 2)

        ' type of entry
        ENTRY_VOLUME = $83
        ENTRY_FILE = $85
        ENTRY_STREAM_EXT = $C0
        ENTRY_FILENAME = $C1

DAT
                        org
sdCogInit
                        ' nastavi potrebne piny do vystupu
                        mov     DIRA, sd_output
                        ' sd deassert
                        or      OUTA, sd_cs

                        ' 20x odeslani 0xFF pri neaktivni karte             
                        mov     sdTemp, #200
:lp1                    call    #sdSend0xFF
                        djnz    sdTemp, #:lp1
                        ' nulov�n� statusu
                        ' smycka ceka na prikaz
:cmdloop                rdbyte  cmd, SD_COMMAND_Ptr  WZ
                   if_z jmp     #:cmdloop               ' NOP
                        cmp     cmd, #s#CMD_SEEK+1 WC
                  if_nc jmp     #:cmdloop               ' neni prikaz pro tento cog                                
                        rdbyte  sdTemp, LOCK_Ptr WZ
                  if_nz jmp     #:cmdloop               ' je zamek,proto command ignorujeme
                        mov     sdError, #errBUSY
                        cmp     cmd, #s#CMD_INIT WZ
                   if_z call    #sdInit
                        cmp     cmd, #s#CMD_ROOT WZ
                   if_z call    #sdSetRoot
                        cmp     cmd, #s#CMD_DIR_START WZ
                   if_z call    #sdDirStart
                        cmp     cmd, #s#CMD_FILE_RESET WZ
                   if_z call    #sdFileReset
                        cmp     cmd, #s#CMD_NEXT_BYTE WZ
                   if_z call    #sdNextByte
                        cmp     cmd, #s#CMD_DIR_NEXT WZ
                   if_z call    #sdDirNext
                        cmp     cmd, #s#CMD_SEEK WZ
                   if_z call    #sdSeek
                        cmp     cmd, #10 WC
                  if_nc jmp     #:cmdloop
                        mov     cmd, #0
                        wrbyte  cmd, SD_COMMAND_Ptr
                        wrbyte  sdError, SD_STATUS_Ptr
                        'wrbyte  sdError, textPtr
                        jmp     #:cmdloop
'-------------------------------------------------------
' CMD_SEEK - zapisuje do Filepos jako do pos. registru           
'-------------------------------------------------------
sdSeek                  'nejprve zamek, aby dalsi data nebyla interpretovana jako prikaz
                        wrbyte  c_FFFFFFFF, LOCK_Ptr
                        ' status - mozno nacist dalsi byte
                        mov     sdError, #s#errWaitForData 
:loop                   wrbyte  sdError, SD_STATUS_Ptr
:loop2                  rdbyte  sdTemp, LOCK_Ptr WZ
                  if_z  jmp     #:end             ' ukonceni zamku znamena konec prikazu
                        rdbyte  sdTemp, SD_STATUS_Ptr
                        cmp     sdTemp, #s#errBusy WZ       ' byl dalsi zapis do CMD?
                  if_nz jmp     #:loop2
                        ' rotace hodnoty do sdFilePos
                        shl     sdFilePos, #8                        
                        rdbyte  sdTemp, SD_COMMAND_Ptr
                        or      sdFilePos, sdTemp
                        jmp     #:loop
:end                    add     sdFilePos, #20  ' preskoci entry cast                                                               
sdSeek_ret              ret
'-------------------------------------------------------
' DIR_START           
'-------------------------------------------------------
sdDirStart              mov     sdEntryIndex, #0
                        call    #sdGetEntry
sdFileReset             mov     sdFilePos, #0
                        mov     sdError, #Ok
sdDirNext_ret                        
sdFileReset_ret                        
sdDirStart_ret          ret                        
'-------------------------------------------------------
' DIR_NEXT           
'-------------------------------------------------------
sdDirNext               call    #sdGetNextEntry
                        jmp     #sdFileReset
'-------------------------------------------------------
' NEXT            
'-------------------------------------------------------
sdNextByte              
                        cmp     sdFilePos, c_FFFFFFFF WZ
                        mov     sdError, #errEOF
                  if_z  jmp     #sdNextByte_ret         ' neni co cist
                        mov     sdError, #OK         ' nastavi status na 0
                        cmp     sdFilePos, #20 WC
                  if_nc jmp     #:readfromfile
                        ' prvnich dvacet znaku je deskriptor          
                        mov     sdPointer, sdEntryPtr
                        add     sdPointer, sdFilePos
                        rdbyte  sdTemp, sdPointer
                        wrbyte  sdTemp, SD_DATA_ptr
                        ' posun na dalsi bajt
                        add     sdFilePos, #1
                        jmp     #sdNextByte_ret
:readfromfile           ' pokud se jedna o slozku, ctenim ji aktivujeme jako aktualni
                        rdbyte  sdTemp, sdEntryPtr      ' atributy jsou na prvnim miste polozky
                        and     sdTemp, #$10    WZ
                   if_z jmp     #:filer
                        ' adresar
                        mov     lbaCurrentDir, eLBA     ' zapise LBA teto polozky
                        call    #sdDirStart             ' nastartuje cteni polozek
                        jmp     #sdNextByte_ret
                             
:filer                  mov     sdPointer,sdFilePos
                        sub     sdPointer, #20
                        ' test, zda jsme neprekrocili velikost souboru
                        cmp     sdPointer, eSize WC
                  if_nc wrbyte  c_FFFFFFFF, SD_DATA_ptr ' mimo soubor se vrati FF
                  if_nc mov     sdError, #errEOF        ' status - konec souboru
                  if_nc jmp     #sdNextByte_ret
                        ' nejprve urci LBA sektoru
                        mov     sdSectorLBA, sdPointer
                        shr     sdSectorLBA, #9
                        add     sdSectorLBA, eLBA
                        call    #sdReadSector           ' nacteni sektoru
                        and     sdPointer, #511
                        add     sdPointer, sdbufferptr1 ' vypocet adresy bajtu
                        rdbyte  sdTemp, sdPointer
                        add     sdFilePos, #1
                        wrbyte  sdTemp, SD_DATA_ptr                                   
sdNextByte_ret          ret
'-------------------------------------------------------                    
' inicializace sd karty
' o: sdError: 0 - ok; errCardInitError - chyba
' local: counter 
'------------------------------------------------------- 
sdInit                  ' pro inicializaci se pouzije pomalejsi rychlost
                        mov     timerHalfBit, tInitHalfBit
                        ' sd_deassert
                        or      OUTA, sd_cs
                        ' priprava chyby
                        mov     sdError, #errCardInitError
                        ' zruseni cteni ze streamu
                        mov     sdFilePos, c_FFFFFFFF
                        mov     sdLastSectorLBA, c_FFFFFFFF
                        ' 10x odeslani 0xFF pri neaktivni karte             
                        mov     sdInit_counter, #80
:lp1                    call    #sdSend0xFF
                        djnz    sdInit_counter, #:lp1
                        ' odesilani 0xff a kontrola, ze karta taky vraci 0xff
                        andn    OUTA, sd_cs
:lpclr                  call    #sdSend0xFF
                        call    #sdSend0xFF
                        call    #sdSend0xFF
                        call    #sdSend0xFF
                        add     inreg, #1    WZ
              if_nz     jmp     #:lpclr              
                        or      OUTA, sd_cs                               
                        ' CMD 0 dokud nedostaneme vysledek 01, max 20x s mezi pauzou 20
                        mov     sdInit_counter, #40
:cmd0                   call    #sdDelay_20ms
                        mov     sdCmdId, #SD_CMD0
                        call    #sdSendCmd
                        cmp     sdAnswer, #1   WZ
                  if_z  jmp     #:cont
                        djnz    sdInit_counter, #:cmd0                        
                        ' nepovedlo se, karta asi chybi
                        jmp     #sdInit_ret
:cont                   ' CMD 8 - SEND_IF_COND - dulezite pro SDHC kartu
                        mov     sdCmdId, #SD_CMD8
                        call    #sdSendCmd
                        cmp     sdAnswer, #1  WZ
                  if_nz jmp     #sdInit_ret
                         
                        ' SD_SEND_OP_COND (ACMD41)
                        mov     sdInit_counter, #200
:cmd41                  mov     sdCmdId, #SD_CMD55         ' APP_CMD - musi predchazet aplikacnimu prikazu ACMD41
                        call    #sdSendCmd
                        mov     sdCmdId, #SD_ACMD41
                        call    #sdSendCmd
                        tjz     sdAnswer, #:cont2
                        call    #sdDelay_20ms
                        djnz    sdInit_counter, #:cmd41
                        jmp     #sdInit_ret
:cont2                  ' CMD58 (mandatory for SDHC card)
                        mov     sdCmdId, #SD_CMD58
                        call    #sdSendCmd
                        tjnz    sdAnswer, #sdInit_ret   'pokud neni nula, selhala inicializace
                        shr     inreg, #24
                        and     inreg, #$80 WZ
                   if_z jmp     #sdInit_ret
                        '------------------------------------------------
                        ' karta zinicializovana - dale urceni FAT systemu
                        ' zvyseni rychlosti
                        mov     timerHalfBit, tHalfBit                                                                                                                                                                                                                                                                     

                        'mov     sdError, #errBadFormat
                        ' nacteni MBR  + kontrola ze na konci sektoru je AA55 pattern
                        mov     sdSectorLBA, #0
                        call    #sdInit_read

                        mov     sdPointer,#MBR_SYSTEMID
                        add     sdPointer, sdbufferptr1 
                        rdbyte  sdTemp, sdPointer
                        cmp     sdTemp, #$EE  WZ  ' GPT?
                        
                        mov     sdPointer, #MBR_LBABEGIN ' ziska LBA prvniho oddilu ulozeneho v prvnim sektoru MBR disku
                  if_nz jmp     #:contboot
                        mov     sdSectorLBA, #2
                        call    #sdReadSector           ' neno sdInit_read, protoze neobsahuje AA55    
                        mov     sdPointer, #$20         ' ziska LBA prvniho oddilu ulozeneho ve druhem sektoru GPT disku
                                     
:contboot               call    #rd_long
                        mov     lbaFatBegin, sdTemp                                                                                     
                        ' nacte boot sector + kontrola, ze na konci sektoru je AA55 pattern
                        mov     sdSectorLBA, lbaFatBegin
                        call    #sdInit_read
                        ' kontrola, ze se jedna o exFAT
                        mov     sdPointer, #3
                        call    #rd_long
                        cmp     sdTemp, exfa  WZ
                  if_nz mov     sdError, #errBadFormat
                  if_nz jmp     #sdInit_ret
                                                
                  '      wrlong  sdTemp, textPtr
                                        
                        ' nacteni nekterych parametru
                        mov     sdPointer, #BOOT_SECTORPERCLUSTER
                        add     sdPointer, sdbufferptr1
                        rdbyte  fatSecPerCluster, sdPointer
                        mov     sdPointer, #BOOT_CLUSTERHEAPOFFSET
                        call    #rd_long
                        mov     lbaClusterHeap, sdTemp
                        add     lbaClusterHeap, lbaFatBegin
                        mov     sdPointer, #BOOT_ROOTDIRCLUSTER
                        call    #rd_long
                        mov     sdCluster, sdTemp
                        call    #sdCluster2LBA
                        mov     lbaRoot, sdSectorLBA
                        ' nastaveni do rootu
                        call    #sdSetRoot                       
                        mov     sdError, #Ok
sdInit_ret              ret
' nacte sektor, pokud nedoslo k chybe, kontroluje patern AA55 na konci sektoru.
' v pripade chyby skace rovnou sd_Init_ret
sdInit_read             call    #sdReadSector
                        tjnz    sdError, #sdInit_ret
                        mov     sdPointer, sdbufferptr1
                        add     sdPointer, #510
                        rdword  sdTemp, sdPointer
                        cmp     sdTemp, c_AA55 WZ
                  if_nz mov     sdError, #errBadFormat
                  if_nz jmp     #sdInit_ret
sdInit_read_ret         ret
'-------------------------------------------------------
' nacte 4 bajty ze sektoru
' i: sdPointer - offset vuci zacatku sektoru
' o: sdTemp
'-------------------------------------------------------
t4                      long    0
rd_long                 add     sdPointer, sdbufferptr1
                        rdbyte  sdTemp, sdPointer
                        add     sdPointer, #1
                        rdbyte  t4, sdPointer
                        add     sdPointer, #1
                        shl     t4, #8
                        or      sdTemp, t4     
                        rdbyte  t4, sdPointer
                        add     sdPointer, #1
                        shl     t4, #16
                        or      sdTemp, t4
                        rdbyte  t4, sdPointer
                        shl     t4, #24
                        or      sdTemp, t4     
rd_long_ret             ret                             
'-------------------------------------------------------
' Vyplni entry info
' i: nastaveny sdCurrentDir, sdEntryIndex
' o: atribut,delka,jmeno (20 bajtu)
'    posunuti sdEntryIndex na dalsi zacatek prvni polozky
' Pokud se narazi na konec seznamu, nastavi atribut na 0xFF.
' Volume entry - jako soubor se specialnim atributem
'-------------------------------------------------------
d                       long    0
i                       long    0
sdGetNextEntry          add     sdEntryIndex, #1
sdGetEntry              mov     sdPointer, sdEntryPtr
                        call    #sdLoadEntry

                        rdbyte  d, sdEntryAddress WZ
                          
                if_z    jmp     #:eofdir
                        cmp     d,#ENTRY_VOLUME WZ
                if_z    jmp     #:volumeentry
                        cmp     d,#ENTRY_FILE              WZ
                if_nz   jmp     #sdGetNextEntry
                        ' ENTRY_FILE 
                        ' urci pocet polozek, ktere tvori tento zaznam. Pokud je jiny nez 2, pak
                        ' ma soubor moc dlouhy nazev - ignoruje se
                        add     sdEntryAddress, #1
                        rdbyte  d, sdEntryAddress                               
                        cmp     d, #2 WZ
                 if_nz  jmp     #sdGetNextEntry
                        ' prvni polozka - atributy                                     
                        add     sdEntryAddress, #3
                        rdbyte  d, sdEntryAddress                               ' atributy
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        ' druha polozka - velikost a cislo clusteru
                        call    #sdLoadNextEntry
                        add     sdEntryAddress, #20
                        rdlong  sdCluster, sdEntryAddress
                        add     sdEntryAddress, #4
                        rdlong  d, sdEntryAddress
                        ' zapis velikosti - musi byt po bajtech, neni zarovnano
                        mov     eSize, d
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        shr     d, #8
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        shr     d, #8
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        shr     d, #8
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        ' treti polozka = 15x znak - jmeno
                        call    #sdLoadNextEntry
                        mov     i, #15
:loop                   add     sdEntryAddress, #2
                        rdword  d, sdEntryAddress       WZ
                  if_z  mov     d, #" "                  ' nula se nahradi mezerou      
                        cmp     d, #32 WC
                  if_c  mov     d, #"_"    
                        cmp     d, #128 WC
                  if_nc mov     d, #"_"    
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        djnz    i, #:loop
                        ' prevod cluster - LBA a jeho zapis
                        call    #sdCluster2LBA
                        ' LBA neni pro PMD dulezite, ponechame jen v COGu
                        mov     eLBA, sdSectorLBA
                        jmp     #:end
:eofdir                 ' konec seznamu - atributy se nastavi na 0xFF
                        wrbyte  c_FFFFFFFF, sdPointer
                        jmp     #:end
:volumeentry            ' atribut = 0x80 + 0x10 - jako slozka s oznacenim, ze to je volume
                        mov     d, #$90
                        wrbyte  d, sdPointer                        
                        add     sdPointer, #1
                        ' velikost = 0
                        mov     eSize, #0
                        mov     i, #4
                        mov     d, #0
:wr0                    wrbyte  d, sdPointer
                        add     sdPointer, #1
                        djnz    i, #:wr0
                        ' nastavi jmeno na mezery
                       ' mov     sdPointer, textPtr
                        mov     i, #15
                        mov     d, #32
:wrsp                   wrbyte  d, sdPointer
                        add     sdPointer, #1
                        djnz    i, #:wrsp
                        sub     sdPointer, #15
                        ' kopie jmena                     
                        add     sdEntryAddress, #1
                        rdbyte  i, sdEntryAddress
                        add     sdEntryAddress, #1
:cpname                 rdword  d, sdEntryAddress
                        add     sdEntryAddress, #2
                        cmp     d, #32 WC
                  if_c  mov     d, #"_"    
                        cmp     d, #128 WC
                  if_nc mov     d, #"_"     
                        wrbyte  d, sdPointer
                        add     sdPointer, #1
                        djnz    i, #:cpname
                        ' LBA - root
                        mov     eLBA, lbaRoot                                                                          
:end
sdGetEntry_ret
sdGetNextEntry_ret      ret

          
'-------------------------------------------------------
' Zajisti nacteni sektoru, ve kterem se nachazi aktualni
' polozka a vypocita adresu, kde se nachazi v ramci sektoru
'-------------------------------------------------------
sdEntryAddress          long    0               ' adresa, kde se nachazi polozka v ramci sektoru
sdLoadNextEntry         add     sdEntryIndex, #1
sdLoadEntry             ' sdEntryIndex - rozklad na sektor/addresu
                        mov     sdSectorLBA, sdEntryIndex
                        shr     sdSectorLBA, #4 ' /16
                        add     sdSectorLBA, lbaCurrentDir                        
                        call    #sdReadSector
                        mov     sdEntryAddress, sdEntryIndex
                        and     sdEntryAddress, #$F     ' max. 16 polozek na sector
                        shl     sdEntryAddress, #5      ' * 32 (entry - ma velikost 32 B)
                        add     sdEntryAddress, sdbufferptr1
sdLoadNextEntry_ret
sdLoadEntry_ret         ret
'-------------------------------------------------------
' nastavi root jako aktualni adresar
'-------------------------------------------------------
sdSetRoot               mov     lbaCurrentDir, lbaRoot
                        mov     sdEntryIndex,   #0
                        mov     sdFilePos, c_FFFFFFFF 'zastaveni cteni streamu
                        mov     sdError, #Ok
sdSetRoot_ret           ret
'-------------------------------------------------------
' prevod cisla clusteru na cislo sektoru
' i: sdCluster - cislo clusteru
' o: sdSectorLBA - cislo sektoru  (cluster-2) << fatSecPerCluster + lbaClusterHeap
'-------------------------------------------------------
sdCluster2LBA           mov     sdSectorLBA, sdCluster
                        sub     sdSectorLBA, #2
                        shl     sdSectorLBA, fatSecPerCluster
                        add     sdSectorLBA, lbaClusterHeap
sdCluster2LBA_ret       ret                                
'-------------------------------------------------------
' cteni jednoho sektoru
' i: sdSectorLBA - pozadovany sector, ktery se chce cist
' o: sdError: 0 - ok; errReadSectorError - chyba
'    SDBUFFER1: - 512B dat  
'-------------------------------------------------------
sdReadSector            cmp     sdLastSectorLBA, sdSectorLBA WZ
                   if_z mov     sdError, #0
                   if_z jmp     #sdReadSector_ret
                        mov     sdLastSectorLBA, c_FFFFFFFF     
                        call    #sdSend0xFF
                        ' sd_assert
                        andn    OUTA, sd_cs
                        call    #sdSend0xFF                        
                        ' priprava chyby
                        mov     sdError, #errReadSectorError
                        ' CMD17 - READ SINGLE BLOCK
                        ' 0x40+17 | LBA_MSB | LBA | LBA | LBA_LSB | CRC 
                        mov     outreg, #$40 + 17
                        call    #sdSend
                        mov     outreg, sdSectorLBA
                        shr     outreg, #24
                        call    #sdSend
                        mov     outreg, sdSectorLBA
                        shr     outreg, #16
                        call    #sdSend
                        mov     outreg, sdSectorLBA
                        shr     outreg, #8
                        call    #sdSend
                        mov     outreg, sdSectorLBA
                        call    #sdSend
                        ' crc - nezalezi na nem
                        call    #sdSend0xFF
                        ' ceka se na odpoved 0, max. 10 pokusu
                        mov     sdReadSector_cnt, c_ReadWait
:loop1                  call    #sdSend0xFF                        
                        and     inreg, #$80    NR, WZ    ' otestuje bit 7
                  if_nz djnz    sdReadSector_cnt, #:loop1
                        ' odpoved je chyba?
                        and     inreg, #$ff    WZ
                  if_nz jmp     #:end     
                        ' ceka se na zahajovaci bajt 0xFE, max. 500 pokusu
                        mov     sdReadSector_cnt, c_ReadWait
:loop2                  call    #sdSend0xFF
                        and     inreg, #$FF    WR
                        cmp     inreg, #$FE    WZ
                  if_nz djnz    sdReadSector_cnt, #:loop2
                  if_nz jmp     #:end 
:read                   ' nacte 512B
                        mov     sdReadSector_cnt, c_512
                        mov     sdReadSector_ptr, sdbufferptr1
:loop3                  call    #sdSend0xFF
                        wrbyte  inreg, sdReadSector_ptr
                        add     sdReadSector_ptr, #1
                        djnz    sdReadSector_cnt, #:loop3
                        ' 16 bit CRC (ignoruje se)
                        call    #sdSend0xFF                                                                                                                                                   
                        call    #sdSend0xFF                        
                        mov     sdLastSectorLBA, sdSectorLBA
                        mov     sdError, #0                                                                                                                                                                  
                        ' sd_deassert
:end                    or      OUTA, sd_cs
                        ' flush SPI
                        call    #sdSend0xFF
sdReadSector_ret        ret
'-------------------------------------------------------
' Delay 20ms
'-------------------------------------------------------
sdDelay_20ms            mov     sdDelay, timer20ms
                        add     sdDelay, cnt
                        waitcnt sdDelay, #0
sdDelay_20ms_ret        ret                                     
'-------------------------------------------------------                    
' odesle 6 bajtovy prikaz
' i: sdCmdId - id zpr�vy
' o: sdAnswer - prvni bajt jiny nez 0xFF
'    inreg    - dalsi 4 prijate bajty
' lokal: counter
'-------------------------------------------------------                    
sdSendCmd               ' sd_assert
                        call    #sdSend0xFF
                        andn    OUTA, sd_cs
                        call    #sdSend0xFF
                        ' vypocita skutecnou adresu prikazu v RAM
                        add     sdCmdId, sdCmd             
                        ' odesle prikaz
                        mov     sdSendCmd_counter, #6
:loop                   rdbyte  outreg, sdCmdId
                        add     sdCmdId, #1
                        call    #sdSend
                        djnz    sdSendCmd_counter, #:loop
                        ' pocka na odpoved - jiny udaj nez FF, max. 10x
                        mov     sdSendCmd_counter, #10
:wait                   call    #sdSend0xFF
                        and     inreg, #$FF
                        cmp     inreg, #$FF     WZ
                  if_nz jmp     #:read
                        djnz    sdSendCmd_counter, #:wait
:read                   ' poznaci si prvni bajt jiny od 0xFF
                        mov     sdAnswer, inreg
                        ' precte odpoved - 4 bajty - nastrada se v inreg
                        call    #sdSend0xFF
                        call    #sdSend0xFF
                        call    #sdSend0xFF
                        call    #sdSend0xFF
                        ' jeden navic
                        call    #sdSend0xFF
                        ' sd_deassert
                        or      OUTA, sd_cs
                        call    #sdSend0xFF             
sdSendCmd_ret           ret
'-------------------------------------------------------                    
' odesle 0xFF pres SPI
'-------------------------------------------------------
sdSend0xFF              mov     outreg, #$FF
'-------------------------------------------------------                    
' odesle bajt z outreg, nacte soucasne vstup do inreg
' i:   outreg - co se ma odeslat
' o:   inreg - co se prijalo
' lokal: bitcnt, temp
'-------------------------------------------------------
sdSend                  mov     sdSend_bitcnt, #8
                        mov     sdSend_temp, cnt
                        add     sdSend_temp, timerHalfBit
:loop                   test    outreg, #$80    WC
                        muxc    OUTA, sd_di     
                        shl     outreg, #1
                        waitcnt sdSend_temp, timerHalfBit
                        or      OUTA, sd_sck
                        waitcnt sdSend_temp, timerHalfBit
                        shl     inreg, #1
                        test    sd_do, INA     WC
                        muxc    inreg, #1       
                        andn    OUTA, sd_sck
                        djnz    sdSend_bitcnt, #:loop                                                        
sdSend_ret              
sdSend0xFF_ret          ret
'-------------------------------------------------------
' Konstanty                     
'-------------------------------------------------------
sd_di                   long    1<<s#pinDI
sd_do                   long    1<<s#pinDO
sd_sck                  long    1<<s#pinSCK
sd_cs                   long    1<<s#pinSDCS
sd_output               long    (1<<s#pinDI)+(1<<s#pinSCK)+(1<<s#pinSDCS)

timer1s                 long    80_000_000
timerHalfBit            long    80_000_000 / SD_INIT_SPEED / 2 ' doba trvani vysilani pulky bitu
tHalfBit                long    80_000_000 / SD_SPEED / 2 ' doba trvani vysilani pulky bitu
tInitHalfBit            long    80_000_000 / SD_INIT_SPEED / 2 ' doba trvani vysilani pulky bitu
timer20ms               long    80_000_000 / 1000 * 20
c_512                   long    512
c_ReadWait              long    50000            ' jak dlouho cekat na FE pri cteni sektoru
c_AA55                  long    $AA55
c_0                     long    0
c_FFFFFFFF              long    $FFFFFFFF
exfa                    long    $41465845
'-------------------------------------------------------
' Registry                    
'-------------------------------------------------------
sdbufferptr1            long    0       ' ukazatel do RAM, kde se nach�z� buffer pro sd (512B)
sdCmd                   long    0       ' ukazatel na tabulku prikazu
sdEntryPtr              long    0       ' ukazatel na entry zaznam
SD_COMMAND_ptr          long    0       ' ukazatel na bajt s pr�kazem - sdileny mezi cogy 
SD_STATUS_ptr           long    0       ' ukazatel na bajt se stavem - sdileny mezi cogy
SD_DATA_ptr             long    0       ' ukazatel na bajt s daty - sdileny mezi cogy
LOCK_Ptr                long    0       ' ukazatel na z�mek, aby nedoch�zelo k falesnemu cteni prikazu
sdFilePos               long    -1      ' offset pro cteni ze streamu - FFFFFFFF -> stream neni otevreny
sdLastSectorLBA         long    -1      ' nactene cislo sektoru
textPtr                 long    0
sdCmdId                 res     1                 
sdDelay                 res     1
sdTemp                  res     1
sdError                 res     1       ' 0 - OK, jinak cislo chyby
outreg                  res     1       ' registr pro hodnotu, kter� se pos�l� sd
inreg                   res     1       ' registr pro prijem dat na DI
sdAnswer                res     1
sdPointer               res     1       ' pouziva se pro vypocet adresy do sdbufferu
lbaFatBegin             res     1       ' LBA cislo sektoru, kde zacina oddil (boot sector daneho oddilu)
fatSecPerCluster        res     1       ' pocet sektoru tvorici jeden cluster
lbaClusterHeap          res     1       ' LBA cislo sektoru, kde zacina halda clusteru
lbaRoot                 res     1       ' LBA, kde zacina root directory
sdCluster               res     1       ' cislo clusteru - pouyiva se pro prepocet na LBA                                
sdSectorLBA             res     1       ' cislo sektoru, kter� se m� nacist
lbaCurrentDir           res     1       ' cislo sektoru, kde zacina aktualni adresar
sdEntryIndex            res     1       ' offset aktualniho zaznamu v adresari
eLBA                    res     1       ' lba posledne nactene polozky
eSize                   res     1       ' velikost posledne nactene polozky
cmd                     res     1       ' posledne nacteny prikaz                
'-------------------------------------------------------
' "Lokalni" promenne                    
'-------------------------------------------------------                    
sdSend_bitcnt           res     1       'pocitadlo odeslanych/prijatych bitu pres SPI
sdSend_temp             res     1       'lokalni promenna pro odesilani/prijem dat
sdSendCmd_counter       res     1       'pocitadlo
sdInit_counter          res     1       'pocitadlo
sdReadSector_cnt        res     1
sdReadSector_ptr        res     1
                        fit                   