{
***************************************************************************    
*    PAX modul v1.6                                                       *
*    Author: Zdenek Sestak                                                *
*    Web: maximalne.8u.cz                                                 *
***************************************************************************
   
}
CON
        _clkmode = xtal1 + pll16x  '80MHz                                       
        _xinfreq = 5_000_000

        RED = $8000
        GREEN = $2000
        BLUE = $0C00
        YELLOW = $F000
        WHITE = $FC00
        LIGHTGRAY = $A800
        GRAY = $5400
        DARKGRAY = $5400
        CYAN = $3c00
        MAGENTA = $8C00
        
OBJ
        sd:     "SDHCdriver"
        uart:   "UART"
        vga:    "VGAdriver"
        rom:    "ROMdriver"
        s:      "Shared"   
VAR

        'Deklarace sdílené pameti, kterou mohou pouzívat vsechny moduly
        byte  text[20]                   ' +  0  TEXT MESSAGE
        byte  F8_cmd                     ' + 20  misto pro prikaz                 
        byte  F8_data                    ' + 21  data, ktera modul bude cist
        byte  FB_status                  ' + 22  stavova zprava
        byte  lock                       ' + 23  zamek pro ignorovani prikazu
        byte  rx_head                    ' + 24  zacatek dat prijatych bajtu UARTem                       
        byte  rx_tail                    ' + 25  konec dat prijatych bajtu UARTem
        byte  tx_head                    ' + 26  zacatek dat pro odesilani
        byte  tx_tail                    ' + 27  konec dat pro odesilani
        byte  rx_buffer[s#BUFFER_LENGTH] ' + 28  prijimaci buffer
        byte  tx_buffer[s#BUFFER_LENGTH] ' + 28 + BUFFER_LENGTH vysilaci buffer
        byte  pallete[64]                ' + 28 + 2*BUFFER_LENGTH paleta barev
        byte  c17d_data                  ' hodnota ulozena na adrese C17D                      


DAT
  strTitle      byte " PAX v1.6, PMD: ?   "
  strMode       byte " Color mode         "
  txtGray       byte "GRAY"
  txtRGBM       byte "RGBM"        
  txtACE        byte "ACE "        
  currMode      byte 0
  buttState     byte 0
  temp          byte 0
  textOffTimer  long 0
  Shared        long 0
  buttTimer     long 0
  romptr        long 0

  ' palety pro preddefinovane barevne rezimy
  PAL_COLORACE  word    GREEN, GREEN, YELLOW, YELLOW, CYAN, CYAN, WHITE, WHITE
                word    YELLOW, YELLOW, RED, RED, MAGENTA, MAGENTA, MAGENTA, MAGENTA
                word    CYAN, CYAN, MAGENTA, MAGENTA, BLUE, BLUE, MAGENTA, MAGENTA
                word    WHITE, WHITE, MAGENTA, MAGENTA, MAGENTA, MAGENTA, MAGENTA, MAGENTA
  PAL_GRAY      word    WHITE, WHITE, WHITE, LIGHTGRAY, WHITE, GRAY, WHITE, DARKGRAY
                word    LIGHTGRAY, WHITE, LIGHTGRAY, LIGHTGRAY, LIGHTGRAY, GRAY, LIGHTGRAY, DARKGRAY
                word    GRAY, WHITE, GRAY, LIGHTGRAY, GRAY, GRAY, GRAY, DARKGRAY
                word    DARKGRAY, WHITE, DARKGRAY, LIGHTGRAY, DARKGRAY, GRAY, DARKGRAY, DARKGRAY                        
  PAL_COLOR     word    GREEN, GREEN, GREEN, RED, GREEN, BLUE, GREEN, MAGENTA
                word    RED, GREEN, RED, RED, RED, BLUE, RED, MAGENTA
                word    BLUE, GREEN, BLUE, RED, BLUE, BLUE, BLUE, MAGENTA
                word    MAGENTA, GREEN, MAGENTA, RED, MAGENTA, BLUE, MAGENTA, MAGENTA
                        
PUB start

  Shared := @text
  f8_cmd := 0
  FB_status := 0
  buttState := 0
  textOffTimer := cnt
  SetColorMode(2)
  
  ' init VGA cog
  vga.start(Shared)
  
  'init SD cog
  sd.start(Shared)

  'init UART cog
  uart.start(Shared)

  ' ceka se na nacteni bajtu z adresy c17d
  bytemove(@text, @strTitle, 20)
  repeat while c17d_data == 0
  if(c17d_data == $BE)
    'verze 3
    text[16] := $33
    ' oprava ROM modulu, zmena CD na CC
    rom.Fix3
  elseif (c17d_data == $7F)   
    'verze 2/2A
    text[16] := $32
  'init ROM cog
  rom.start(Shared)

  ' odchytavani casove nenarocnych prikazu
  repeat
    if lock == 0   
      if F8_cmd & $F0 == $c0
        ' nastaveni barevneho rezimu
         case F8_cmd
           $C0, $C1 : temp := 2       
           $C2, $C3 : temp := 1
           OTHER    : temp := 0
           
         SetColorMode(temp)         
         F8_cmd := 0
         FB_status := s#OK              
      case F8_cmd
        ' prazdny prikaz, aktivuje cteni statusu 
        s#CMD_NOP:
        ' cteni prijateho znaku z UARTu
        ' stavovy automat tlacitka
         case buttState
           'ceka se na stisk
           0:
             if textOffTimer <> 0
               if (cnt - textOffTimer > 400_000_000)    
                 bytefill(@text, 32, 20)
                 textOffTimer := 0
                 
             if (INA & (1<<s#pinBUTTON)) == 0                    ' stisk buttonu nastaví jiný mód
               buttState := 1
               buttTimer := cnt
           ' ceka se na uvolneni    
           1:
             if (INA & (1<<s#pinBUTTON)) <> 0                    ' stisk buttonu nastaví jiný mód
               ' jak dlouhy byl stisk?
               if (cnt - buttTimer < 80_000_000 / 50 )
                  buttState := 0     ' kratsi stisk nez 50ms ignorujeme, asi zakmit
               elseif (cnt - buttTimer < 80_000_000 * 4  )
                  SetColorMode(currMode + 1)  ' stisk do 1 sekundy - zmena barevneho rezimu     
                  buttState := 0
               else                              
                  ' dlouhy stisk
                  buttState := 0
                       
        s#CMD_UART_READ:
          F8_cmd := 0
          if rx_tail <> rx_head
          ' 'cteni dat z UARTu
            F8_data := rx_buffer[rx_tail]
            rx_tail := (rx_tail + 1) & s#BUFFER_MASK
            FB_status := s#OK
          else
            'nic se neprijalo
            FB_status:= s#errRxEmpty
        ' zahajeni odesilani dat na UART    
        s#CMD_UART_SEND:
          lock := 1         'zamek - dalsi zapisy prikazu jsou data
          FB_status := s#errWaitForData
          repeat while lock <> 0
            if(FB_status == s#errBusy)
              ' v cmd jsou data pro vysilani na uart
              Send(F8_cmd)
              FB_status := s#errWaitForData
          F8_cmd := 0
          FB_status := s#OK
        ' zapis do ROM  
        s#CMD_ROM_WRITE:
          lock := 1         'zamek - dalsi zapisy prikazu jsou data
          FB_status := s#errWaitForData
          repeat while (lock <> 0)
            if(FB_status == s#errBusy)
              ' v cmd jsou data pro zapis
              rom.WriteRom(F8_cmd)
              F8_cmd := 0                
              lock := 0
          F8_cmd := 0                
          FB_status := s#OK        
  
PUB Send (bytechr)
  repeat until (tx_tail <> ((tx_head + 1) & s#BUFFER_MASK))
  tx_buffer[tx_head] := bytechr
  tx_head := (tx_head + 1) & s#BUFFER_MASK

PUB SetColorMode(mode) | source, txt
  if( mode > 2)
    mode := 0  
  if( mode == currMode )
    return

  case mode
    0:      
      txt := @txtGRAY
      source := @PAL_GRAY
    1:
      txt := @txtRGBM
      source := @PAL_COLOR
    2:
      txt := @txtACE
      source := @PAL_COLORACE

  bytemove(@strMode[12], txt, 4)        
  bytemove(@text, @strMode, 20)
  textOffTimer := cnt
    
  currMode := mode  
  
  bytemove(@pallete, source, 64)     
                 