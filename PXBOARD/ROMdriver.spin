{
***************************************************************************
*    ROM Driver                                                           *
*    Author: Zdenek Sestak                                                *
*    Web: maximalne.8u.cz                                                 *
***************************************************************************
Vlastnosti driveru:
- emulace 8255 v rom modulu PMD85 - distribuce príkazuů a čctení dat
                                      
}
  
CON
        _clkmode = xtal1 + pll16x  '80MHz                                       
        _xinfreq = 5_000_000

OBJ
        s:      "Shared"
VAR
  long cog
  
  long txt      
  word addr
  
PUB Start (Shared)

  if cog
    cogstop(cog~ - 1)

  romDataPointer := @ROM_DATA   ' nastaveni ukazatele na data ROM modulu
  JmpTable := @PortJmpTable     ' jump tabulka je ve sdilene RAM
  F8_cmd_ptr := Shared + s#PAX_CMD 
  F8_data_ptr := Shared + s#PAX_DATA
  FB_ptr := Shared + s#PAX_STATUS
  Lock_ptr := Shared + s#PAX_LOCK  
  Address_ptr := @addr
  palette_ptr := Shared + s#PALETTE
  txt := Shared + s#MESSAGE  
  txt_ptr := Shared + s#MESSAGE  
  cog := cognew(@romemuinit, 0) + 1

  
PUB WriteRom(data)
  'ladici vypis
'  byte[txt][0] := (addr>>8) & 255
'  byte[txt][1] := addr & 255
'  byte[txt][3] := data
'  byte[txt][4] := byte[@ROM_DATA][addr]
  ' zapis
  if(addr => 10*1024)
    'mimo prostor
    RESULT := 1
  else
    'zapise data
    byte[@ROM_DATA][addr] := data
    addr++
    RESULT := 0 
  
PUB Fix3
  ' pro modely 3 se upravi prvni bajt v image, aby se zachoval autostart
  if(byte[@ROM_DATA][0] == $CD AND byte[@ROM_DATA][2] == $8C)   
     byte[@ROM_DATA][0] := $CC
     byte[@ROM_DATA][2] := $EC   

PUB Fix2
  ' pro modely 2/2A se upravi prvni bajt v image, aby se zachoval autostart
  if(byte[@ROM_DATA][0] == $CC AND byte[@ROM_DATA][2] == $EC)   
     byte[@ROM_DATA][0] := $CD
     byte[@ROM_DATA][2] := $8C       
DAT
' ┌───────────────────────────────────────────────────────────────────────────────────────────────────────┐
' │Emulace ROM + driver SD karty                                                                          │
' │- F8 - cteni - vratí data z adresy F9/FA nebo z posledniho prikazu                                     │
' │     - zápis - zapisují se prikazy: cmd (6b) + slot (2b) | [volitelne data]                            │
' │     - SD_INIT  - znova zinicializuje SD kartu                                                         │
' │     - SD_ROOT# - nastavi se na vychozi adresar                                                        │
' │     - SD_DIR#  - zacne vypisovat obsah akt. adresare od zacatku                                       │
' │     - SD_OPEN# - otevre soubor, ktery byl prave vycten prikazem SD_DIR                                │
' │     - SD_RESET# - nastavi datovy stream na zacatek                                                    │
' │     - SD_SEEK#   - posune ukazatel na zadanou pozici                                                  │
' │     - SD_WRITE#  - zapise data do souboru (na aktualni pozici, muze soubor zvetsovat)                 │
' │- F9,FA - zápis a ctení jak do/z registru, nastavuje adresu, ze ktere se bere udaj pro cteni z F8      │
' │- FB    - ctení - status operace - ok, busy, eof, error                                                │
' │- FB    - zapis - je jedno co, ale vede ke zruseni aktuálního prikazu                                  │
' └───────────────────────────────────────────────────────────────────────────────────────────────────────┘
                        org
romemuinit              mov     F8_ptr, romDataPointer                        
       
portloop                
                        andn    OUTA, #$ff
                        andn    DIRA, #$ff

portloop2               waitpeq io_mask, io_mask        ' ceka na WR=1 & RD=1
                                                
                        waitpne io_mask, io_mask        ' ceka na jakoukoliv zmenu na pinech RD/WR
                        mov     datab, INA
                        mov     rtemp, datab
                        shr     rtemp, #8
                        and     rtemp, #$7e
                        add     rtemp, JmpTable
                        rdword  rtemp, rtemp
                        jmp     rtemp

rd_F8                   or      DIRA, #$ff
                        rdbyte  rtemp, F8_ptr
                        rev     rtemp, #24
                        or      OUTA, rtemp                        
                        waitpeq rdio_bit, rdio_bit
                '        andn    OUTA, #$ff
                '        andn    DIRA, #$ff
                '        rdbyte  rtemp, F8_cmd_ptr WZ
               '  if_z   wrbyte  SD_NEXT, F8_cmd_ptr
               '  if_z   wrbyte  c255, FB_ptr                
                        jmp     #portloop
                        
rd_F9                   or      DIRA, #$ff
                        mov     rtemp, ch_F9                
                        rev     rtemp, #24
                        or      OUTA, rtemp
                        waitpeq rdio_bit, rdio_bit
                        jmp     #portloop

rd_FA                   or      DIRA, #$ff
                        mov     rtemp, ch_FA                ' zdrojova adresa se dopocitava
                        rev     rtemp, #24                        
                        or      OUTA, rtemp
                        waitpeq rdio_bit, rdio_bit
                        jmp     #portloop

rd_FB                   or      DIRA, #$ff
                        rdbyte  rtemp, FB_ptr                                                                        
                        rev     rtemp, #24
                        or      OUTA, rtemp
                        waitpeq rdio_bit, rdio_bit
                        jmp     #portloop
                        

wr_F8                   wrbyte  c255, FB_ptr          ' nez SD modul zareaguje, nastavime status na BUSY
                        rev     datab, #24
                        cmp     datab, #$d0 WZ
                  if_z  jmp     #setColor
wr_F8_cmd               mov     F8_ptr, F8_data_ptr   ' dalsi cteni z F8 je prepnuto na data z SD modulu
                        wrbyte  datab, F8_cmd_ptr     ' zapise prikaz pro SD modul
                        jmp     #portloop
wr_F9                   rev     datab, #24
                        mov     ch_F9, datab
                        jmp     #setRomAdr
wr_FA                   rev     datab, #24
                        mov     ch_FA, datab
                        jmp     #setRomAdr
wr_FB                   rev     datab, #24
                        ' vypne zamek, deaktivuje prikaz
                       ' wrbyte  c0, F8_cmd_Ptr
                        wrbyte  c0, Lock_ptr
                        jmp     #portloop
wr_6C                   ' zapis na 6C se prevede jako na prikaz C0 - zmena barevneho rezimu
                        rev     datab, #24
                        and     datab, #$40    WZ, NR  ' pouze, pokud je bit b6 nulovy
                  if_nz jmp     #portloop      
                        wrbyte  c255, FB_ptr          ' nez SD modul zareaguje, nastavime status na BUSY  
                        and     datab, #$0F                              
                        or      datab, #$C0
                        jmp     #wr_F8_cmd   

setRomAdr              ' nastavi adresu pro cteni z F8
                        mov     F8_ptr, ch_FA
                        shl     F8_ptr, #8
                        or      F8_ptr, ch_F9
                        wrword  F8_ptr, address_ptr
                        add     F8_ptr, romDataPointer
                        ' vypne zamek
                       ' wrbyte  c0, Lock_ptr
                        jmp     #portloop
                        ' nastaveni barvy v palete
setColor                and     ch_F9, #63
                        and     ch_F9, #2  WZ, NR   ' lichy/sudy?     
                        wrbyte  c0, FB_ptr
                        add     ch_F9, palette_ptr
                        shl     ch_FA, #2
                        wrbyte  ch_FA, ch_F9                        
                 if_nz  jmp     #portloop
                        ' zapis do sudeho nastavi soucasne i lichy
                        add     ch_F9, #2
                        wrbyte  ch_FA, ch_F9                        
                        jmp     #portloop                        
                                 
io_mask                 long    (1<<s#pinWRIO) + (1<<s#pinRDIO)
rdio_bit                long    (1<<s#pinRDIO)
' pri cteni z adresy F8/FB cteme ze sdilene ram, zde jsou jejich adresy
F8_ptr                  long    0
FB_ptr                  long    0
' zapis prikazu na F8 - vyvola zapis do sdilene RAM
F8_cmd_ptr              long    0
' ukazatel do sdilene pameti, kde se nachazeji SD data
F8_data_ptr             long    0
' ukazatel na paletu
palette_ptr             long    0
' ukazatel na lock
Lock_ptr                long    0
' ukazatel na misto, kam se zapise adresa
address_ptr             long    0
' ukazatel na misto, kam se zapise adresa
txt_ptr             long    0
' následuje buffer pro ctení ze vsech kanálu F8/F9/FAS/FB, musí následovat presne v uvedenem poradí      
ch_F9                   long    $34
ch_FA                   long    $56
romDataPointer          long    0
JmpTable                long    0

c0                      long    0
'SD_NEXT                 long    5               ' cislo prikazu pro dalsi polozku
c255                    long    255
datab                   res     1
rtemp                   res     1
revdata                 res     1
                        fit

' rozskokova tabulka pro cteni/zapis na porty
DAT
                                         {_____  __ __ __ __ __} 
PortJmpTable                             {CS_6x  A2 WR RD A0 A1}
                        word    portloop {  0    0  0  0  0  0      - chyba}     
                        word    portloop        {0  0  0  0  1      - chyba}     
                        word    portloop        {0  0  0  1  0      - chyba}     
                        word    portloop        {0  0  0  1  1      - chyba}
                             
                        word    portloop        {0  0  1  0  0      - chyba}     
                        word    portloop        {0  0  1  0  1      - chyba}     
                        word    portloop        {0  0  1  1  0      - chyba}     
                        word    portloop        {0  0  1  1  1      - chyba}     
                                                                
                        word    portloop        {0  1  0  0  0      - chyba}        
                        word    portloop        {0  1  0  0  1      - chyba}        
                        word    portloop        {0  1  0  1  0      - chyba}        
                        word    portloop        {0  1  0  1  1      - chyba}        
                                                                
                        word    portloop        {0  1  1  0  0      - neaktivni}     
                        word    portloop        {0  1  1  0  1      - neaktivni}     
                        word    portloop        {0  1  1  1  0      - neaktivni}     
                        word    portloop        {0  1  1  1  1      - neaktivni}
                        
                        word    portloop {  0    1  0  0  0  0      - chyba}     
                        word    portloop        {1  0  0  0  1      - chyba}     
                        word    portloop        {1  0  0  1  0      - chyba}     
                        word    portloop        {1  0  0  1  1      - chyba}
                             
                        word    wr_6C           {1  0  1  0  0      - zapis 6C}     
                        word    portloop        {1  0  1  0  1      - zapis 6D}     
                        word    portloop        {1  0  1  1  0      - zapis 6E}     
                        word    portloop        {1  0  1  1  1      - zapis 6F}     
                                                                
                        word    portloop        {1  1  0  0  0      - ctení 6C}        
                        word    portloop        {1  1  0  0  1      - ctení 6D}        
                        word    portloop        {1  1  0  1  0      - ctení 6E}        
                        word    portloop        {1  1  0  1  1      - ctení 6F}        
                                                                
                        word    portloop        {1  1  1  0  0      - neaktivni}     
                        word    portloop        {1  1  1  0  1      - neaktivni}     
                        word    portloop        {1  1  1  1  0      - neaktivni}     
                        word    portloop        {1  1  1  1  1      - neaktivni}  

                        word    portloop {  1    0  0  0  0  0      - chyba}     
                        word    portloop        {0  0  0  0  1      - chyba}     
                        word    portloop        {0  0  0  1  0      - chyba}     
                        word    portloop        {0  0  0  1  1      - chyba}
                             
                        word    wr_F8           {0  0  1  0  0      - zapis F8}     
                        word    wr_FA           {0  0  1  0  1      - zapis FA}     
                        word    wr_F9           {0  0  1  1  0      - zapis F9}     
                        word    wr_FB           {0  0  1  1  1      - zapis FB}     
                                                                
                        word    rd_F8           {0  1  0  0  0      - cteni F8}     
                        word    rd_FA           {0  1  0  0  1      - cteni FA}     
                        word    rd_F9           {0  1  0  1  0      - cteni F9}     
                        word    rd_FB           {0  1  0  1  1      - cteni FB}     
                                                                
                        word    portloop        {0  1  1  0  0      - neaktivni}     
                        word    portloop        {0  1  1  0  1      - neaktivni}     
                        word    portloop        {0  1  1  1  0      - neaktivni}     
                        word    portloop        {0  1  1  1  1      - neaktivni}     
                        word    portloop,portloop,portloop,portloop,portloop,portloop,portloop,portloop
                        word    portloop,portloop,portloop,portloop,portloop,portloop,portloop,portloop
                                                                      
DAT
  
  ROM_DATA
                        'byte    0
                        file  "pax_rom.bin"
'                        file  "basic2A.rmm"                        
                        'byte    $AA[6*1024]
                        'file  "loader.bin"