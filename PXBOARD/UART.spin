{
***************************************************************************
*    UART Driver                                                          *
*    Author: Zdenek Sestak                                                *
*    Web: maximalne.8u.cz                                                 *
***************************************************************************
Vlastnosti driveru:
- full duplex driver s pevnou komunikacni rychlosti
                                      
}
  
CON
        _clkmode        = xtal1 + pll16x
        _xinfreq        = 5_000_000
        
        BAUDRATE = 19200

OBJ
        s: "Shared"
        
VAR

  long  cog                                             'Cog flag/id


PUB Start (Shared)

  if cog
    cogstop(cog~ - 1)
 ' longfill(@rx_head, 0, 9)

  rxhead := Shared + s#RX_HEAD
  txhead := Shared + s#TX_HEAD
  txtail := Shared + s#TX_TAIL
 
  msgptr := Shared + s#MESSAGE
  rxbuff := Shared + s#RX_BUFFER
  txbuff := Shared + s#TX_BUFFER
  SetBaudRate(BAUDRATE)
  'buffer_ptr := @rx_buffer
  cog := cognew(@entry, 0) + 1

PUB SetBaudRate (bd)
  bitticks := clkfreq / bd

 
DAT

'***********************************
'* Assembly language serial driver *
'***********************************

                        org
'
'
' Entry
'
entry                   test    rxtxmode,#%100  wz    'init tx pin according to mode
                        test    rxtxmode,#%010  wc
        if_z_ne_c       or      outa,txmask
        if_z            or      dira,txmask

                        mov     txcode,#transmit      'initialize ping-pong multitasking
'
'
' Receive
'
receive                 jmpret  rxcode,txcode         'run chunk of tx code, then return

                        test    rxtxmode,#%001  wz    'wait for start bit on rx pin
                        test    rxmask,ina      wc
        if_z_eq_c       jmp     #receive

                        mov     rxbits,#9             'ready to receive byte
                        mov     rxcnt,bitticks
                        shr     rxcnt,#1
                        add     rxcnt,cnt                          

:bit                    add     rxcnt,bitticks        'ready next bit period

:wait                   jmpret  rxcode,txcode         'run chunk of tx code, then return

                        mov     t1,rxcnt              'check if bit receive period done
                        sub     t1,cnt
                        cmps    t1,#0           wc
        if_nc           jmp     #:wait

                        test    rxmask,ina      wc    'receive bit on rx pin
                        rcr     rxdata,#1
                        djnz    rxbits,#:bit

                        shr     rxdata,#32-9          'justify and trim received byte
                        and     rxdata,#$FF
                        test    rxtxmode,#%001  wz    'if rx inverted, invert byte
        if_nz           xor     rxdata,#$FF

                        rdbyte  t2,rxhead             'save received byte and inc head
                        add     t2,rxbuff
                        wrbyte  rxdata,t2
                        
                       ' wrbyte  rxdata, msgptr       ' testovaci zapis do bufferu na obrazovce
                        
                        sub     t2,rxbuff
                        add     t2,#1
                        and     t2,#s#BUFFER_MASK
                        wrbyte  t2,rxhead

                        jmp     #receive              'byte done, receive next byte
'
'
' Transmit
'
transmit                jmpret  txcode,rxcode         'run chunk of rx code, then return
                       '      jmp     #transmit
                    '    mov     t1,par                'check for head <> tail
                    '    add     t1,#2 << 2
                    '    rdlong  t2,t1
                    '    add     t1,#1 << 2
                    '    rdlong  t3,t1
                        rdbyte  t2, txhead
                        rdbyte  t3, txtail
                        cmp     t2,t3           wz
        if_z            jmp     #transmit

                        add     t3,txbuff             'get byte and inc tail
                        rdbyte  txdata,t3
                        sub     t3,txbuff
                        add     t3,#1
                        and     t3,#s#BUFFER_MASK
                       ' wrlong  t3,t1
                        wrbyte  t3, txtail 

                        or      txdata,#$100          'ready byte to transmit
                        shl     txdata,#2
                        or      txdata,#1
                        mov     txbits,#11
                        mov     txcnt,cnt

:bit                    test    rxtxmode,#%100  wz    'output bit on tx pin 
                        test    rxtxmode,#%010  wc    'according to mode
        if_z_and_c      xor     txdata,#1
                        shr     txdata,#1       wc
        if_z            muxc    outa,txmask        
        if_nz           muxnc   dira,txmask
                        add     txcnt,bitticks        'ready next cnt

:wait                   jmpret  txcode,rxcode         'run chunk of rx code, then return

                        mov     t1,txcnt              'check if bit transmit period done
                        sub     t1,cnt
                        cmps    t1,#0           wc
        if_nc           jmp     #:wait

                        djnz    txbits,#:bit          'another bit to transmit?

                        jmp     #transmit             'byte done, transmit next byte
msgptr                  long    0                        

rxmask                  long    1 << s#pinRXD
txmask                  long    1 << s#pinTXD
rxtxmode                long    0

rxbuff                  long    0
txbuff                  long    0
bitticks                long    0

rxhead                  long    0
txhead                  long    0
txtail                  long    0
'
'
' Uninitialized data
'
t1                      res     1
t2                      res     1
t3                      res     1


rxdata                  res     1
rxbits                  res     1
rxcnt                   res     1
rxcode                  res     1

txdata                  res     1
txbits                  res     1
txcnt                   res     1
txcode                  res     1