{
***************************************************************************    
*    VGA 288x256 bitmap -> 800x600/60Hz                                   *
*    Author: Zdenek Sestak                                                *
*    Web: maximalne.8u.cz                                                 *
***************************************************************************

 Tento program generuje VGA signál - obraz v rozlisení 288x256 se zobrazuje
 s dvojnásobným zvetsením 576x512 podle techto casových údaju:

     Scanline part   Pixels  Time [µs]
     Visible area    800     20
     Front porch     40      1
     Sync pulse      128     3.2
     Back porch      88      2.2
     Whole line      1056    26.4

     Frame part      Lines   Time [ms]
     Visible area    600     15.84
     Front porch     1       0.0264
     Sync pulse      4       0.1056
     Back porch      23      0.6072
     Whole frame     628     16.5792
                 
}

CON
        _clkmode = xtal1 + pll16x  '80MHz                                       
        _xinfreq = 5_000_000

        ' Parametry 800x600 @ 60Hz
        hp = 800      'horizontal pixels
        vp = 600      'vertical pixels
        hf = 48       'horizontal front porch pixels
        hs = 128      'horizontal sync pixels
        hb = 80       'horizontal back porch pixels
        vf = 1        'vertical front porch lines
        vs = 4        'vertical sync lines
        vb = 23       'vertical back porch lines
        hn = 0        'horizontal normal sync state (0|1)
        vn = 0        'vertical normal sync state (0|1)
        pr = 20       'pixel rate in MHz at 80MHz system clock (5MHz granularity)

        ' Výchozí kombinace barev  
        RED = $8000
        GREEN = $2000
        BLUE = $0C00
        YELLOW = $F000
        WHITE = $FC00
        LIGHTGRAY = $A800
        GRAY = $5400
        DARKGRAY = $5400
        CYAN = $3c00
        MAGENTA = $8C00


        ' H/V inactive states  
        hv_inactive = (hn << 1 + vn) * $0101

VAR
        long cog, colorcog, buscog 
        'Deklarace sdílené pameti, kterou pouzívají vsechny moduly
        byte  VIDEORAM[48*256]          'VIDEORAM
        word  COLORB[96]                'COLOR BUFFER
OBJ
        s:      "Shared"
PUB start (Shared)

  'Pokud je driver jiz spusteny, zastavi se
  if buscog
    cogstop(buscog~ - 1)
  if cog
    cogstop(cog~ - 1)
  if colorcog
    cogstop(colorcog~ - 1)

  ' odkazy na globální prom. pro jednotlivé cogy 
  pixels :=  @VIDEORAM
  colors :=  @COLORB
  pixels2 := @VIDEORAM 
  colors2 := @COLORB
  textPtr := Shared + s#MESSAGE
  textPtr2 := Shared + s#MESSAGE
  palettePtr := Shared + s#PALETTE
  buspixels2 :=  @VIDEORAM
  c17d_adr := Shared + s#C17D

  fontPtr := $8000 

  cog      := cognew(@init, 0) + 1
  colorcog := cognew(@colorinit, 0) + 1
  buscog   := cognew(@businit2, 0) + 1                 ' synchronizuje se signálem FI2

 
  {repeat
    waitcnt(cnt + 100000000)
    bytemove(@message, @strSpace, 20)
    repeat
      if (INA & 1<<pinBUTTON) == 10                      ' stisk buttonu nastaví jiný mód
        COLORMODE := (COLORMODE+1)&7
        waitcnt(cnt + 80000000/4)
        repeat
        until (INA & (1<<pinBUTTON)) == 0
    until oldColorMode <> COLORMODE
    bytemove(@message, @strMode, 20)
    message[12] := COLORMODE + 48
    oldColorMode := COLORMODE
    }
  
DAT
  

DAT
'┌────────────────────────────────────────────────────────────────────────────────────────────────────┐
'│Pixel procesor                                                                                      │
'│- generuje 6 pixelový údaj, vybírá barvu z colorbufferu                                             │
'│- popis funkce:                                                                                     │
'│  1) generování synchronizace                                                                       │
'│  2) vykreslení 2xliché linky + kopírování barev pro sudou linku z globální RAM do colorbufferu2    │
'│  3) vykreslení 2xsudé linky + kopírování barev pro lichou linku z globální RAM do colorbufferu     │
'└────────────────────────────────────────────────────────────────────────────────────────────────────┘

                        org                             'start programuv COG od $000 

' Inicializace
init                    mov     VCFG, regVCFG
                        mov     CTRA, regCTRA
                        mov     FRQA, regFRQA
                        mov     vscl, #1
                        mov     CNT, CNT
                        add     CNT, delay
                        waitcnt CNT, #0
                        mov     vscl, #100
                        mov     DIRA, regDIRA
                        
' Hlavní cykl vykreslování
loop                    mov     x, #4
                        call    #blank

                        mov     vscl, hpixels           
                        waitvid hvsync,#0

                        'kopie text z RAM do lokálního cogu
                        mov     pptr, textPtr
                        rdlong  pattern, pptr
                        add     pptr,#4
                        mov     text, pattern

                        rdlong  pattern, pptr
                        add     pptr,#4
                        mov     text+1, pattern


                        rdlong  pattern, pptr
                        add     pptr,#4
                        mov     text+2, pattern


                        rdlong  pattern, pptr
                        add     pptr,#4
                        mov     text+3, pattern


                        rdlong  pattern, pptr
                        add     pptr,#4
                        mov     text+4, pattern

                        mov     x,#1
                        call    #hsync
                    
                        'zobrazení textu ve 32 mikrorádcích
                        mov y ,#32
                        
                        mov     cptr, fontPtr
txtline                 mov     vscl, #80           
                        waitvid hvsync,#0
                        mov     vscl, vscl_txt           
                        mov     vcfg, regVCFGtxt           
                        movs    txtchar, #text
                        mov     y1, #5
txtchar                 mov     colattr1, pptr
                        rol     colattr1, #6            ' predbezny posun pro vypocet adresy fontu
                        mov     pattern, colattr1
                        and     pattern, patmask
                        and     colattr1, #(1 <<6)     WZ, NR
                        add     pattern, cptr
                        rdlong  pattern, pattern 
          if_z          shl     pattern, #1              
                        waitvid BWColor,pattern

                        ror     colattr1, #8
                        mov     pattern, colattr1
                        and     pattern, patmask 
                        and     colattr1, #(1<<6)     WZ, NR
                        add     pattern, cptr
                        rdlong  pattern, pattern 
          if_z          shl     pattern, #1                       
                        waitvid BWColor,pattern

                        ror     colattr1, #8
                        mov     pattern, colattr1
                        and     pattern, patmask  
                        and     colattr1, #(1<<6)     WZ, NR
                        add     pattern, cptr
                        rdlong  pattern, pattern 
          if_z          shl     pattern, #1                                 
                        waitvid BWColor,pattern

                        ror     colattr1, #8
                        and     colattr1, #(1<<6)     WZ, NR
                        and     colattr1, patmask 
                        add     colattr1, cptr
                        add     txtchar, #1
                        rdlong  pattern, colattr1 
          if_z          shl     pattern, #1              
                        waitvid BWColor,pattern

                        djnz    y1, #txtchar

                        mov     vscl, #80           
                        waitvid hvsync,#0

                        mov     x,#1
                        call    #hsync

                        add     cptr, #4
                        djnz    y, #txtline
                     
                        'mezera mezi textem a grafickou obrazovkou
                        mov x, #6
                        call    #blank
                        mov     vscl, hpixels         '800 pixelová mezera
                        waitvid hvsync,#0
                        mov     VCFG, regVCFG  

                        'kopie barev do colorbuffer
                        mov     x, #24
                        mov     cptr, colors
                        movd    cllop, #colorbuffer   'mezi touto a modifikovanou instrukcí musí být min 1 instrukce  
                        mov     cptr, colors
cllop                   rdlong  colorbuffer, cptr
                        add     cptr, #4
                        add     cllop, destinc
                        djnz    x, #cllop

                        mov     x,#1
                        call    #hsync            

                        mov     pptr, pixels
                        mov     y, #128
'vykreslení lichých linek bitmapy ve dvou scanline na monitoru+kopie barev pro sudé linky
bitmapline              mov     y1, #2
                        mov     cptr, colors
                        add     cptr, #96
:scanline               mov     vscl, #112           
                        waitvid hvsync,#0                                       'vynechaný prostor vlevo

                        rdlong  colorbuffer2, cptr
                        mov     colattr1, colorbuffer
                        add     cptr, #4
                        rdlong  colorbuffer2+1, cptr
                        movs    :cl1ptr, #colorbuffer
                        add     cptr, #4
                        rdlong  colorbuffer2+2, cptr
                        movs    :cl2ptr, #colorbuffer+1
                        add     cptr, #4
                        rdlong  colorbuffer2+3, cptr
                        mov     vscl, vscl_pixel
                        add     cptr, #4
                        rdlong  colorbuffer2+4, cptr
                        mov     x, #12
                        add     cptr, #4
                        rdlong  colorbuffer2+5, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+6, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+7, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+8, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+9, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+10, cptr
                        add     cptr, #4                        
                        rdlong  colorbuffer2+11, cptr     
                        add     cptr, #4                        
                        
:line                   rdlong  pattern, pptr
                        waitvid colattr1,pattern
                        shr     pattern, #8
                        shr     colattr1, #16
:cl2ptr                 mov     colattr2, colorbuffer

                        add     :cl1ptr, #2                                                
                        waitvid colattr1,pattern
                        shr     pattern, #8
:cl1ptr                 mov     colattr1, colorbuffer
                        add     :cl2ptr, #2                                                
                        waitvid colattr2,pattern
                        add     pptr, #4
                        shr     colattr2, #16
                        shr     pattern, #8
                        waitvid colattr2,pattern                        
                        djnz    x, #:line                           

                        mov     vscl, #112 + hf                                 'vynechaný prostor vpravo + front porch
                        waitvid hvsync,#0
                        sub     pptr, #48

                        rdlong  colorbuffer2+12, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+13, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+14, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+15, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+16, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+17, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+18, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+19, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+20, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+21, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+22, cptr
                        add     cptr, #4
                        rdlong  colorbuffer2+23, cptr     
                        sub     cptr, #23*4

                        mov     vscl,#hs                                        'horizontální synchronizace
                        xor     hvsync,#2        
                        waitvid hvsync,#0
                        xor     hvsync,#2

                        mov     vscl,#hb              
                        waitvid hvsync,#0

                        djnz    y1, #:scanline                                  'konec vykreslení jednoho scanline                
                        add     pptr, #48
'vykreslení sudých linek bitmapy ve dvou scanline na monitoru
bitmapline2             mov     y1, #2
                        mov     cptr, colors
:scanline               mov     vscl, #112                                      'vynechaný prostor vlevo
                        waitvid hvsync,#0

                        rdlong  colorbuffer, cptr
                        mov     colattr1, colorbuffer2
                        add     cptr, #4
                        rdlong  colorbuffer+1, cptr
                        movs    :cl1ptr, #colorbuffer2
                        add     cptr, #4
                        rdlong  colorbuffer+2, cptr
                        movs    :cl2ptr, #colorbuffer2+1
                        add     cptr, #4
                        rdlong  colorbuffer+3, cptr
                        mov     vscl, vscl_pixel
                        add     cptr, #4
                        rdlong  colorbuffer+4, cptr
                        mov     x, #12
                        add     cptr, #4
                        rdlong  colorbuffer+5, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+6, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+7, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+8, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+9, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+10, cptr
                        add     cptr, #4                        
                        rdlong  colorbuffer+11, cptr     
                        add     cptr, #4                    

:line                   rdlong  pattern, pptr
                        waitvid colattr1,pattern
                        shr     pattern, #8
                        shr     colattr1, #16
:cl2ptr                 mov     colattr2, colorbuffer2

                        add     :cl1ptr, #2                                                
                        waitvid colattr1,pattern
                        shr     pattern, #8
:cl1ptr                 mov     colattr1, colorbuffer2
                        add     :cl2ptr, #2                                                
                        waitvid colattr2,pattern
                        add     pptr, #4
                        shr     colattr2, #16
                        shr     pattern, #8
                        waitvid colattr2,pattern                        
                        djnz    x, #:line                           

                        mov     vscl, #112 + hf        
                        waitvid hvsync,#0
                        sub     pptr, #48

                        rdlong  colorbuffer+12, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+13, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+14, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+15, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+16, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+17, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+18, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+19, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+20, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+21, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+22, cptr
                        add     cptr, #4
                        rdlong  colorbuffer+23, cptr     
                        sub     cptr, #23*4

                        mov     vscl,#hs                                        'hor. synchronizace
                        xor     hvsync,#2        
                        waitvid hvsync,#0
                        xor     hvsync,#2

                        mov     vscl,#hb              
                        waitvid hvsync,#0
                        
                        djnz    y1, #:scanline                                  'konec vykreslení jednoho scanline                
                        add     pptr, #48

                        djnz    y, #bitmapline                                  'konec vykreslení jednoho bitmapline

                        'prázdné linky za obrazem
                        mov     x, #44
                        call    #blank

                        'vertikální synchronizace 
                        mov     x,#vf                   'vertical front porch
                        call    #blank
                        mov     x,#vs                   'vertical sync lines
                        call    #vsync
                        mov     x,#vb                   'vertical back porch 
                        call    #vsync
                        jmp     #loop                   'snímek vykreslen, opakování
                               

' Podprogram - prázdné linky

vsync                   xor     hvsync,#$1            'neguje bit vertikální synchronizace

blank                   mov     vscl, hpixels         '800 pixelová mezera
                        waitvid hvsync,#0

hsync                   mov     vscl,#hf              'front porch
                        waitvid hvsync,#0

                        mov     vscl,#hs              'horizontální synchro
                        xor     hvsync,#2        
                        waitvid hvsync,#0
                        xor     hvsync,#2

                        mov     vscl,#hb              'back porch
                        waitvid hvsync,#0
                        djnz    x,#blank              

hsync_ret
blank_ret
vsync_ret               ret


delay                   long    $30000
' Data
' Vystupni rezim na P16..P23
regDIRA                 long    %11111111 << 16          

'Konfigurace video adaptéru:
' - VGA mode, 8 bit
' - Color Mode = 2 color mode
' - VGroup = P16..P23
' - VPins = vystup na vsech 8 pinech
regVCFG                 long    %0_01_0_0_0_000_00000000000_010_0_11111111
regVCFGtxt              long    %0_01_1_0_0_000_00000000000_010_0_11111111

'Konfigurace COUNTER A:
' - frekvence = 40MHz = 8 x 5MHz
' - MODE = 1 PLL internal (video mode)
' - PLLDIV = VCO/2      (NCOx8)
' - BPIN = APIN = 0             
regCTRA                 long    %0_00001_110_00000000_000000_000_000000

' Frekvence NCO = 5MHz = 1/16 * 80MHz   
regFRQA                 long    16 << 24

hpixels                 long    hp
pixels                  long    0                       'ukazatel na data v RAM
colors                  long    0                       'ukazatel na color buffer v RAM
pptr                    long    pattern                 'pracovní registr
cptr                    long    0
destinc                 long    %1000000000

colattr1                long    %00101000_00000100
colattr2                long    %00101000_00000100
BWcolor                 long    %00111100_00111100_00000000_00000000
fontPtr                 long    0

vscl_pixel              long    2 << 12 + 12            '1 pixel per 2 clock and 12 clocks per set
vscl_txt                long    2 << 12 + 32
hvsync                  long    hv_inactive ^ $200      '+/-H,-V states
pattern                 long    %10101010_10101010_10101010_10101010
patmask                 long    $fe<<6
textPtr                 long    0

' Lokální neinicializované registry
colorbuffer             res     48 * 2 / 4
colorbuffer2            res     48 * 2 / 4
text                    res     5    
x                       res     1
y                       res     1
y1                      res     1
                        fit

DAT
'┌──────────────────────────────────────────────────────────────────────────────────────────┐
'│Color procesor                                                                            │
'│ - pripravuje barvy podle barevnych atributu jednoho radku                                │
'└──────────────────────────────────────────────────────────────────────────────────────────┘
                        org
colorinit               
                        mov     rega, #0 WC, WR         'vynulování counteru a Carry bitu
                        mov     colorinit, defc         'úprava tabulky barev, která musí startovat od adresy 0

                        waitpeq VSmask, VSmask          'pauznutí do aktivace vertikální synchronizace
                        waitpne VSmask, VSmask

                        
                        mov     temp, #vb+40 
                        call    #waitHSX

                        mov     pal_start, #PAL_GLOBAL
                     
                        ' kopie globalni palety do cogu
                        mov     temp, palettePtr
                        movd    destPal, #PAL_GLOBAL
                        mov     LineCounter, #32
copypallete             rdword  rega, temp
                        add     temp, #2                        
destPal                 mov     PAL_GLOBAL, rega
                        add     destPal, #511
                        add     destPal, #1
                        djnz    LineCounter, #copypallete
                                      
                        mov     pixPtr, pixels2
                        mov     colPtr, colors2
                        call    #Colorace
                        call    #waitHS
                        call    #Colorace2
                        call    #waitHS                        
                        call    #waitHS                        
                        call    #waitHS                        
                        call    #waitHS                        
                        'snímková inicializace
                        mov     LineCounter, #127

                        'linková inicializace
coloop                  mov     colPtr, colors2
                        call    #Colorace

                        'pauza do horizontální synchronizace
                        call    #waitHS
                        call    #waitHS
                        call    #Colorace2
                        
                        call    #waitHS
                        call    #waitHS
                        djnz    LineCounter, #coloop
                                                
                        jmp     #colorinit
'podprogram pro nastavení barev jedné linky
'Vstup: pixPtr - ukazatel na zpracovávanou linku
'       colPtr - ukazatel na výstupní buffer (48xword)
Colorace2               mov     pixPtr2, pixPtr
                        sub     pixPtr2, #48
                        movs    regas, #5               'nutno upravit pozici atrib., pokud se skládají z sudé linky  
                        movs    regbs, #3               'aby byl stejný kód barvy
                        mov     pal_offset, pal_start
                        add     pal_offset, #1                        
                        jmp     #cac                        
Colorace                mov     pixPtr2, pixPtr
                        add     pixPtr2, #48
                        movs    regas, #3
                        movs    regbs, #5
                        mov     pal_offset, pal_start                         
cac                     mov     temp, #12
caloop                  rdlong  rega, pixPtr
                        add     pixPtr, #4
                        and     rega, attrmask          'a4000000_a3000000_a2000000_a1000000

                        rdlong  regb, pixPtr2
                        add     pixPtr2, #4
                        and     regb, attrmask          'b4000000_b3000000_b2000000_b1000000
regas                   shr     rega, #5                '00000a40_00000a30_00000a20_00000a10
regbs                   shr     regb, #3                '000b4000_000b3000_000b2000_000b1000
                        or      rega, regb              '000b4a40_000b3a30_000b2a20_000b1a10
                        movs    caa1, rega              '                         |  9bit   |
                        add     caa1, pal_offset
                        
                        shr     rega, #8                '00000000_000b4a40_000b3a30_000b2a20
                        movs    caa2, rega              '                         |  9bit   |                                                                                                 
                        add     caa2, pal_offset
                                                
                        shr     rega, #8                '00000000_00000000_000b4a40_000b3a30
                        movs    caa3, rega              '                         |  9bit   |                                                                                                 
                        add     caa3, pal_offset

                        shr     rega, #8                '00000000_00000000_00000000_000b4a40
                        movs    caa4, rega              '                         |  9bit   |                                                                                                 
                        add     caa4, pal_offset
                        
caa1                    mov     regc1, 0                        
                        wrword  regc1, colPtr
                        add     colPtr, #2                        
caa2                    mov     regc2, 0
                        wrword  regc2, colPtr
                        add     colPtr, #2                        
caa3                    mov     regc3, 0                        
                        wrword  regc3, colPtr
                        add     colPtr, #2                        
caa4                    mov     regc4, 0                        

                        wrword  regc4, colPtr
                        add     colPtr, #2                                                   
                        djnz    temp, #caloop
Colorace2_ret
Colorace_ret            ret                                    
' Podprogram - pauznutí do aktivace horizontální synchronizace
waitHS                  mov     temp, #1
waitHSX                 waitpeq HSmask, HSmask          
                        waitpne HSmask, HSmask          
                        djnz    temp, #waitHSX
waitHSX_ret
waitHS_ret              ret                        

Zero                    long    0
LineCounter             long    0     ' aktuální index vykreslované linky (0 - 627)
HSmask                  long    %10 << 16  ' P17 = horizontální synchro
VSmask                  long    %1  << 16  ' P16 = vertikální synchro
temp                    long    0                        
colors2                 long    0
pixels2                 long    0
pixPtr                  long    0
pixPtr2                 long    0
colPtr                  long    0
rega                    long    0
regb                    long    0
regc1                   long    0
regc2                   long    0
regc3                   long    0
regc4                   long    0
defc                    long    GREEN
cmask                   long    $FCFCFCFC
attrmask                long    $C0C0C0C0
palettePtr              long    0             'ukazatel na globální paletu
PAL_GLOBAL              'long    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0    ' prostor pro nacteni globalni palety
                        long   GREEN, GREEN, YELLOW, YELLOW, CYAN, CYAN, WHITE, WHITE
                        long    YELLOW, YELLOW, RED, RED, MAGENTA, MAGENTA, MAGENTA, MAGENTA
                        long    CYAN, CYAN, MAGENTA, MAGENTA, BLUE, BLUE, MAGENTA, MAGENTA
                        long    WHITE, WHITE, MAGENTA, MAGENTA, MAGENTA, MAGENTA, MAGENTA, MAGENTA                                  
pal_start               res     1                                                  
pal_offset              res     1                                    
                        fit       

DAT
' ┌───────────────────────────────────────────────────────────────────────────────────────────────────────┐
' │Bus driver 2                                                                                           │
' │- nacita data z datove sbernice, identifikuje video data a kopiruje je do vnitrni pameti               │  
' │- sleduje jediný kontrolní signál - FI2                                                                │  
' └───────────────────────────────────────────────────────────────────────────────────────────────────────┘
                        org
businit2                mov     iodata0, #0        WC
bussyncstart            mov     tempcnt, screensize
:syncloop               waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        cmp     iodata0, syncpattern WZ    'nalezen vzor synchronizace?
                 if_z   jmp     #:syncend      
                        sub     tempcnt, #1      WZ        'pokud jsme projeli celý screen,                     
                 if_z   jmp     #bussyncstart              ' posuneme se o 1 takt FI2 a jedem znova
                        nop
                        nop
                        nop
                        nop
                        nop
                        shr     iodata0, #8
                        mov     iodata2, INA
                        rev     iodata2, #24                        
                        shl     iodata2, #24            'posun datového údaje na pozici MSB                        
                        and     iodata0, clearmask      'umazání MSB pozic
                        or      iodata0, iodata2        'spojení do jednoho longu
                        jmp     #:syncloop
:syncend                waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        'získání 4 bajtového údaje z datového vstupu
                        call    #readLong
                        call    #readLong
                        call    #readLong
                        'test, zda jsme nechytli patern, ktery se opakuje behem zatmeni
                        'pokud ano, znova se zopakuje patern po 64 * 64 - 5 bajtech (- 10 clock)
                        mov     tempcnt, blanksize
                        add     tempcnt, tempcnt
                        sub     tempcnt, #10+12*2 + 1       'trefime se na druhy potencialni synchr. pattern
                        waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        wrbyte  iodata0, c17d_adr       ' zapise bajt z adresy C17D - u modelu 2/2A je tady 0x7F, u modelu 3 0xBE
                        
:skiploop               waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        'and     iodata0, #$FF                        
'                        wrbyte  iodata0, c17d_adr       ' zapise bajt z adresy C17D - u modelu 2/2A je tady 0x7F, u modelu 3 0xBE 
                        djnz    tempcnt, #:skiploop
                        
                        'získání 4 bajtového údaje z datového vstupu
                        call    #readLong                        
                        ' zbývá jen zarovnat obraz                   
                        cmp     iodata0, syncpattern WZ
                        mov     tempcnt, skipvalue1
              if_z      mov     tempcnt, skipvalue2     'pokud jsme se synchronizovali na jiné místo                        
:skiploop2              waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        djnz    tempcnt, #:skiploop2                                                                                          
blanklines             ' blank lines
                        mov     tempcnt, blanksize
                        sub     tempcnt, #1                                          
:blankrowloop           waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        'nastavení pro následující fázi (mezi cykly nebude prostor)
                        mov     row2, #256
                        mov     col2, #48                    
                       ' mov     pxPtr2, buspixels2
                        waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        djnz    tempcnt, #:blankrowloop
                        'posledni byte 
                        waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        mov     pxPtr2, buspixels2
                        sub     pxPtr2,#1 
                        waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        'cteni video udaju ze sbernice
                        'synchronizace probiha kazdym druhym taktem fi2 (jeden se propasne)                        
:renderloop             waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                  '      test    rdiomask, INA WZ
                        mov     ttt, cnt
                        add     ttt, #17
                        waitcnt ttt,ttt
                        add     pxPtr2, #1
                        mov     iodata2, INA
                        rev     iodata2, #24
                        wrbyte  iodata2, pxPtr2                        
                        djnz    col2, #:renderloop
                        '16 video udaju se nevykresluje - radkove zatemneni
                        mov     tempcol, #16
:blankloop              waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        mov     col2, #48
                       ' mov     tempcnt, blanksize
                        waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        djnz    tempcol, #:blankloop
                        djnz    row2, #:renderloop
                        
                        jmp     #blanklines
                        'získání 4 bajtového údaje z datového vstupu 
readLong                mov     tempcnt, #4
readLong_getpat         waitpne FI2mask, FI2mask
                        waitpeq FI2mask, FI2mask
                        mov     ttt, cnt
                        sub     tempcnt, #1     WZ
                        add     ttt, #17
                        waitcnt ttt,ttt
                        shr     iodata0, #8
                        mov     iodata2, INA
                        rev     iodata2, #24
                        
                        shl     iodata2, #24            'posun datového údaje na pozici MSB
                        and     iodata0, clearmask      'umazání MSB pozic
                        or      iodata0, iodata2        'spojení do jednoho longu
                  if_nz jmp     #readLong_getpat
readLong_ret            ret        
                        
FI2mask                 long    1<<s#pinFI2
rdiomask                long    1<<s#pinRDIO + 1<<s#pinA2                            
buspixels2              long    0                       ' Ukazatel do interní RAM
c17d_adr                long    0                       ' Ukazatel, kam se ulozi bajt z adresy c17d (rozpoznani modelu 2/2a a 3)
clearmask               long    $00FFFFFF               ' maska pro umazání MSB pozic
syncpattern             long    $75636578 ' "xecu"      ' posloupnost, podle které synchronizujeme obraz
screensize              long    320*64                  ' jak velkou posloupnost prohledáváme
blanksize               long    64*64                   ' velikost zatemnene casti v bajtech 
textPtr2                long    0                       ' ukazatel na textovou zprávu
skipvalue1              long    24 + 128 * (122+64)   
skipvalue2              long    24 + 128 * 250
ttt                     res     1
tempcnt                 res     1
temppat                 res     1
pxPtr2                  res     1                                               
col2                    res     1
row2                    res     1
tempcol                 res     1
temprow                 res     1
iodata2                 res     1
iodata0                 res     1    
                        fit
                                                                              