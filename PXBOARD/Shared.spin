{
***************************************************************************    
*    Nastaveni PAX modulu                                                 *
*    Author: Zdenek Sestak                                                *
*    Web: maximalne.8u.cz                                                 *
***************************************************************************


               1k0   ┌─────────────────────┐
    D7  ──────────┤P0                P31├──── RXD
               1k0   │                     │                    3V3       3V3
    D6  ──────────┤P1                P30├──── TXD                      
               1k0   │                     │                  10k     ┌───┴───┐                                        
    D5  ──────────┤P2                P29├──── I²C - SDA ─────┻─────┤SDA  A0├┐                         LE33          
               1k0   │                     │                           │     A1├┫  24LC256               ┌─────┐        
    D4  ──────────┤P3                P28├──── I²C - SCL ───────────┤SCL  A2├┫                  3V3 ─┤     ├── +5V 
               1k0   │                     │                           └───┬───┘│                        └──┬──┘        
    D3  ──────────┤P4                P27├─  SD_DO (MISO)                                               ┣───── GND 
               1k0   │                     │                                                                           
    D2  ──────────┤P5                P26├─  SD_SCK                                                                         
               1k0   │                     │                
    D1  ──────────┤P6                P25├─  SD_DI (MOSI)
               1k0   │                     │
    D0  ──────────┤P7                P24├─  SD_CS
                     │                     │
                   ┌─┤VSS               VDD├─  3V3              
               GND─┫ │                     │                    
                   └─┤BOEn               XO├─┐                  
                     │                     │  XTAL 5MHz        
            DTR ────┤RESn               XI├─┘                  
                     │                     │                    
                3V3 ─┤VDD               VSS├─ GND        
              8x10k  │                     │              
    FI2 ──────────┤P8                P23├───┐240        
                     │                     │     ┣── RED    
    A1  ──────────┤P9                P22├───┘470        
                     │                     │                 
    A0  ──────────┤P10               P21├───┐240        
                     │                     │     ┣── GREEN  
    RD  ──────────┤P11               P20├───┘470        
                     │                     │                 
    WR  ───-──────┤P12               P19├───┐240        
                     │                     │     ┣── BLUE   
CS (A2) ──────────┤P13               P18├───┘470        
                     │                     │ 240             
CS_6x   ──────────┤P14               P17├────── HSYNC
                     │                     │ 240             
         3V3 ───┳─┤P15               P16├────── VSYNC  
                   │ └─────────────────────┘
                    ├ color mode switch 
                   │
                     
} 
CON
        _clkmode = xtal1 + pll16x  '80MHz                                       
        _xinfreq = 5_000_000
        
        '----------------------------------------------------------------
        ' Prirazeni signalu k pinum
        '----------------------------------------------------------------
        pinFI2 = 8                 ' vstup signálu FI2
        pinA0 = 10
        pinA1 = 9
        pinA2 = 13                                     
        pinRDIO = 11               ' vstup signálu RDIO - ctení procesoru z I/O prostoru                      
        pinWRIO = 12               ' vstup signálu WRIO - zápis procesoru do I/O prostoru                      
        pinCS_6x = 14              ' vstup signalu PAX_CS - pristup na ridici porty 6C-6F
        pinBUTTON = 15          
        pinRXD = 31
        pinTXD = 30                    


        ' pripojeni SD karty na piny
        pinSDCS = 24
        pinDI   = 25
        pinSCK  = 26            
        pinDO   = 27

        BUFFER_LENGTH = 64                 ' velikost vysilacich a prijimacich bufferu, musi byt mocnina 2
        BUFFER_MASK   = BUFFER_LENGTH - 1  ' pro rychly vypocet modulo

        FI2_OFFSET = 11                    ' vychozi delay zpozdeni od FI2, pokud NENI IMPLEMENTOVANO
        
        
        '----------------------------------------------------------------
        ' cisla chyb
        '----------------------------------------------------------------
        OK     = 0                                      ' bez chyby
        errCardInitError = 1                            ' chyba behem inicializace SD karty
        errReadSectorError = 2                          ' chyba behem cteni sektoru
        errBadFormat = 3                                ' na karte neni exFAT
        errEOD = 4                                      ' dosazeno konce vypisu
        errEOF = 5                                      ' dosazeno konce souboru
        errTimeout = 6                                  ' vyprseni casu
        errRxEmpty = 10                                 ' UART neprijal zadna data
        errRomFull = 11                                 ' do ROM modulu se uz vice neda zapsat
        errWaitForData = 127                            ' ceka se na zapis dat (napr. pro UART)                                        
        errBusy = $FF                                   ' operace neni jeste dokoncena

        '----------------------------------------------------------------
        ' prikazy
        '----------------------------------------------------------------
        ' NOP
        CMD_NOP = 0
        ' inicializace SD karty - vhodne vzdy na zacatku nebo vymene karty        
        CMD_INIT = 1
        ' nastavi root jako aktualni adresar
        CMD_ROOT = 2
        ' zahaji vypis polozek z aktualniho adresare, polozka je tvorena 20 bajty, po 20 bajtech
        ' se automaticky vycte obsah souboru
        CMD_DIR_START = 3
        ' presune se na dalsi polozku v adresari
        CMD_DIR_NEXT = 4
        ' nacte dalsi bajt z polozky/souboru
        CMD_NEXT_BYTE = 5
        ' resetuje ukazatel na zacatek streamu - 20 B popis + obsah souboru
        CMD_FILE_RESET = 6
        ' nastavi 4B ukazatel do souboru
        CMD_SEEK = 7
        ' zahaji cteni z UARTU
        CMD_UART_READ = 10    
        ' zahaji vysilani pres UART
        CMD_UART_SEND = 11

        ' zapis noveho obsahu ROM modulu
        CMD_ROM_WRITE = $20

        ' nastaveni barevneho rezimu = CMD_COLORMODE + index rezimu (4 bity)
        CMD_COLORMODE = $C0

        ' nastaveni palety = CMD_SETCOLOR + 16x barva popredi (pro sudy i lichy mikroradek), + 16x barva pozadi (pro sudy i lichy)
        CMD_SETCOLOR  = $D0

        '----------------------------------------------------------------
        ' index promennych do sdilene pameti
        '----------------------------------------------------------------
        MESSAGE    =  0         ' textova zprava, ktera se vypisuje nad obraz
        PAX_CMD    = 20         ' prikaz pro PAX modul - zapisuje se na port F8
        PAX_DATA   = 21         ' data pro cteni z PAX modulu - ctou se z portu F8
        PAX_STATUS = 22         ' status operace - cte se z portu FB
        PAX_LOCK   = 23         ' zamek - pokud neni nula, tak zapis do CMD je ve skutecnosti zapisem dat
        RX_HEAD    = 24         
        RX_TAIL    = 25         
        TX_HEAD    = 26         
        TX_TAIL    = 27         
        RX_BUFFER  = 28
        TX_BUFFER  = RX_BUFFER + BUFFER_LENGTH
        PALETTE    = TX_BUFFER + BUFFER_LENGTH
        C17D       = PALETTE + 64
        
VAR


DAT
        
PUB  None