Soubor projektů pro PAX modul.
==============================

KiCAD
=====

Schéma zapojení PAX modulu, bez návrhu desky.


Generator
=========

Projekt pro ATTiny25/45/85 vytvořen v Microchip Studio.
Výsledek překladu: Generator.hex


PXBOARD
=======

Projekt pro čip Parallax Propeller vytvořen v Propeller Tool v1.3.2.
V Prostředí se musí označit soubor PAXmain.spin jako "Top" a dále překládat
některou z podnabídek Compile Top (např. pro vytvoření obrazu pro eeprom
je v nabídce Run-Compile Top-View Info .... zde tlačítko Save EEPROM file)
Výsledek překladu: PAXmain.eeprom


PAXDecoder
==========

Zdrojový kód pro ATF22V10C, vytvořeno pro WinCupl.
Výsledek překladu: PAXDECODER.jed


PAXCommander
============

Projekt GUI rozhraní, kterým se vypní "emulovaný ROM modul". Vytvořen pro vývojové
prostředí DevX85. Překlad probíhá překladačem "The Macroassembler AS" nadvakrát:
- nejprve překlad main.asm, který includuje zbylé soubory
- po té pax_rom.asm, který vytvoří spouštěcí image
Výsledek překladu: pax_rom.bin (zkopírovat do PXBOARD)


Více zde: maximalne.8u.cz/
